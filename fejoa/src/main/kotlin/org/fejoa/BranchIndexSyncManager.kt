package org.fejoa

import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import org.fejoa.network.RemoteIndexJob
import org.fejoa.network.ReturnType
import org.fejoa.network.SyncManager
import org.fejoa.network.WatchJob
import org.fejoa.repository.Repository
import org.fejoa.storage.HashValue
import org.fejoa.storage.StorageDir
import org.fejoa.support.Future
import org.fejoa.support.await


data class SyncEvent(val context: Context, val status: Status, val branch: String, val remote: String,
                     val error: String = ""): Logger.EventData {
    enum class Context {
        BRANCH,
        REMOTE_INDEX
    }

    enum class Status {
        SYNCED,
        TIMEOUT,
        WATCH_RESULT,
        ERROR
    }

    companion object {
        fun error(context: Context, branch: String, error: String): SyncEvent {
            return SyncEvent(context, Status.ERROR, branch, "", error)
        }

        fun timeout(context: Context, branch: String, remote: Remote): SyncEvent {
            return SyncEvent(context, Status.TIMEOUT, branch, remote.id)
        }

        fun watchResult(context: Context, branch: String, remote: Remote): SyncEvent {
            return SyncEvent(context, Status.WATCH_RESULT, branch, remote.id)
        }

        fun synced(context: Context, branch: String, remote: Remote): SyncEvent {
            return SyncEvent(context, Status.SYNCED, branch, remote.id)
        }
    }
}

fun Logger.syncInfo(event: SyncEvent) {
    onInfo(Logger.Event(Logger.TYPE.SYNC_MANAGER, event))
}

fun Logger.syncError(event: SyncEvent) {
    onError(Logger.Event(Logger.TYPE.SYNC_MANAGER, event))
}

class RemoteIndexWatcher(val context: FejoaContext, val syncManager: SyncManager, val remote: Remote,
                         val branchIndexCache: BranchIndexCache,
                         private val logger: Logger,
                         private val onBranchIndexUpdated: (Remote, StorageDir) -> Any) {
    private var isWorking: Boolean = false
    private var remoteIndexBranch: String? = null
    private var fetchIndexJob: Future<RemoteIndexJob.Result>? = null
    private var watchJob: Future<WatchJob.Result>? = null
    private var syncJob: Future<SyncManager.SyncResult>? = null

    /**
     * Called from a free coroutine (not in the event looper)
     */
    private suspend fun work() {
        var first = true
        while (isWorking) {
            val authInfo = LoginAuthInfo()
            val branch = getRemoteIndexBranch() ?: break

            val indexStorageDir = context.looper.runSync {
                context.getRemoteIndexStorage(remote.id, branch)
            }
            context.looper.runSync {
                if (first) {
                    // An up to date index doesn't mean that all branches has been synced. Thus, notify once before to
                    // handle this case.
                    onBranchIndexUpdated.invoke(remote, indexStorageDir)
                    first = false
                }

                val localHead = (indexStorageDir.getBackingDatabase() as Repository).branchBackend.getBranchLog().getHead()

                watchJob = syncManager.authManager.send(
                        WatchJob(listOf(WatchJob.WatchEntry(remote.user, branch, localHead?.entryId)), false),
                        remote, authInfo)
            }

            // we might have stopped working in the meantime
            if (!isWorking) {
                watchJob?.cancel()
                continue
            }

            val watchResult = watchJob?.await() ?: continue
            watchJob = null

            context.looper.runSync {
                if (watchResult.updated.isEmpty()) {
                    logger.syncInfo(SyncEvent.timeout(SyncEvent.Context.REMOTE_INDEX, branch, remote))
                    return@runSync
                }
                logger.syncInfo(SyncEvent.watchResult(SyncEvent.Context.REMOTE_INDEX, branch, remote))
                syncJob = syncManager.sync(context, indexStorageDir, remote, authInfo)
            }

            // we might have stopped working in the meantime
            if (!isWorking) {
                syncJob?.cancel()
                continue
            }
            val syncResult = syncJob?.await() ?: continue
            syncJob = null

            context.looper.runSync {
                if (syncResult.code == ReturnType.OK)
                    logger.syncInfo(SyncEvent.synced(SyncEvent.Context.REMOTE_INDEX, branch, remote))
                else
                    logger.syncError(SyncEvent.error(SyncEvent.Context.REMOTE_INDEX, branch, syncResult.message))

                onBranchIndexUpdated.invoke(remote, indexStorageDir)
            }
        }
    }

    private suspend fun getRemoteIndexBranch(): String? {
        if (remoteIndexBranch != null)
            return remoteIndexBranch

        fetchIndexJob = branchIndexCache.getRemoteIndexBranch(remote)
        val result = fetchIndexJob!!.await()
        fetchIndexJob = null
        if (result.code != ReturnType.OK) {
            context.looper.runSync {
                logger.syncError(SyncEvent.error(SyncEvent.Context.REMOTE_INDEX, "",
                        "Failed to get remote index: ${result.message}$"))
            }
            stop()
            return null
        }
        remoteIndexBranch = result.branch
        return remoteIndexBranch
    }

    fun start() {
        if (isWorking)
            return
        isWorking = true
        launch(context.coroutineContext) {
            try {
                work()
            } catch (exception: Throwable) {
                this@RemoteIndexWatcher.context.looper.runSync {
                    logger.syncError(SyncEvent.error(SyncEvent.Context.REMOTE_INDEX, "", exception.message
                            ?: "Exception while syncing the remote index (remote ${remote.server})"))
                }
                if (exception !is Future.CancelationException) {
                    if (isWorking) { // restart on failure TODO: catch where is error happened
                        stop()
                        start()
                    }
                }
            }
        }
    }

    fun stop() {
        isWorking = false

        fetchIndexJob?.cancel()
        fetchIndexJob = null

        watchJob?.cancel()
        watchJob = null

        syncJob?.cancel()
        syncJob = null
    }
}


/**
 * Sync local branches with the remotes
 *
 * To do so it watches and syncs the remote branch index and triggers the synchronization process.
 */
class RemoteIndexSyncer
    private constructor(val syncManager: SyncManager, val userData: UserData, val branchIndexCache: BranchIndexCache,
                        var logger: Logger) {

    private val context = syncManager.authManager.context
    private val remoteWorkers: MutableMap<String, RemoteIndexWatcher> = HashMap()
    private val syncJobs: MutableList<Future<SyncManager.SyncResult>> = ArrayList()
    private var syncingRemotes: Collection<Remote>? = null

    fun isSyncing(): Boolean {
        return syncingRemotes != null
    }

    companion object {
        suspend fun create(syncManager: SyncManager, userData: UserData, branchIndexCache: BranchIndexCache, logger: Logger = DevNullLogger())
                : RemoteIndexSyncer {
            val syncer = RemoteIndexSyncer(syncManager, userData, branchIndexCache, logger)
            val branchIndex = userData.context.getLocalIndexStorage()
            branchIndex.addListener(object : StorageDir.Listener {
                override suspend fun onAfterCommit(base: HashValue, newTip: HashValue) {
                    if (base != newTip) {
                        syncer.restartSyncing()
                    }
                }
            })
            return syncer
        }
    }

    private fun onRemoteIndexChanged(remote: Remote, remoteIndexDir: StorageDir) {
        context.looper.schedule {
            try {
                val localIndex: MutableMap<String, HashValue> = HashMap(BranchIndex(userData.context.getLocalIndexStorage()).list())
                val remoteIndex = BranchIndex(remoteIndexDir).list()
                val userDataBranches = userData.getBranches()

                for (entry in remoteIndex) {
                    val branch = entry.key
                    val localLogHead = localIndex.remove(branch)
                    if (localLogHead == entry.value)
                        continue

                    for (branch in userDataBranches.filter { it.branch.toHex() == branch })
                        sync(branch, remote)
                }

                // check for local branches that are not at the remote yet
                for (entry in localIndex) {
                    val branch = entry.key
                    for (branch in userDataBranches.filter { it.branch.toHex() == branch })
                        sync(branch, remote)
                }
            } catch (e: Throwable) {
                logger.syncError(SyncEvent.error(SyncEvent.Context.BRANCH, "", e.message
                        ?: "Exception while syncing"))
            }
        }
    }

    /**
     * Called from the event loop
     */
    private suspend fun sync(branch: Branch, remote: Remote) {
        // check first if the branch is at the remote
        val remoteRef = branch.locations.listRemoteRefs().firstOrNull { it.remoteId == remote.id }
                ?: return

        val dir = userData.getStorageDir(branch)
        try {
            val job = syncManager.sync(context, dir, remote, remoteRef.authInfo)
            syncJobs.add(job)
            launch(syncManager.authManager.context.coroutineContext) {
                try {
                    val syncResult = job.await()

                    this@RemoteIndexSyncer.context.looper.runSync {
                        // notify the observer
                        val branchHex = branch.branch.toHex()
                        if (syncResult.code == ReturnType.OK)
                            logger.syncInfo(SyncEvent.synced(SyncEvent.Context.BRANCH, branchHex, remote))
                        else
                            logger.syncError(SyncEvent.error(SyncEvent.Context.BRANCH, branchHex, syncResult.message))
                    }
                } catch (exception: Exception) {
                    this@RemoteIndexSyncer.context.looper.runSync {
                        logger.syncError(SyncEvent.error(SyncEvent.Context.BRANCH, branch.branch.toHex(),
                                exception.message ?: "Exception while syncing branch ${branch.branch}"))
                    }
                } finally {
                    syncJobs.remove(job)
                }
            }
        } finally {
            dir.close()
        }
    }

    fun startSyncing(remotes: Collection<Remote>) {
        stopSyncing()

        remotes.forEach { remote ->
            val worker = RemoteIndexWatcher(userData.context, syncManager, remote, branchIndexCache, logger,
                    this::onRemoteIndexChanged)
            remoteWorkers[remote.id] = worker
            worker.start()
        }

        syncingRemotes = remotes
    }

    fun stopSyncing() {
        syncingRemotes = null

        remoteWorkers.forEach { it.value.stop() }
        remoteWorkers.clear()

        syncJobs.forEach { it.cancel() }
        syncJobs.clear()
    }

    fun restartSyncing() {
        syncingRemotes?.let { startSyncing(it) }
    }
}
