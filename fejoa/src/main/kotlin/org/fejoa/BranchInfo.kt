package org.fejoa

import kotlinx.serialization.Optional
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON
import org.fejoa.storage.*


class RemoteRef(val remoteId: String, val authInfo: AuthInfo) {
    @Serializable
    class PlainRemoteRefIO(val remoteId: String, val authInfo: PlainAuthInfo)

    @Serializable
    class LoginRemoteRefIO(val remoteId: String, val authInfo: LoginAuthInfo)

    @Serializable
    class TokenRemoteRefIO(val remoteId: String, val authInfo: AuthToken)

    @Serializable
    class RemoteRefTypeParser(val authInfo: AuthInfoParser)

    companion object {
        fun parse(value: String): RemoteRef {
            val typeParser = JSON(nonstrict = true).parse<RemoteRefTypeParser>(value)
            return when (typeParser.authInfo.type) {
                AuthType.PLAIN -> {
                    val parsed = JSON.parse<PlainRemoteRefIO>(value)
                    RemoteRef(parsed.remoteId, parsed.authInfo)
                }
                AuthType.LOGIN -> {
                    val parsed = JSON.parse<LoginRemoteRefIO>(value)
                    RemoteRef(parsed.remoteId, parsed.authInfo)
                }
                AuthType.TOKEN -> {
                    val parsed = JSON.parse<TokenRemoteRefIO>(value)
                    RemoteRef(parsed.remoteId, parsed.authInfo)
                }
            }
        }
    }

    fun toJson(): String {
        return when (authInfo.type) {
            AuthType.PLAIN -> JSON.stringify(PlainRemoteRefIO(remoteId, authInfo as PlainAuthInfo))
            AuthType.LOGIN -> JSON.stringify(LoginRemoteRefIO(remoteId, authInfo as LoginAuthInfo))
            AuthType.TOKEN -> JSON.stringify(TokenRemoteRefIO(remoteId, authInfo as AuthToken))
        }
    }
}

@Serializable
class BranchInfo(val branch: String, val description: String = "", @Optional val key: JsonSecretKeyData? = null)

/**
 * @param tokenId the token id to find the server token in the AccessStore
 */
@Serializable
class ContactAccess(@Serializable(with = HashValueDataSerializer::class) val contact: HashValue,
                    @Serializable(with = HashValueDataSerializer::class) val tokenId: HashValue, val token: AccessToken)

class Branch(val storageDir: IOStorageDir, val context: String, val branch: HashValue) {
    val branchInfo = DBBranchInfo(storageDir, "info")
    val locations = DBMapRemoteRef(storageDir, "locations")
    val contactAccessMap = DBMapContactAccess(storageDir, "contactAccess")

    suspend fun updateRemote(remote: Remote, authInfo: AuthInfo) {
        updateRemote(RemoteRef(remote.id, authInfo))
    }

    suspend fun updateRemote(remoteRef: RemoteRef) {
        locations.get(HashValue.fromHex(remoteRef.remoteId)).write(remoteRef)
    }
}
