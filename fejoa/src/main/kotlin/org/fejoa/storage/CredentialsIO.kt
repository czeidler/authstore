package org.fejoa.storage

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON
import org.fejoa.crypto.*
import org.fejoa.support.decodeBase64
import org.fejoa.support.encodeBase64


@Serializable
class KeyJson(val key: String, val algo: CryptoSettings.SYM_ALGO) {
    companion object {
        suspend fun from(key: SecretKey): KeyJson {
            val keyBase64 = CryptoHelper.crypto.encode(key).encodeBase64()
            return KeyJson(keyBase64, key.algo)
        }
    }

    suspend fun toSecretKey(): SecretKey {
        return CryptoHelper.crypto.secretKeyFromRaw(this.key.decodeBase64(), this.algo)
    }
}

suspend fun SecretKey.toJson(): String {
    return JSON.stringify(KeyJson.from(this))
}

suspend fun secretKeyFromJson(json: String): SecretKey {
    return JSON.parse<KeyJson>(json).toSecretKey()
}

@Serializable
private class SymCredentialsJson(val key: KeyJson, val iv: String, val algo: CryptoSettings.SYM_ALGO)

suspend fun SecretKeyIvData.toJson(): String {
    return JSON.stringify(SymCredentialsJson(KeyJson.from(this.key), this.iv.encodeBase64(), this.algo))
}

suspend fun symCredentialsFromJson(json: String): SecretKeyIvData {
    val symJson = JSON.parse<SymCredentialsJson>(json)
    return SecretKeyIvData(symJson.key.toSecretKey(), symJson.iv.decodeBase64(), symJson.algo)
}



@Serializable
private class KeyPairJson(val publicKey: String, val privateKey: String, val algo: CryptoSettings.ASYM_ALGO) {
    companion object {
        suspend fun from(pair: KeyPair): KeyPairJson {
            val publicKeyBase64 = CryptoHelper.crypto.encode(pair.publicKey).encodeBase64()
            val privateKeyBase64 = CryptoHelper.crypto.encode(pair.privateKey).encodeBase64()
            return KeyPairJson(publicKeyBase64, privateKeyBase64, pair.publicKey.algo)
        }
    }

    suspend fun toKeyPair(): KeyPair {
        val publicK = CryptoHelper.crypto.publicKeyFromRaw(publicKey.decodeBase64(), algo)
        val privateK = CryptoHelper.crypto.privateKeyFromRaw(privateKey.decodeBase64(), algo)
        return KeyPair(publicK, privateK)
    }
}

@Serializable
private class SignCredentialsJson(val key: KeyPairJson, val algo: CryptoSettings.ASYM_ALGO)

suspend fun KeyPairData.toJson(): String {
    return JSON.stringify(SignCredentialsJson(KeyPairJson.from(this.keyPair), this.algo))
}

suspend fun signCredentialsFromJson(json: String): KeyPairData {
    val signJson = JSON.parse<SignCredentialsJson>(json)
    return KeyPairData(signJson.key.toKeyPair(), signJson.algo)
}