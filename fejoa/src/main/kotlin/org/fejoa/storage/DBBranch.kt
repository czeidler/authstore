package org.fejoa.storage

import kotlinx.serialization.json.JSON
import org.fejoa.Branch
import org.fejoa.BranchInfo
import org.fejoa.ContactAccess
import org.fejoa.RemoteRef
import org.fejoa.support.PathUtils


class DBRemoteRef(dir: IOStorageDir, path: String) : DBValue<RemoteRef>(dir, path) {
    constructor(parent: DBObject, relativePath: String)
            : this(parent.dir, PathUtils.appendDir(parent.path, relativePath))

    override suspend fun write(obj: RemoteRef) {
        dir.writeString(path, obj.toJson())
    }

    override suspend fun get(): RemoteRef {
        return RemoteRef.parse(dir.readString(path))
    }
}

class DBMapRemoteRef(dir: IOStorageDir, path: String)
    : DBMap<HashValue, DBRemoteRef>(dir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listFiles(path)
    }

    override fun get(remoteId: HashValue): DBRemoteRef {
        return DBRemoteRef(dir, PathUtils.appendDir(path, remoteId.toHex()))
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }

    suspend fun listRemoteRefs(): List<RemoteRef> {
        return list().map { get(HashValue.fromHex(it)).get() }
    }
}


class DBContactAccess(dir: IOStorageDir, path: String) : DBValue<ContactAccess>(dir, path) {
    constructor(parent: DBObject, relativePath: String)
            : this(parent.dir, PathUtils.appendDir(parent.path, relativePath))

    override suspend fun write(obj: ContactAccess) {
        dir.writeString(path, JSON.stringify(obj))
    }

    override suspend fun get(): ContactAccess {
        return JSON.parse(dir.readString(path))
    }
}

class DBMapContactAccess(dir: IOStorageDir, path: String)
    : DBMap<HashValue, DBContactAccess>(dir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listFiles(path)
    }

    override fun get(tokenId: HashValue): DBContactAccess {
        return DBContactAccess(dir, PathUtils.appendDir(path, tokenId.toHex()))
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }
}


class DBBranchInfo(dir: IOStorageDir, path: String) : DBValue<BranchInfo>(dir, path) {
    constructor(parent: DBObject, relativePath: String)
            : this(parent.dir, PathUtils.appendDir(parent.path, relativePath))

    override suspend fun write(obj: BranchInfo) {
        dir.writeString(path, JSON.stringify(obj))
    }

    override suspend fun get(): BranchInfo {
        return JSON.parse(dir.readString(path))
    }
}

class DBContextBranchMap(dir: IOStorageDir, path: String) : DBMap<String, DBBranchMap>(dir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listDirectories(path)
    }

    override fun get(key: String): DBBranchMap {
        return DBBranchMap(dir, PathUtils.appendDir(path, key), key)
    }

    override suspend fun remove(key: String) {
        dir.remove(PathUtils.appendDir(path, key))
    }
}

class DBBranchMap(dir: IOStorageDir, path: String, val context: String) : DBMap<HashValue, Branch>(dir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listDirectories(path)
    }

    override fun get(branch: HashValue): Branch {
        return Branch(IOStorageDir(dir, PathUtils.appendDir(path, branch.toHex())), context, branch)
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }
}
