package org.fejoa.storage

import kotlinx.serialization.json.JSON
import org.fejoa.command.OutQueueEntry
import org.fejoa.support.PathUtils


class OutQueueEntryDBValue(parent: DBObject, relativePath: String) : DBValue<OutQueueEntry>(parent, relativePath) {
    override suspend fun write(obj: OutQueueEntry) {
        dir.writeString(path, JSON.stringify(obj))
    }

    override suspend fun get(): OutQueueEntry {
        return JSON.parse(dir.readString(path))
    }
}

class OutQueueEntryDBMap(dir: IOStorageDir, path: String) : DBMap<HashValue, OutQueueEntryDBValue>(dir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listFiles(path)
    }

    override fun get(key: HashValue): OutQueueEntryDBValue {
        return OutQueueEntryDBValue(this, key.toHex())
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }

    suspend fun listEntries(): Collection<OutQueueEntry> {
        return list().map { get(HashValue.fromHex(it)).get() }
    }
}
