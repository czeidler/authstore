package org.fejoa.storage

import kotlinx.serialization.json.JSON
import org.fejoa.Remote
import org.fejoa.support.PathUtils


class RemoteDBValue(parent: DBObject, relativePath: String) : DBValue<Remote>(parent, relativePath) {
    override suspend fun write(obj: Remote) {
        dir.writeString(path, JSON.stringify(obj))
    }

    override suspend fun get(): Remote {
        return JSON.parse(dir.readString(path))
    }
}

class DBMapRemote(dir: IOStorageDir, path: String) : DBMap<HashValue, RemoteDBValue>(dir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listFiles(path)
    }

    override fun get(key: HashValue): RemoteDBValue {
        return RemoteDBValue(this, key.toHex())
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }

    suspend fun listRemotes(): Collection<Remote> {
        return list().map { get(HashValue.fromHex(it)).get() }
    }
}
