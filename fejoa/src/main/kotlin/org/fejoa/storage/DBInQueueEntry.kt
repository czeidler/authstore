package org.fejoa.storage

import kotlinx.serialization.json.JSON
import org.fejoa.command.InQueue
import org.fejoa.support.PathUtils


class InQueueEntryDBValue(parent: DBObject, relativePath: String) : DBValue<InQueue.InQueueEntry>(parent, relativePath) {
    override suspend fun write(obj: InQueue.InQueueEntry) {
        dir.writeString(path, JSON.stringify(obj))
    }

    override suspend fun get(): InQueue.InQueueEntry {
        return JSON.parse(dir.readString(path))
    }
}

class InQueueEntryDBMap(dir: IOStorageDir, path: String) : DBMap<HashValue, InQueueEntryDBValue>(dir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listFiles(path)
    }

    override fun get(key: HashValue): InQueueEntryDBValue {
        return InQueueEntryDBValue(this, key.toHex())
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }

    suspend fun listEntries(): Collection<InQueue.InQueueEntry> {
        return list().map { get(HashValue.fromHex(it)).get() }
    }
}
