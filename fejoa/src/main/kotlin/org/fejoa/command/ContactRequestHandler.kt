package org.fejoa.command

import kotlinx.serialization.json.JSON
import org.fejoa.Client
import org.fejoa.asPublicKeyData
import org.fejoa.crypto.getId
import org.fejoa.storage.HashValue


class ContactRequestHandler(val client: Client) : InQueueManager.Handler<InQueueManager.EnvelopeHandle> {
    private val userData = client.userData

    override val command: String
        get() = ContactRequestCommand.NAME

    inner class PendingContactRequests(handleManager: InQueueHandleManager<ContactRequestCommand>,
                                       command: ContactRequestCommand, handle: InQueueManager.EnvelopeHandle)
        : InQueueHandleManager.PendingRequests<ContactRequestCommand>(handleManager, command, handle) {
        override suspend fun onAccept() {
            client.sendContactRequest(command.userRemote,
                    command.verificationKey.asPublicKeyData(),
                    command.asymEncKey.asPublicKeyData(), false)
            addContact(command)
            userData.commit()
        }
    }

    val handleManager = InQueueHandleManager<ContactRequestCommand>()

    private suspend fun validateRequest(command: ContactRequestCommand) {
        // verify contact id
        // In the inqueue we already verified that contact knows the private signing key. It remains to check that the
        // contact id is valid, i.e. equals the verification key id.
        val verificationKey = command.verificationKey.asPublicKeyData()
        val verificationKeyId = verificationKey.publicKey.getId()
        if (verificationKeyId != command.user)
            throw Exception("Invalid user id. User id doesn't match with verification key")
    }

    private suspend fun addContact(command: ContactRequestCommand) {
        val contact = userData.contacts.list.get(command.user)
        contact.id.write(command.user)
        contact.remotes.get(HashValue.fromHex(command.userRemote.id)).write(command.userRemote)

        // keys
        val verificationKey = command.verificationKey.asPublicKeyData()
        contact.keys.verificationKeys.get(verificationKey.publicKey.getId()).write(command.verificationKey)
        val asymEncKey = command.asymEncKey.asPublicKeyData()
        contact.keys.asymEncKeys.get(asymEncKey.publicKey.getId()).write(command.asymEncKey)
    }

    override suspend fun onNewEntry(entry: InQueueManager.EnvelopeHandle) {
        val command = JSON.parse<ContactRequestCommand>(entry.commandJson)
        validateRequest(command)

        val ourRequestPair = userData.contacts.requested.listRequests().firstOrNull {
            it.second.remote.address() == command.userRemote.address()
        }

        if (ourRequestPair != null) {
            // auto accept since we already requested the contact
            val ourRequest = ourRequestPair.second
            if (ourRequest.verificationKey != null && command.verificationKey != ourRequest.verificationKey) {
                throw Exception("Verification key differs from key in request. For security reasons, please start a new contact request.")
            }
            if (ourRequest.asymEncKey != null && command.asymEncKey != ourRequest.asymEncKey) {
                throw Exception("Encryption key differs from key in request. For security reasons, please start a new contact request.")
            }

            addContact(command)
            // remove request
            userData.contacts.requested.get(ourRequestPair.first).delete()
            userData.commit()

            return
        }

        //TODO auto except request of already known contacts?

        handleManager.addPending(PendingContactRequests(handleManager, command, entry))
    }

}