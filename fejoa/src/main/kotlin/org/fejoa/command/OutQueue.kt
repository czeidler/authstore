package org.fejoa.command

import kotlinx.coroutines.experimental.launch
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON
import kotlinx.serialization.protobuf.ProtoBuf
import org.fejoa.*
import org.fejoa.crypto.CryptoHelper
import org.fejoa.crypto.CryptoSettings
import org.fejoa.crypto.PublicKeyData
import org.fejoa.network.CommandJob
import org.fejoa.network.ReturnType
import org.fejoa.storage.HashValue
import org.fejoa.storage.OutQueueEntryDBMap
import org.fejoa.storage.StorageDir
import org.fejoa.support.await
import org.fejoa.support.decodeBase64
import org.fejoa.support.encodeBase64
import org.fejoa.support.toUTF


data class OutQueueEvent(val sendResult: String, val remote: Remote): Logger.EventData

fun Logger.outQueueInfo(event: OutQueueEvent) {
    onInfo(Logger.Event(Logger.TYPE.OUT_QUEUE, event))
}

fun Logger.outQueueError(event: OutQueueEvent) {
    onError(Logger.Event(Logger.TYPE.OUT_QUEUE, event))
}

/**
 * @param command base64 encoded envelope data
 * @param receiver base64 encode. Wrapped envelope key encrypted with the receiver's key.
 * @param senderKey base64 encode. Wrapped envelope key encrypted with the sender's key.
 */
@Serializable
class OutQueueEntry(val receiver: Remote, val command: String, val receiverKey: String, val senderKey: String)


class OutQueue(val storageDir: StorageDir) {
    val entries = OutQueueEntryDBMap(storageDir, "queue")

    private suspend fun hash(receiver: Remote, command: ByteArray): HashValue {
        val stream = CryptoHelper.sha256Hash()
        stream.write(ProtoBuf.dump(receiver))
        stream.write(command)
        return HashValue(stream.hash())
    }

    suspend fun remove(entry: OutQueueEntry) {
        entries.remove(hash(entry.receiver, entry.command.decodeBase64()))
        commit()
    }

    suspend fun post(receiver: Remote, envelope: ByteArray, receiverKey: ByteArray, senderKey: ByteArray) {
        val id = hash(receiver, envelope)
        entries.get(id).write(OutQueueEntry(receiver, envelope.encodeBase64(), receiverKey.encodeBase64(),
                senderKey.encodeBase64()))
        commit()
    }

    private suspend fun commit() {
        storageDir.commit()
    }
}


class OutQueueManager(val connectionAuthManager: ConnectionAuthManager, val outQueue: OutQueue, val logger: Logger) {
    val context = connectionAuthManager.context

    val listener = object: StorageDir.Listener {
        override suspend fun onAfterCommit(base: HashValue, newTip: HashValue) {
            triggerWork()
        }
    }

    private var started = false
    private var workRequested = false
    private var isWorking = false

    suspend fun start() {
        if (started)
            return
        started = true

        outQueue.storageDir.addListener(listener)

        triggerWork()
    }

    fun stop() {
        if (!started)
            return
        started = false

        outQueue.storageDir.removeListener(listener)
    }

    private suspend fun triggerWork() {
        if (isWorking) {
            workRequested = true
            return
        }
        isWorking = true

        // load the list before entering the new coroutine
        val entries = outQueue.entries.listEntries()
        launch(context.coroutineContext) {
            workQueue(entries)

            this@OutQueueManager.context.looper.runSync {
                isWorking = false
                if (workRequested) {
                    workRequested = false
                    triggerWork()
                }
            }
        }
    }

    // TODO: send out jobs in parallel
    private suspend fun workQueue(list: Collection<OutQueueEntry>) {
        list.forEach {
            val result = connectionAuthManager.send(CommandJob(it.receiver.user, it.command.decodeBase64(),
                    it.receiverKey.decodeBase64()), it.receiver, PlainAuthInfo()).await()

            context.looper.schedule {
                if (result.code == ReturnType.OK) {
                    outQueue.remove(it)
                    logger.outQueueInfo(OutQueueEvent(result.message, it.receiver))
                } else
                    logger.outQueueError(OutQueueEvent(result.message, it.receiver))
            }
        }
    }
}

suspend fun OutQueue.post(jsonCommand: String, userData: UserData, contact: Contact) {
    return post(jsonCommand, userData, contact.remotes.listRemotes().first(),
            contact.keys.asymEncKeys.listKeys().first().asPublicKeyData())
}

suspend fun OutQueue.post(jsonCommand: String, userData: UserData, contactRemote: Remote,
                          contactAsymEnc: PublicKeyData?) {
    val senderSigningCredentials = userData.getSigningCredentials()
    val enveloper = CommandEnveloper.create(CryptoSettings.default.symmetric, userData.id.get(),
            senderSigningCredentials, contactAsymEnc)
    val envelope = enveloper.envelope(jsonCommand.toUTF())
    val receiverKey = enveloper.wrapReceiverKey()?.toProtoBuffer()?.toByteArray() ?: ByteArray(0)

    val senderEncCredentials = userData.getAsymEncCredentials()
    val senderKey = enveloper.wrapKey(senderEncCredentials.keyPair.publicKey,
            senderEncCredentials.algo)?.toProtoBuffer()?.toByteArray() ?: ByteArray(0)

    return post(contactRemote, envelope.toProtoBuffer().toByteArray(),
            receiverKey = receiverKey,
            senderKey = senderKey)
}