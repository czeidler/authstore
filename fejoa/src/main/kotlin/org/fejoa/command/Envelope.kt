package org.fejoa.command

import org.fejoa.chunkcontainer.BoxSpec
import org.fejoa.command.Envelope.Companion.CONTENT_TAG
import org.fejoa.command.Envelope.Companion.TYPE_TAG
import org.fejoa.crypto.*
import org.fejoa.protocolbufferlight.ProtocolBufferLight
import org.fejoa.support.DeflateUtils



class Signature(val signature: ByteArray, val signerId: ByteArray, val signingKeyId: ByteArray,
                val algo: CryptoSettings.ASYM_ALGO) {
    companion object {
        const val SIGNATURE_TAG = 1
        const val SIGNER_ID_TAG = 2
        const val KEY_ID_TAG = 3
        const val ALGO_TAG = 4

        fun read(bytes: ByteArray): Signature {
            val buffer = ProtocolBufferLight(bytes)
            val signature = buffer.getBytes(SIGNATURE_TAG) ?: throw Exception("Missing signature field")
            val signerId = buffer.getBytes(SIGNER_ID_TAG) ?: throw Exception("Missing signerId field")
            val keyId = buffer.getBytes(KEY_ID_TAG) ?: throw Exception("Missing keyId field")
            val algoValue = buffer.getString(ALGO_TAG) ?: throw Exception("Missing algoValue field")
            val algo = CryptoSettings.ASYM_ALGO.values().firstOrNull { it.name == algoValue }
                    ?: throw Exception("Unknown algo $algoValue")
            return Signature(signature, signerId, keyId, algo)
        }
    }

    fun toProtoBuffer(): ProtocolBufferLight {
        val buffer = ProtocolBufferLight()
        buffer.put(SIGNATURE_TAG, signature)
        buffer.put(SIGNER_ID_TAG, signerId)
        buffer.put(KEY_ID_TAG, signingKeyId)
        buffer.put(ALGO_TAG, algo.name)
        return buffer
    }
}


enum class EnvelopeType {
    PLAIN,
    SYM_ENC,
    ASYM_ENC
}

interface Envelope {
    companion object {
        const val TYPE_TAG = 0
        const val CONTENT_TAG = 1

        fun read(byteArray: ByteArray): Envelope {
            val buffer = ProtocolBufferLight(byteArray)
            val type = buffer.getString(Envelope.TYPE_TAG)?.let { type ->
                EnvelopeType.values().firstOrNull { it.name == type } ?: throw Exception("Unknown type $type")
            } ?: throw Exception("Missing type field")
            return when (type) {
                EnvelopeType.PLAIN -> PlainEnvelope.read(buffer)
                EnvelopeType.SYM_ENC -> SymEncEnvelope.read(buffer)
                EnvelopeType.ASYM_ENC -> AsymEncEnvelope.read(buffer)
            }
        }
    }

    val type: EnvelopeType
    val content: ByteArray

    fun toProtoBuffer(): ProtocolBufferLight
}

/**
 * Commands are usually send encrypted for which reason the more compact ProtocolBuffer for serialization is choosen for
 * the envelope (the command would be not much more readable when encode a command in json). However, type information
 * and enums are stored as string values to increase extensibility and make unknown envelope field easier to debug.
 */
class PlainEnvelope private constructor(override val type: EnvelopeType, override val content: ByteArray,
                                   val zipType: BoxSpec.ZipType = BoxSpec.ZipType.DEFLATE,
                                   val signature: Signature? = null) : Envelope {
    companion object {
        suspend fun pack(data: ByteArray, signerID: ByteArray, signCredentials: KeyPairData?,
                         zipType: BoxSpec.ZipType = BoxSpec.ZipType.DEFLATE)
                : PlainEnvelope {
            val signature = signCredentials?.let {
                val sig = CryptoHelper.crypto.sign(data, it.keyPair.privateKey, it.algo)

                Signature(sig, signerID, it.getId().bytes, it.algo)
            }

            var usedZipType = zipType
            val packed = when (zipType) {
                BoxSpec.ZipType.NONE -> data
                BoxSpec.ZipType.DEFLATE -> {
                    var result = DeflateUtils.deflate(data)
                    // only pack if we safe space
                    if (result.size >= data.size) {
                        result = data
                        usedZipType = BoxSpec.ZipType.NONE
                    }
                    result
                }
            }

            return PlainEnvelope(EnvelopeType.PLAIN, packed, usedZipType, signature)
        }

        const val ZIP_TAG = 2
        const val SIGNATURE_TAG = 3

        fun read(buffer: ProtocolBufferLight): PlainEnvelope {
            val content = buffer.getBytes(CONTENT_TAG) ?: throw Exception("Missing packed data field")
            val zipType = buffer.getString(ZIP_TAG)?.let { type ->
                BoxSpec.ZipType.values().firstOrNull { it.name == type }?: throw Exception("Unknown zip type $type")
            } ?: BoxSpec.ZipType.NONE
            val signature = buffer.getBytes(SIGNATURE_TAG)?.let { Signature.read(it) }
            return PlainEnvelope(EnvelopeType.PLAIN, content, zipType, signature)
        }
    }

    override fun toProtoBuffer(): ProtocolBufferLight {
        val buffer = ProtocolBufferLight()
        buffer.put(TYPE_TAG, type.name)
        buffer.put(CONTENT_TAG, content)
        buffer.put(ZIP_TAG, zipType.name)
        signature?.let {
            buffer.put(SIGNATURE_TAG, it.toProtoBuffer().toByteArray())
        }
        return buffer
    }

    suspend fun getData(): ByteArray {
        return when (zipType) {
            BoxSpec.ZipType.NONE -> content
            BoxSpec.ZipType.DEFLATE -> DeflateUtils.inflate(content)
        }
    }

    suspend fun verify(key: PublicKey): Boolean {
        return signature?.let {
            CryptoHelper.crypto.verifySignature(getData(), it.signature, key, it.algo)
        } ?: false
    }
}

/**
 * @param keyId optional key id to identify the encryption key
 */
class SymEncEnvelope(override val type: EnvelopeType, override val content: ByteArray, val keyId: ByteArray,
                     val iv: ByteArray, val algo: CryptoSettings.SYM_ALGO) : Envelope {
    companion object {
        const val KEY_ID_TAG = 2
        const val ALGO_TAG = 3
        const val IV_TAG = 4

        suspend fun pack(data: ByteArray, keyId: ByteArray, key: SecretKey, algo: CryptoSettings.SYM_ALGO): SymEncEnvelope {
            val iv = CryptoHelper.crypto.generateBits(algo.ivSize)
            val encryptedData = CryptoHelper.crypto.encryptSymmetric(data, key, iv, algo)
            return SymEncEnvelope(EnvelopeType.SYM_ENC, encryptedData, keyId, iv, algo)
        }

        fun read(buffer: ProtocolBufferLight): SymEncEnvelope {
            val content = buffer.getBytes(CONTENT_TAG) ?: throw Exception("Missing packed data field")
            val keyId = buffer.getBytes(KEY_ID_TAG) ?: ByteArray(0)
            val iv = buffer.getBytes(IV_TAG) ?: throw Exception("Missing iv field")
            val algo = buffer.getString(ALGO_TAG)?.let { type ->
                CryptoSettings.SYM_ALGO.values().firstOrNull { it.name == type }
                        ?: throw Exception("Unknown algo type $type")
            } ?: throw Exception("Missing algo field")
            return SymEncEnvelope(EnvelopeType.SYM_ENC, content, keyId, iv, algo)
        }
    }

    override fun toProtoBuffer(): ProtocolBufferLight {
        val buffer = ProtocolBufferLight()
        buffer.put(Envelope.TYPE_TAG, type.name)
        buffer.put(Envelope.CONTENT_TAG, content)
        if (keyId.isNotEmpty())
            buffer.put(KEY_ID_TAG, keyId)
        buffer.put(IV_TAG, iv)
        buffer.put(ALGO_TAG, algo.name)
        return buffer
    }

    suspend fun decrypt(key: SecretKey): ByteArray {
        return CryptoHelper.crypto.decryptSymmetric(content, key, iv, algo)
    }
}

class AsymEncEnvelope(override val type: EnvelopeType, override val content: ByteArray, val keyId: ByteArray,
                      val algo: CryptoSettings.ASYM_ALGO) : Envelope {
    companion object {
        const val KEY_ID_TAG = 2
        const val ALGO_TAG = 3

        suspend fun pack(data: ByteArray, publicKey: PublicKey, algo: CryptoSettings.ASYM_ALGO): AsymEncEnvelope {
            val encryptedData = CryptoHelper.crypto.encryptAsymmetric(data, publicKey, algo)
            return AsymEncEnvelope(EnvelopeType.ASYM_ENC, encryptedData, publicKey.getId().bytes, algo)
        }

        fun read(buffer: ProtocolBufferLight): AsymEncEnvelope {
            val content = buffer.getBytes(CONTENT_TAG) ?: throw Exception("Missing packed data field")
            val keyId = buffer.getBytes(KEY_ID_TAG) ?: throw Exception("Missing key id field")
            val algo = buffer.getString(ALGO_TAG)?.let { type ->
                CryptoSettings.ASYM_ALGO.values().firstOrNull { it.name == type }
                        ?: throw Exception("Unknown algo type $type")
            } ?: throw Exception("Missing algo field")
            return AsymEncEnvelope(EnvelopeType.ASYM_ENC, content, keyId, algo)
        }
    }

    override fun toProtoBuffer(): ProtocolBufferLight {
        val buffer = ProtocolBufferLight()
        buffer.put(Envelope.TYPE_TAG, type.name)
        buffer.put(Envelope.CONTENT_TAG, content)
        buffer.put(KEY_ID_TAG, keyId)
        buffer.put(ALGO_TAG, algo.name)
        return buffer
    }

    suspend fun decrypt(key: PrivateKey): ByteArray {
        return CryptoHelper.crypto.decryptAsymmetric(content, key, algo)
    }
}
