package org.fejoa.command

import kotlinx.serialization.Serializable
import org.fejoa.FejoaContext
import org.fejoa.Logger
import org.fejoa.Remote
import org.fejoa.asPublicKeyData
import org.fejoa.crypto.CryptoHelper
import org.fejoa.crypto.PrivateKey
import org.fejoa.crypto.PublicKeyData
import org.fejoa.protocolbufferlight.ProtocolBufferLight
import org.fejoa.storage.HashValue
import org.fejoa.storage.InQueueEntryDBMap
import org.fejoa.storage.StorageDir
import org.fejoa.support.decodeBase64
import org.fejoa.support.encodeBase64
import org.fejoa.support.toUTFString


data class InQueueEvent(val message: String, val command: String): Logger.EventData

fun Logger.inQueueInfo(event: InQueueEvent) {
    onInfo(Logger.Event(Logger.TYPE.IN_QUEUE, event))
}

fun Logger.inQueueError(event: InQueueEvent) {
    onError(Logger.Event(Logger.TYPE.IN_QUEUE, event))
}

class InQueue(val storageDir: StorageDir) {
    val queue = InQueueEntryDBMap(storageDir, "queue")

    @Serializable
    class InQueueEntry(val time: Long, val command: String, val receiverKey: String) {
        constructor(time: Long, command: ByteArray, receiverKey: ByteArray)
                : this(time, command.encodeBase64(), receiverKey.encodeBase64())
    }

    private suspend fun hash(command: ByteArray): HashValue {
        return HashValue(CryptoHelper.sha256Hash(command))
    }

    suspend fun add(time: Long, command: ByteArray, receiverKey: ByteArray) {
        queue.get(hash(command)).write(InQueueEntry(time, command, receiverKey))
        storageDir.commit()
    }

    suspend fun remove(entry: InQueueEntry) {
        queue.remove(hash(entry.command.decodeBase64()))
        storageDir.commit()
    }
}

class InQueueManager(val context: FejoaContext, val inQueue: InQueue, val logger: Logger,
                     val keyGetter: suspend (keyId: HashValue) -> PrivateKey?,
                     val verificationKeyGetter: suspend (signer: HashValue, keyId: HashValue) -> PublicKeyData?) {
    companion object {
        const val ErrorHandlerName = "error"
    }

    interface Handler<in T : EntryHandle> {
        val command: String

        suspend fun onNewEntry(entry: T)
    }

    class MapList<T> {
        // map the command name to a list of handlers
        private val map: MutableMap<String, MutableList<T>> = HashMap()

        fun add(key: String, value: T) {
            map[key]?.add(value) ?: run {
                val handlerList: MutableList<T> = ArrayList()
                handlerList.add(value)
                map[key] = handlerList
            }
        }

        fun remove(key: String, value: T) {
            map[key]?.run {
                remove(value)
                if (isEmpty())
                    map.remove(key)
            }
        }

        fun getEntries(key: String): List<T> {
            return map[key] ?: emptyList()
        }
    }

    abstract inner class EntryHandle(val entry: InQueue.InQueueEntry) {
        /**
         * Removes the entry from the queue
         */
        suspend fun setHandled() {
            ongoingHandles.remove(entry.command)
            cleanUp()
            inQueue.remove(entry)
        }

        protected abstract fun cleanUp()
    }

    inner class EnvelopeHandle(entry: InQueue.InQueueEntry, val command: String, val commandJson: String,
                               val sender: HashValue)
        : EntryHandle(entry) {
        override fun cleanUp() {
            commandHandles.remove(command, this)
        }
    }

    inner class ErrorHandle(entry: InQueue.InQueueEntry, val error: String) : EntryHandle(entry) {
        override fun cleanUp() {
            errorHandles.remove(this)
        }
    }

    val listener = object : StorageDir.Listener {
        override suspend fun onAfterCommit(base: HashValue, newTip: HashValue) {
            handleInCommands()
        }
    }

    private var started = false

    val commandHandles: MapList<EnvelopeHandle> = MapList()
    val errorHandles: MutableList<ErrorHandle> = ArrayList()

    private val commandHandlers = MapList<Handler<EnvelopeHandle>>()
    private val errorHandlers: MutableList<Handler<ErrorHandle>> = ArrayList()

    // keep track for which commands we already have handles
    val ongoingHandles: MutableSet<String> = HashSet()

    private suspend fun notify(handler: Handler<EnvelopeHandle>, handle: EnvelopeHandle) {
        try {
            handler.onNewEntry(handle)
        } catch (e: Throwable) {
            addError(ErrorHandle(handle.entry, e.message ?: "Exception in handler"))
        }
    }

    /**
     * Add a command handler and notifies the handler about existing handles
     */
    suspend fun addCommandHandler(handler: Handler<EnvelopeHandle>) {
        commandHandlers.add(handler.command, handler)
        commandHandles.getEntries(handler.command).forEach {
            notify(handler, it)
        }
    }

    suspend fun addErrorHandler(handler: Handler<ErrorHandle>) {
        errorHandlers.add(handler)
        errorHandles.forEach {
            handler.onNewEntry(it)
        }
    }

    private suspend fun addError(errorHandle: ErrorHandle) {
        errorHandles.add(errorHandle)
        errorHandlers.forEach {
            it.onNewEntry(errorHandle)
        }
    }

    private suspend fun addHandle(handle: EnvelopeHandle) {
        val key = handle.command
        commandHandles.add(key, handle)
        commandHandlers.getEntries(key).forEach {
            notify(it, handle)
        }
    }

    private suspend fun decodeKey(data: ByteArray): CommandEnveloper.SymKeyData? {
        if (data.isEmpty())
            return null
        val envelope = Envelope.read(data)
        if (envelope !is AsymEncEnvelope)
            throw Exception("Unexpected key envelope type: ${envelope.type}")

        return CommandEnveloper.unwrapKey(envelope, keyGetter)
    }

    private suspend fun decodeCommand(data: ByteArray, keyData: CommandEnveloper.SymKeyData?): PlainEnvelope {
        val envelope = Envelope.read(data)

        return when (envelope.type) {
            EnvelopeType.PLAIN -> envelope as PlainEnvelope
            EnvelopeType.SYM_ENC -> {
                if (keyData == null)
                    throw Exception("Key data required to decrypt envelope")
                val symEnvelope = envelope as SymEncEnvelope
                PlainEnvelope.read(ProtocolBufferLight(CommandEnveloper.unwrap(symEnvelope, keyData)))
            }
            EnvelopeType.ASYM_ENC -> throw Exception("${envelope.type} is an invalid command envelope")
        }
    }

    private suspend fun handleInCommands() {
        for (entry in inQueue.queue.listEntries()) {
            if (ongoingHandles.contains(entry.command))
                continue
            ongoingHandles.add(entry.command)

            try {
                val key = decodeKey(entry.receiverKey.decodeBase64())
                val envelope = decodeCommand(entry.command.decodeBase64(), key)

                val commandJson = envelope.getData().toUTFString()
                val command = CommandNameParser.parse(commandJson)

                if (envelope.signature == null)
                    throw Exception("Command is not signed")

                val verificationKey = verificationKeyGetter.invoke(HashValue(envelope.signature.signerId),
                        HashValue(envelope.signature.signingKeyId))?.publicKey ?: let {
                    // If it is a command request the verification key might be in the command
                    // Note, this does not prevent man in the middle attacks but proves that the sender at least has the
                    // private signing key.
                    // verify command
                    if (command.command == ContactRequestCommand.NAME) {
                        if (command.verificationKey == null)
                            null
                        else
                            command.verificationKey.asPublicKeyData().publicKey
                    } else
                        null
                } ?: throw Exception("Can't find verification key")
                if (!envelope.verify(verificationKey))
                    throw Exception("Failed to verify command")

                logger.inQueueInfo(InQueueEvent("Command received", command.command))
                addHandle(EnvelopeHandle(entry, command.command, commandJson, HashValue(envelope.signature.signerId)))
            } catch (e: Exception) {
                addError(ErrorHandle(entry, e.message ?: "Unknown exception"))
            }
        }
    }

    fun start() {
        if (started)
            return
        started = true

        inQueue.storageDir.addListener(listener)

        context.looper.schedule {
            handleInCommands()
        }
    }

    fun stop() {
        if (!started)
            return
        started = false

        inQueue.storageDir.removeListener(listener)
    }
}
