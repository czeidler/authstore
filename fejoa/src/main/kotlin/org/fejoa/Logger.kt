package org.fejoa

interface Logger {
    enum class Category {
        INFO,
        WARNING,
        ERROR
    }

    enum class TYPE {
        SYNC_MANAGER,
        IN_QUEUE,
        OUT_QUEUE,
        BRANCH_UPDATE_NOTIFIER
    }

    enum class LogLevel(val level: Int) {
        DETAILED(5),
        NORMAL(10)
    }

    interface EventData

    data class Event(val type: TYPE, val data: EventData)

    fun onEvent(category: Category, event: Event, level: LogLevel = LogLevel.NORMAL)

    fun onInfo(event: Event, level: LogLevel = LogLevel.NORMAL) {
        onEvent(Category.INFO, event, level)
    }

    fun onError(event: Event, level: LogLevel = LogLevel.NORMAL) {
        onEvent(Category.ERROR, event, level)
    }
}

class DispatcherLogger(val children: MutableList<Logger> = ArrayList()) : Logger {
    override fun onEvent(category: Logger.Category, event: Logger.Event, level: Logger.LogLevel) {
        children.forEach { it.onEvent(category, event, level) }
    }
}

class DevNullLogger : Logger {
    override fun onEvent(category: Logger.Category, event: Logger.Event, level: Logger.LogLevel) {}
}

class DefaultLogger : Logger {
    class LogTypeMap(private val map: MutableMap<Logger.TYPE, MutableList<Pair<Logger.Event, Logger.LogLevel>>>
                     = HashMap()) {
        fun getMap(): Map<Logger.TYPE, MutableList<Pair<Logger.Event, Logger.LogLevel>>> {
            return map
        }

        fun getEvents(type: Logger.TYPE): List<Pair<Logger.Event, Logger.LogLevel>> {
            return map[type] ?: emptyList()
        }

        fun getEvents(): List<Pair<Logger.Event, Logger.LogLevel>> {
            return map.flatMap { it.value }
        }

        fun addEvent(event: Logger.Event, level: Logger.LogLevel) {
            val list = map[event.type]
                    ?: ArrayList<Pair<Logger.Event, Logger.LogLevel>>().also { map[event.type] = it}
            list.add(event to level)
        }
    }

    class Entry(category: Logger.Category, event: Logger.Event, level: Logger.LogLevel)

    private val map: MutableMap<Logger.Category, LogTypeMap> = HashMap()
    private val list: MutableList<Entry> = ArrayList()

    fun getLogTypeMap(category: Logger.Category): LogTypeMap {
        return map[category] ?: LogTypeMap()
    }

    /**
     * All categories merged
     */
    fun getMerged(): LogTypeMap {
        val merged: MutableMap<Logger.TYPE, MutableList<Pair<Logger.Event, Logger.LogLevel>>> = HashMap()
        map.forEach { merged.putAll(it.value.getMap()) }
        return LogTypeMap(merged)
    }

    override fun onEvent(category: Logger.Category, event: Logger.Event, level: Logger.LogLevel) {
        val logTypeMap = map[category] ?: LogTypeMap().also { map[category] = it }
        logTypeMap.addEvent(event, level)

        list += Entry(category, event, level)
    }

    fun clear() {
        map.clear()
    }
}