package org.fejoa.network

import kotlinx.serialization.Serializable
import kotlinx.serialization.serializer


class RemoteIndexJob(val user: String) : RemoteJob<RemoteIndexJob.Result>() {
    companion object {
        val METHOD = "getRemoteIndex"
    }

    class Result(code: ReturnType, message: String, val branch: String) : RemoteJob.Result(code, message)

    @Serializable
    class RPCParams(val user: String)

    @Serializable
    class RPCResult(val branch: String)


    private fun getHeader(): String {
        return JsonRPCRequest(id = id, method = METHOD, params = RPCParams(user))
                .stringify(RPCParams::class.serializer())
    }

    override suspend fun run(remoteRequest: RemoteRequest): Result {
        val replyString = remoteRequest.send(getHeader()).receiveHeader()
        val response = JsonRPCResponse.parse(RPCResult::class.serializer(), replyString, id)
        val result = response.result
        return Result(ReturnType.OK, "ok", result.branch)
    }
}
