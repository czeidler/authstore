package org.fejoa.network

import org.fejoa.support.AsyncInStream
import org.fejoa.support.AsyncOutStream


interface RemoteRequest {
    interface DataSender {
        fun outStream(): AsyncOutStream
        suspend fun send(): Reply
    }

    interface Reply {
        suspend fun receiveHeader(): String
        suspend fun inStream(): AsyncInStream

        fun close()
    }

    /**
     * Send can be called multiple times to send multiple messages.
     *
     * An old ongoing request is canceled when sending a new request.
     */
    suspend fun send(header: String): Reply
    suspend fun sendData(header: String): DataSender

    /**
     * Cancel an ongoing request
     */
    fun cancel()
}

object HTTPRequestMultipartKeys {
    val REQUEST_KEY = "request"
    val DATA_KEY = "data"
}

expect fun platformCreateHTTPRequest(url: String): RemoteRequest
