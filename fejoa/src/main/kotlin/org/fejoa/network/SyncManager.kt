package org.fejoa.network

import org.fejoa.*
import org.fejoa.repository.Repository
import org.fejoa.repository.ThreeWayMerge
import org.fejoa.storage.Config
import org.fejoa.storage.Hash
import org.fejoa.storage.StorageDir
import org.fejoa.support.Future
import org.fejoa.support.assert
import org.fejoa.support.async
import org.fejoa.support.await


class SyncManager(val authManager: ConnectionAuthManager, var passwordGetter: PasswordGetter? = null) {
    class SyncResult(val code: ReturnType, val message: String, val head: Hash?)

    companion object {
        /**
         * To be called from the event loop
         */
        fun sync(fejoaContext: FejoaContext, storageDir: StorageDir, authManager: ConnectionAuthManager, remote: Remote,
                         authInfo: AuthInfo, passwordGetter: PasswordGetter?): Future<SyncResult> {
            var pullJob: Future<PullJob.Result>? = null
            var pushJob: Future<PushJob.Result>? = null

            val user = remote.user
            val branch = storageDir.branch
            val repository = storageDir.getBackingDatabase() as Repository

            val future = async {
                fejoaContext.looper.runSync {
                    pullJob = authManager.send(
                            PullJob(repository, storageDir.getCommitSignature(), ThreeWayMerge(), user, branch),
                            remote, authInfo, passwordGetter)
                }

                val pullResult = pullJob!!.await()
                pullJob = null

                val remoteRepoExists = pullResult.remoteHead != null
                val remoteHead = if (pullResult.remoteHead != null) {
                    if (pullResult.remoteHead.value.isZero)
                        null
                    else
                        pullResult.remoteHead
                } else null

                // the following block is fine to not run in the looper
                val localHead = run {
                    if (pullResult.code != ReturnType.OK)
                        return@async SyncResult(pullResult.code, pullResult.message, null)

                    val head = storageDir.getHead()
                    if (remoteRepoExists && remoteHead == head) { // fast forward
                        if (pullResult.oldHead != head && head != null)
                            storageDir.onTipUpdated(pullResult.oldHead?.value ?: Config.newDataHash(), head.value)
                        return@async SyncResult(ReturnType.OK, "Sync after pull: $branch", head)
                    }
                    head
                }

                val localExists = repository.log.getHead() != null
                if (!localExists)
                    return@async SyncResult(ReturnType.OK, "Nothing to push", null)

                fejoaContext.looper.runSync {
                    pushJob = authManager.send(PushJob(repository, user, branch), remote, authInfo,
                            passwordGetter)
                }
                val pushResult = pushJob!!.await()
                pushJob = null
                return@async SyncResult(pushResult.code, pushResult.message, localHead)
            }
            future.whenCompleted({_, error ->
                if (error is Future.CancelationException) {
                    pullJob?.cancel()
                    pushJob?.cancel()
                }
            })
            return future
        }
    }

    private val ongoingSyncs = OngoingSyncs()

    class OngoingSyncs(private val map: MutableMap<String, Future<SyncResult>> = HashMap()) {
        fun isSyncing(branch: String): Future<SyncResult>? {
            return map[branch]
        }

        fun startSyncing(branch: String, job: Future<SyncResult>) {
            assert(map[branch] == null)
            map[branch] = job
        }

        fun finishSyncing(branch: String) {
            map.remove(branch) ?: throw Exception("branch not syncing")
        }
    }

    /**
     * To be called from the event loop
     */
    fun sync(context: FejoaContext, storageDir: StorageDir, remote: Remote, authInfo: AuthInfo,
                     passwordGetter: PasswordGetter? = null): Future<SyncResult> {
        val branch = storageDir.branch
        val ongoingJob = ongoingSyncs.isSyncing(branch)
        if (ongoingJob != null)
            return ongoingJob

        val future = Companion.sync(context, storageDir, authManager, remote, authInfo,
                passwordGetter ?: this.passwordGetter)
        ongoingSyncs.startSyncing(branch, future)

        future.whenCompleted({ _, _ ->
            ongoingSyncs.finishSyncing(branch)
        })
        return future
    }
}