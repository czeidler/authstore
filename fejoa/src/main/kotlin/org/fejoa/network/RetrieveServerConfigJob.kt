package org.fejoa.network

import kotlinx.serialization.Optional
import kotlinx.serialization.Serializable
import kotlinx.serialization.serializer
import org.fejoa.ServerConfig


class RetrieveServerConfigJob(val user: String) : RemoteJob<RetrieveServerConfigJob.Result>() {
    companion object {
        val METHOD = "retrieveServerConfig"
    }

    @Serializable
    class RPCParams(val user: String)

    /**
     * List of accounts that are still active (logged in)
     */
    @Serializable
    class PPCResult(@Optional val serverConfig: ServerConfig? = null)

    class Result(code: ReturnType, message: String, val serverConfig: ServerConfig? = null)
        : RemoteJob.Result(code, message)

    private fun getHeader(): String {
        return JsonRPCRequest(id = id, method = METHOD, params = RPCParams(user)).stringify(RPCParams::class.serializer())
    }

    override suspend fun run(remoteRequest: RemoteRequest): Result {
        val replyString = remoteRequest.send(getHeader()).receiveHeader()

        val response = try {
            JsonRPCResponse.parse(PPCResult::class.serializer(), replyString, id)
        } catch (e: Exception) {
            val error = JsonRPCError.parse(ErrorMessage::class.serializer(), replyString, id).error
            return Result(ensureError(error.code), error.message)
        }

        return Result(ReturnType.OK, "ok", response.result.serverConfig)
    }
}
