package org.fejoa

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON
import org.fejoa.crypto.*
import org.fejoa.support.*


@Serializable
class UserDataRef(val branch: String, val algo: CryptoSettings.SYM_ALGO)


/**
 * Data required by the server
 */
@Serializable
class ServerConfig(val userDataConfig: UserDataConfig, val outQueue: String, val inQueue: String,
                   val accessStore: String)

/**
 * Information to access UserData
 *
 * Contains the UserKeyParams to derive the master key which is used to decrypt the UserDataIndex and the KeyStore. The
 * UserData can be opened using credentials stored in the KeyStore.
 *
 * @param encMasterKey to derive the master key
 * @param userData encrypted user data information (UserDataRef)
 * @param extra unencrypted data
 */
@Serializable
class UserDataConfig(val encMasterKey: PasswordProtectedKey, val userData: EncData) {
    companion object {
        suspend fun create(masterKey: SecretKey, password: String, userKeyParams: UserKeyParams,
                           userData: UserDataRef, cache: BaseKeyCache): UserDataConfig {
            // encrypt the master key
            val encMasterKey = PasswordProtectedKey.create(masterKey, userKeyParams, password, cache)

            // enc user data
            val settings = CryptoSettings.default.symmetric
            val iv = CryptoHelper.crypto.generateBits(settings.algo.ivSize)
            val data = JSON(indented = true).stringify(userData).toUTF()
            val encrypted = CryptoHelper.crypto.encryptSymmetric(data, masterKey, iv, settings.algo)

            return UserDataConfig(encMasterKey, EncData(encrypted, iv, settings.algo))
        }
    }

    suspend fun open(password: String, cache: BaseKeyCache): Pair<SecretKeyData, UserDataRef> {
        val masterKey = encMasterKey.decryptKey(password, cache)
        val userDataRef = getUserDataRef(masterKey, userData)
        return SecretKeyData(masterKey, userDataRef.algo) to userDataRef
    }

    private suspend fun getUserDataRef(key: SecretKey, encData: EncData): UserDataRef {
        val plain = CryptoHelper.crypto.decryptSymmetric(encData.getEncData(), key, encData.getIv(),
                encData.algo)
        return JSON.parse(plain.toUTFString())
    }
}
