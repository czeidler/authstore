package org.fejoa

import kotlinx.coroutines.experimental.async
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON
import org.fejoa.command.BranchUpdateCommand
import org.fejoa.command.post
import org.fejoa.network.RemoteIndexJob
import org.fejoa.network.ReturnType
import org.fejoa.repository.Repository
import org.fejoa.storage.*
import org.fejoa.support.Future
import org.fejoa.support.PathUtils
import org.fejoa.support.await


interface NotifyStatus {
    enum class StatusType {
        LAST_NOTIFIED,
        WAIT_FOR_ACCESS_STORE
    }

    val type: StatusType

    fun toJson(): String

    @Serializable
    private class NotifyStatusParser(val type: StatusType)

    companion object {
        fun parse(value: String): NotifyStatus {
            val typeParser = JSON(nonstrict = true).parse<NotifyStatusParser>(value)
            return when (typeParser.type) {
                NotifyStatus.StatusType.LAST_NOTIFIED -> JSON.parse<LastNotified>(value)
                NotifyStatus.StatusType.WAIT_FOR_ACCESS_STORE -> JSON.parse<WaitForAccessStore>(value)
            }
        }
    }
}


@Serializable
class LastNotified(@Serializable(with = HashValueDataSerializer::class) val lastNotifiedHead: HashValue,
                   override val type: NotifyStatus.StatusType = NotifyStatus.StatusType.LAST_NOTIFIED)
    : NotifyStatus {
    override fun toJson(): String {
        return JSON.Companion.stringify(this)
    }
}

@Serializable
class WaitForAccessStore(@Serializable(with = HashValueDataSerializer::class) val accessStore: HashValue,
                         override val type: NotifyStatus.StatusType = NotifyStatus.StatusType.WAIT_FOR_ACCESS_STORE)
    : NotifyStatus {
    override fun toJson(): String {
        return JSON.Companion.stringify(this)
    }
}

class DBNotifyStatus(dir: IOStorageDir, path: String) : DBValue<NotifyStatus>(dir, path) {
    constructor(parent: DBObject, relativePath: String)
            : this(parent.dir, PathUtils.appendDir(parent.path, relativePath))

    override suspend fun write(obj: NotifyStatus) {
        dir.writeString(path, obj.toJson())
    }

    override suspend fun get(): NotifyStatus {
        return NotifyStatus.parse(dir.readString(path))
    }
}

class DBMapContactNotifyStatus(val storageDir: StorageDir, path: String) : DBMap<HashValue, DBNotifyStatus>(storageDir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listFiles(path)
    }

    override fun get(contactId: HashValue): DBNotifyStatus {
        return DBNotifyStatus(storageDir, PathUtils.appendDir(path, contactId.toHex()))
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }
}

class DBMapBranchContact(val storageDir: StorageDir, path: String) : DBMap<HashValue, DBMapContactNotifyStatus>(storageDir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listDirectories(path)
    }

    override fun get(branch: HashValue): DBMapContactNotifyStatus {
        return DBMapContactNotifyStatus(storageDir, PathUtils.appendDir(path, branch.toHex()))
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }
}

class DBMapContextBranch(val storageDir: StorageDir, path: String) : DBMap<String, DBMapBranchContact>(storageDir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listDirectories(path)
    }

    override fun get(context: String): DBMapBranchContact {
        return DBMapBranchContact(storageDir, PathUtils.appendDir(path, context))
    }

    override suspend fun remove(key: String) {
        dir.remove(PathUtils.appendDir(path, key))
    }
}

class DBMapRemoteContext(val storageDir: StorageDir, path: String) : DBMap<HashValue, DBMapContextBranch>(storageDir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listDirectories(path)
    }

    override fun get(remoteId: HashValue): DBMapContextBranch {
        return DBMapContextBranch(storageDir, PathUtils.appendDir(path, remoteId.toHex()))
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }
}

class NotificationStore(storageDir: StorageDir): StorageDirObject(storageDir) {
    // remotes
    //      branches
    //          contacts -> status
    var remotes = DBMapRemoteContext(storageDir, "remotes")

    suspend fun getNotifyStatus(remote: Remote, context: String, branch: HashValue, contactId: HashValue): NotifyStatus? {
        val value = remotes.get(HashValue.fromHex(remote.id)).get(context).get(branch).get(contactId)
        if (!value.exists())
            return null
        return value.get()
    }

    suspend fun updateNotifyStatus(remote: Remote, context: String, branch: HashValue, contactId: HashValue, status: NotifyStatus) {
        val value = remotes.get(HashValue.fromHex(remote.id)).get(context).get(branch).get(contactId)
        value.write(status)
        commit()
    }
}

class BranchUpdateNotifier private constructor(val client: Client, val notifyStatusStorage: StorageDir) {
    data class EventData(val message: String, val remote: Remote): Logger.EventData

    companion object {
        suspend fun create(client: Client): BranchUpdateNotifier {
            val userData: UserData = client.userData
            val notifyStatusStorage = client.context.getNotifyStatusStorage(userData.masterKey, userData.storageDir.getCommitSignature())
            return BranchUpdateNotifier(client, notifyStatusStorage)
        }
    }

    private val notificationStore = NotificationStore(notifyStatusStorage)
    private var started = false
    private var fetchBranchIndexJobs: List<Pair<Remote, Future<RemoteIndexJob.Result>>> = emptyList()
    private val watchList: MutableList<Pair<StorageDir, StorageDir.Listener>> = ArrayList()

    inner class Listener(val remote: Remote, val branchIndex: BranchIndex) : StorageDir.Listener {
        override suspend fun onAfterCommit(base: HashValue, newTip: HashValue) {
            client.context.looper.schedule {
                work(remote, branchIndex)
            }
        }
    }

    private suspend fun postUpdate(branchRemote: Remote, contactId: HashValue, branch: HashValue) {
        val contact = client.userData.contacts.list.listContacts().firstOrNull { it.id.get() == contactId } ?: run {
            reportError(EventData("Can't find contact $contactId", branchRemote))
            return
        }

        val userData = client.userData
        val command = BranchUpdateCommand(contactId, userData.id.get(), branchRemote.id, branch)
        client.outQueueManagers.first().outQueue.post(JSON.stringify(command), userData, contact)
    }

    private suspend fun isUpdated(lastNotified: LastNotified, branch: Branch): Boolean {
        val localHead = client.userData.getStorageDir(branch).getHead() ?: return true
        return localHead.value == lastNotified.lastNotifiedHead
    }

    private suspend fun isSynced(branch: Branch, remoteBranchIndex: BranchIndex): Boolean {
        val remoteIndexContent = remoteBranchIndex.list()
        val remoteLogTip = remoteIndexContent[branch.branch.toHex()] ?: return false

        val repo = client.userData.getStorageDir(branch).getBackingDatabase() as Repository
        val localBranchLogTip = repo.branchBackend.getBranchLog().getHead()?.entryId ?: return false

        return remoteLogTip == localBranchLogTip
    }

    /**
     * To be called from the looper
     */
    private suspend fun work(remote: Remote, remoteBranchIndex: BranchIndex) {
        val contextBranchMap = notificationStore.remotes.get(HashValue.fromHex(remote.id))
        contextBranchMap.list().forEach { branchContext ->
            val branchContactMap = contextBranchMap.get(branchContext)
            branchContactMap.list().forEach { branchHex ->
                val branch = HashValue.fromHex(branchHex)
                val contactStatusMap = branchContactMap.get(branch)
                for (contactHex in contactStatusMap.list()) {
                    val contact = HashValue.fromHex(contactHex)
                    val status = contactStatusMap.get(contact).get()

                    val localBranch = client.userData.getBranch(branchContext, branch)
                    if (!isSynced(localBranch, remoteBranchIndex))
                        continue

                    when (status) {
                        is LastNotified -> {
                            if (!isUpdated(status, localBranch))
                                postUpdate(remote, contactId = contact, branch = branch)
                        }
                        is WaitForAccessStore -> {
                            client.userData.getAccessStores().firstOrNull { it.branch == status.accessStore }?.let {
                                if (isSynced(it, remoteBranchIndex)) {
                                    postUpdate(remote, contactId = contact, branch = branch)
                                }
                            }
                        }
                    }
                }
            }
        }


    }

    private suspend fun startWatching(remote: Remote, remoteBranchIndex: BranchIndex) {
        val listener = Listener(remote, remoteBranchIndex)
        remoteBranchIndex.storageDir.addListener(listener)
        watchList.add(remoteBranchIndex.storageDir to listener)

        work(remote, remoteBranchIndex)
    }

    private fun stopWatching() {
        watchList.forEach { it.first.removeListener(it.second) }
        watchList.clear()
    }

    private fun reportInfo(event: EventData) {
        client.logger.onInfo(Logger.Event(Logger.TYPE.BRANCH_UPDATE_NOTIFIER, event))
    }

    private fun reportError(event: EventData) {
        client.logger.onError(Logger.Event(Logger.TYPE.BRANCH_UPDATE_NOTIFIER, event))
    }

    private suspend fun initialWork(list: Collection<Remote>) = async(client.context.coroutineContext) {
        // that's the long running job we don't want to run on the looper:
        fetchBranchIndexJobs = list.map {
            it to client.branchIndexCache.getRemoteIndexBranch(it)
        }

        // back to the looper
        client.withLooper {
            for (pair in fetchBranchIndexJobs) {
                val result = pair.second.await()
                if (result.code != ReturnType.OK) {
                    reportError(EventData(result.message, pair.first))
                    continue
                }

                val remoteIndexDir = client.context.getRemoteIndexStorage(pair.first.id, result.branch)
                val remoteBranchIndex = BranchIndex(remoteIndexDir)
                startWatching(pair.first, remoteBranchIndex)
            }

            fetchBranchIndexJobs = emptyList()
        }
    }

    suspend fun start(list: Collection<Remote>) {
        if (started)
            return
        started = true

        initialWork(list)
    }

    fun stop() {
        if (!started)
            return
        started = false

        fetchBranchIndexJobs.forEach {
            it.second.cancel()
        }
        fetchBranchIndexJobs = emptyList()

        stopWatching()
    }
}