package org.fejoa

import kotlinx.serialization.json.JSON
import kotlinx.serialization.toUtf8Bytes
import org.fejoa.command.*
import org.fejoa.crypto.*
import org.fejoa.network.*
import org.fejoa.repository.sync.AccessRight
import org.fejoa.storage.HashValue
import org.fejoa.support.*
import kotlin.coroutines.experimental.CoroutineContext
import kotlin.coroutines.experimental.suspendCoroutine


class Client private constructor(val context: FejoaContext, val userData: UserData,
                                 val connectionAuthManager: ConnectionAuthManager,
                                 private val syncer: RemoteIndexSyncer,
                                 val branchIndexCache: BranchIndexCache) {

    val logger = DispatcherLogger()

    val outQueueManagers: MutableList<OutQueueManager> = ArrayList()
    val inQueueManagers: MutableList<InQueueManager> = ArrayList()
    private var branchUpdateNotifier: BranchUpdateNotifier? = null

    val contactRequestHandler = ContactRequestHandler(this)
    val grantAccessHandler = GrantAccessHandler(userData)
    val branchUpdateHandler = BranchUpdateHandler(userData)

    companion object {
        private suspend fun construct(userData: UserData,
                                      connectionAuthManager: ConnectionAuthManager = ConnectionAuthManager(userData.context)): Client {
            val branchIndexCache = BranchIndexCache(userData.context, connectionAuthManager)
            val syncer = RemoteIndexSyncer.create(SyncManager(connectionAuthManager, userData.context.passwordGetter),
                    userData, branchIndexCache)

            val client = Client(userData.context, userData, connectionAuthManager, syncer, branchIndexCache)
            client.syncer.logger = client.logger
            return client
        }

        private suspend fun getCoroutineContext(): CoroutineContext {
            return suspendCoroutine { it.resume(it.context) }
        }

        suspend fun create(baseContext: String, namespace: String, password: String,
                           kdf: CryptoSettings.KDF = CryptoSettings.default.kdf): Client {
            val userKeyParams = UserKeyParams(BaseKeyParams(kdf, CryptoHelper.crypto.generateSalt16()),
                    CryptoSettings.HASH_TYPE.SHA256, CryptoSettings.default.symmetric,
                    CryptoHelper.crypto.generateSalt16())

            val context = FejoaContext(AccountIO.Type.CLIENT, baseContext, namespace, getCoroutineContext())
            val userData = UserData.create(context, CryptoSettings.default)
            val userDataSettings = userData.createUserDataConfig(password, userKeyParams)

            val accountIO = context.accountIO
            if (accountIO.exists())
                throw Exception("Account exists")

            accountIO.writeUserDataConfig(userDataSettings)

            return Client.construct(userData)
        }

        suspend fun open(baseContext: String, namespace: String, password: String): Client {
            val context = FejoaContext(AccountIO.Type.CLIENT, baseContext, namespace, getCoroutineContext())
            return open(context, ConnectionAuthManager(context), namespace, password)
        }

        suspend fun open(context: FejoaContext, connectionAuthManager: ConnectionAuthManager, namespace: String,
                         password: String): Client {
            val platformIO = platformGetAccountIO(AccountIO.Type.CLIENT, context.context, namespace)

            val userDataSettings = platformIO.readUserDataConfig()
            val openedSettings = userDataSettings.open(password, context.baseKeyCache)

            val userData = UserData.open(context, openedSettings.first, openedSettings.second.branch)
            return Client.construct(userData, connectionAuthManager)
        }

        suspend fun retrieveAccount(baseContext: String, namespace: String, url: String, user: String,
                                    passwordGetter: PasswordGetter): Client {
            val context = FejoaContext(AccountIO.Type.CLIENT, baseContext, namespace, getCoroutineContext())
            val remote = Remote("no_id", user, url)
            val connectionAuthManager = ConnectionAuthManager(context)

            val reply = connectionAuthManager.send(RetrieveServerConfigJob(user),
                    remote, LoginAuthInfo(), passwordGetter).await()
            if (reply.code != ReturnType.OK)
                throw Exception(reply.message)
            val userDataConfig = reply.serverConfig?.userDataConfig ?: throw Exception("Missing user data config")

            context.accountIO.writeUserDataConfig(userDataConfig)

            val openPassword = passwordGetter.get(PasswordGetter.Purpose.OPEN_ACCOUNT,
                    "$namespace/$user")
            ?: throw Exception("Canceled by user")

            // pull user data branch
            val credentials = userDataConfig.open(openPassword, context.baseKeyCache)
            val userDataDir = context.getStorage(credentials.second.branch, credentials.first, false)
            val results = SyncManager.sync(context, userDataDir, connectionAuthManager, remote,
                    LoginAuthInfo(), passwordGetter).await()

            if (results.code != ReturnType.OK)
                throw Exception("Failed to pull user data from ${remote.server}")

            return open(context, connectionAuthManager, namespace, openPassword)
        }
    }

    fun getSyncManager(): SyncManager {
        return syncer.syncManager
    }

    /**
     * Registers an account at a remote server
     */
    suspend fun registerAccount(user: String, url: String, password: String, userKeyParams: UserKeyParams? = null)
        : Pair<RemoteJob.Result, Remote?> {

        val params = userKeyParams ?: UserKeyParams(
                BaseKeyParams(salt = CryptoHelper.crypto.generateSalt16(), kdf = CryptoSettings.default.kdf),
                CryptoSettings.HASH_TYPE.SHA256, CryptoSettings.default.symmetric, CryptoHelper.crypto.generateSalt16())

        val loginSecret = userData.context.baseKeyCache.getUserKey(params, password).await()
        val loginParams = LoginParams(params,
                CompactPAKE_SHA256_CTR.getSharedSecret(DH_GROUP.RFC5114_2048_256, loginSecret),
                DH_GROUP.RFC5114_2048_256)

        val request = platformCreateHTTPRequest(url)

        val inQueueName = CryptoHelper.generateSha256Id().toHex()
        val accessStore = CryptoHelper.generateSha256Id().toHex()

        val result = RegisterJob(user, loginParams, userData.createServerConfig(password, params,
                inQueueName, accessStore)).run(request)
        if (result.code != ReturnType.OK)
            return result to null

        // add the queue and access store to the user data
        val remote = Remote.create(user, url)
        userData.writeRemote(remote)
        userData.createInQueue(inQueueName, remote)
        userData.createAccessStore(accessStore, remote)
        // TODO restart client...

        return result to remote
    }

    /**
     * Send a request to the given remote.
     *
     * If the contact public key is unknown the request is insecure, i.e. vulnerable against man in the middle attacks.
     *
     * @param contactRemote the contact's remote
     * @param contactAsymEnc the contact's asym encryption key (if known)
     * @param contactVerification the contact's verification key (if known)
     */
    suspend fun sendContactRequest(contactRemote: Remote, contactVerification: PublicKeyData?,
                                   contactAsymEnc: PublicKeyData?, addToRequested: Boolean) {
        val senderSigningCredentials = userData.getSigningCredentials()
        val contactRequest = ContactRequestCommand(userData.id.get(),
                userData.remotes.listRemotes().first(),
                userData.getAsymEncCredentials().asJsonPublicKeyData(),
                senderSigningCredentials.asJsonPublicKeyData())

        outQueueManagers.first().outQueue.post(JSON.stringify(contactRequest), userData, contactRemote, contactAsymEnc)

        if (addToRequested) {
            // save contact request
            val hash = CryptoHelper.sha256Hash()
            hash.write(contactRemote.user.toUtf8Bytes())
            hash.write(contactRemote.server.toUtf8Bytes())
            userData.contacts.requested.get(HashValue(hash.hash())).write(ContactRequest(contactRemote,
                    verificationKey = contactVerification?.asJsonPublicKeyData(),
                    asymEncKey = contactAsymEnc?.asJsonPublicKeyData()))
            userData.commit()
        }

        // There is a race conditions that needs to be considered when the client crashes after the request has been
        // sent but the request hasn't been added to the requested list. In this case, either the client resents the
        // request again or the contact is quicker and approves the requests and sends his details. In the later case
        // the client receives the approval as a contact request and approves/ rejects the request. If the client,
        // rejects the request (even the client sent the initial contact request) this is equivalent of deleting an
        // existing contact from the contact list. In any case the user data stays in a sound state.
    }

    suspend fun grantAccess(contact: Contact, branch: Branch, right: AccessRight) {
        /* 1) Update branch access in the user data
           2) Update the access store(s)
           3) Send access command

           Afterwards:
           4) Wait for access store to be uploaded
           5) Notify contact when branch has been updated
           Step 4 and 5 happen throw the branch update notification manager
        */

        val tokenBuilder = TokenBuilder.create(AccessToken.create(mutableMapOf(branch.branch.toHex() to right)))
        val branchInfo = branch.branchInfo.get()

        // 1)
        val serverToken = tokenBuilder.getServerToken()
        val tokenId = HashValue.fromHex(serverToken.id)
        val contactAccess = branch.contactAccessMap.get(tokenId)
        contactAccess.write(ContactAccess(contact.id.get(), tokenId, tokenBuilder.token))
        userData.commit()

        // 2)
        val remoteIds = branch.locations.listRemoteRefs().map { it.remoteId }
        val accessStoresRemoteRefs = userData.getAccessStores().map { storeBranch ->
            val remotes = storeBranch.locations.listRemoteRefs().filter {
                remoteIds.contains(it.remoteId)
            }
            if (remotes.isEmpty())
                null
            else
            storeBranch to remotes.first()

        }.filterNotNull()
        accessStoresRemoteRefs.forEach {
            val accessStore = AccessStore(userData.getStorageDir(it.first))
            accessStore.token.get(tokenId).write(serverToken)
            accessStore.commit()
        }

        // 3)
        val authToken = tokenBuilder.getContactToken()
        val remoteRefs = accessStoresRemoteRefs.map {
            val remoteRef = it.second
            RemoteRef.TokenRemoteRefIO(remoteRef.remoteId, authToken)
        }.toMutableList()
        val command = GrantAccessCommand(GrantAccessCommand.NAME, userData.id.get(), branch.context, branchInfo,
                remoteRefs)
        outQueueManagers.first().outQueue.post(JSON.stringify(command), userData, contact)

        // update notification map
        branchUpdateNotifier?.notifyStatusStorage?.let {
            val store = NotificationStore(it)
            val remotes = accessStoresRemoteRefs.map {
                it.first to userData.remotes.get(HashValue.fromHex(it.second.remoteId)).get()
            }
            remotes.forEach {
                val accessStoreBranch = it.first.branch
                val remote = it.second
                store.updateNotifyStatus(remote, branch.context, branch.branch, contact.id.get(),
                        WaitForAccessStore(accessStoreBranch))
            }
        }
    }

    suspend fun close() {
        userData.close()
    }

    /**
     * Set a branch location for all branches in the given context
     */
    suspend fun setBranchLocation(remote: Remote, authInfo: AuthInfo, context: String) {
        userData.getBranches(context, true).forEach {
             it.updateRemote(remote, authInfo)
        }
        if (syncer.isSyncing())
            syncer.startSyncing(userData.remotes.listRemotes())
    }

    suspend fun start() {
        val remotes = userData.remotes.listRemotes()
        syncer.startSyncing(remotes)

        userData.getOutQueues().forEach {
            val manager = OutQueueManager(connectionAuthManager, OutQueue(userData.getStorageDir(it)), logger)
            manager.start()
            outQueueManagers.add(manager)
        }

        userData.getInQueues().forEach {
            val manager = InQueueManager(context, InQueue(userData.getStorageDir(it)), logger, {
                val dbValue = userData.keys.asymEncKeys.get(it)
                if (!dbValue.exists()) null else dbValue.get().getPrivateKey()
            }, { signer, keyId ->
                val keyDBValue = userData.contacts.list.get(signer).keys.verificationKeys.get(keyId)
                if (!keyDBValue.exists())
                    null
                else
                keyDBValue.get().asPublicKeyData()
            })
            manager.addCommandHandler(contactRequestHandler)
            manager.addCommandHandler(grantAccessHandler)
            manager.addCommandHandler(branchUpdateHandler)
            manager.start()
            inQueueManagers.add(manager)
        }

        branchUpdateNotifier = BranchUpdateNotifier.create(this)
        branchUpdateNotifier?.start(remotes)
    }

    fun stop() {
        syncer.stopSyncing()

        outQueueManagers.forEach { it.stop() }
        outQueueManagers.clear()

        inQueueManagers.forEach { it.stop() }
        inQueueManagers.clear()

        branchUpdateNotifier?.stop()
        branchUpdateNotifier = null
    }
}

suspend fun <T>Client.withLooper(block: suspend Client.() -> T): T {
    return context.looper.runSync {
        block.invoke(this)
    }
}