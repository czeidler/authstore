package org.fejoa

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON
import org.fejoa.crypto.*
import org.fejoa.storage.*
import org.fejoa.support.PathUtils
import org.fejoa.support.decodeBase64
import org.fejoa.support.encodeBase64


/**
 * @param key base64 encoded raw key
 */
@Serializable
class JsonSecretKeyData(val key: String, val algo: CryptoSettings.SYM_ALGO) {
    suspend fun getSecretKey(): SecretKey {
        return CryptoHelper.crypto.secretKeyFromRaw(key.decodeBase64(), algo)
    }

    suspend fun asSymBaseCredentials(): SecretKeyData {
        return SecretKeyData(getSecretKey(), algo)
    }
}

suspend fun SecretKeyData.asJsonSecretKeyData(): JsonSecretKeyData {
     return JsonSecretKeyData(CryptoHelper.crypto.encode(key).encodeBase64(), algo)
}

interface AsymKey {
    val publicKey: String

    suspend fun getId(): HashValue {
        val raw = publicKey.decodeBase64()
        return HashValue(CryptoHelper.sha256Hash(raw))
    }
}

@Serializable
data class JsonPublicKeyData(override val publicKey: String, val algo: CryptoSettings.ASYM_ALGO) : AsymKey

suspend fun JsonPublicKeyData.asPublicKeyData(): PublicKeyData {
    val public = CryptoHelper.crypto.publicKeyFromRaw(publicKey.decodeBase64(), algo)
    return PublicKeyData(public, algo)
}

suspend fun KeyPairData.asJsonPublicKeyData(): JsonPublicKeyData {
    val crypto = CryptoHelper.crypto
    val public = crypto.encode(keyPair.publicKey).encodeBase64()
    return JsonPublicKeyData(public, algo)
}

suspend fun KeyPairData.asJsonPrivateKeyData(): JsonPrivateKeyData {
    val crypto = CryptoHelper.crypto
    val public = crypto.encode(keyPair.privateKey).encodeBase64()
    return JsonPrivateKeyData(public, algo)
}

suspend fun PublicKeyData.asJsonPublicKeyData(): JsonPublicKeyData {
    val crypto = CryptoHelper.crypto
    val public = crypto.encode(publicKey).encodeBase64()
    return JsonPublicKeyData(public, algo)
}

@Serializable
data class JsonPrivateKeyData(val privateKey: String, val algo: CryptoSettings.ASYM_ALGO)

suspend fun JsonPrivateKeyData.asPrivateKeyData(): PrivateKeyData {
    val private = CryptoHelper.crypto.privateKeyFromRaw(privateKey.decodeBase64(), algo)
    return PrivateKeyData(private, algo)
}

@Serializable
class JsonKeyPairData(override val publicKey: String, val privateKey: String, val algo: CryptoSettings.ASYM_ALGO) : AsymKey {
    companion object {
        suspend fun create(keyPair: KeyPair, algo: CryptoSettings.ASYM_ALGO) : JsonKeyPairData {
            val crypto = CryptoHelper.crypto
            val public = crypto.encode(keyPair.publicKey)
            val private = crypto.encode(keyPair.privateKey)
            return JsonKeyPairData(public.encodeBase64(), private.encodeBase64(), algo)
        }
    }

    suspend fun getAsymCredentials(): KeyPairData {
        val crypto = CryptoHelper.crypto
        val public = crypto.publicKeyFromRaw(publicKey.decodeBase64(), algo)
        val private = crypto.privateKeyFromRaw(privateKey.decodeBase64(), algo)
        return KeyPairData(KeyPair(public, private), algo)
    }

    suspend fun getPublicKey(): PublicKey {
        return CryptoHelper.crypto.publicKeyFromRaw(publicKey.decodeBase64(), algo)
    }

    suspend fun getPrivateKey(): PrivateKey {
        return CryptoHelper.crypto.privateKeyFromRaw(privateKey.decodeBase64(), algo)
    }
}


class DBJsonPublicKeyData(dir: IOStorageDir, path: String) : DBValue<JsonPublicKeyData>(dir, path) {
    constructor(parent: DBObject, relativePath: String)
            : this(parent.dir, PathUtils.appendDir(parent.path, relativePath))

    override suspend fun write(obj: JsonPublicKeyData) {
        dir.writeString(path, JSON.stringify(obj))
    }

    override suspend fun get(): JsonPublicKeyData {
        return JSON.parse(dir.readString(path))
    }
}

class DBMapJsonPublicKeyData(dir: IOStorageDir, path: String) : DBMap<HashValue, DBJsonPublicKeyData>(dir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listFiles(path)
    }

    override fun get(key: HashValue): DBJsonPublicKeyData {
        return DBJsonPublicKeyData(this, key.toHex())
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }

    suspend fun listKeys(): Collection<JsonPublicKeyData> {
        return list().map { get(HashValue.fromHex(it)).get() }
    }
}


class DBJsonKeyPairData(dir: IOStorageDir, path: String) : DBValue<JsonKeyPairData>(dir, path) {
    constructor(parent: DBObject, relativePath: String)
            : this(parent.dir, PathUtils.appendDir(parent.path, relativePath))

    override suspend fun write(obj: JsonKeyPairData) {
        dir.writeString(path, JSON.stringify(obj))
    }

    override suspend fun get(): JsonKeyPairData {
        return JSON.parse(dir.readString(path))
    }
}

class DBMapJsonKeyPairData(dir: IOStorageDir, path: String) : DBMap<HashValue, DBJsonKeyPairData>(dir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listFiles(path)
    }

    override fun get(key: HashValue): DBJsonKeyPairData {
        return DBJsonKeyPairData(this, key.toHex())
    }

    override suspend fun remove(key: HashValue) {
        dir.remove(PathUtils.appendDir(path, key.toHex()))
    }

    suspend fun listKeys(): Collection<JsonKeyPairData> {
        return list().map { get(HashValue.fromHex(it)).get() }
    }
}

