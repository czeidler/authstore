package org.fejoa

import org.fejoa.repository.sync.AccessRight
import org.fejoa.support.toUTF
import org.fejoa.test.testAsync
import kotlin.test.Test
import kotlin.test.assertTrue


class AuthTokenTest {
    @Test
    fun testAuthToken() = testAsync {
        val accessRights: MutableMap<String, AccessRight> = HashMap()
        accessRights["testBranch"] = AccessRight.PULL
        val accessToken = AccessToken.create(accessRights)

        val builder = TokenBuilder.create(accessToken)
        val contactToken = builder.getContactToken()
        val serverToken = builder.getServerToken()

        val authToken = "testTokenForAuthentication".toUTF()

        // check that the access token is valid, i.e. matches the the server token
        assertTrue(serverToken.validate(accessToken))

        // check that the contact knows the private signing key
        val signature = contactToken.sign(authToken)
        assertTrue(serverToken.authenticate(authToken, signature))
    }
}