package org.fejoa.command

import org.fejoa.chunkcontainer.BoxSpec
import org.fejoa.crypto.CryptoHelper
import org.fejoa.crypto.CryptoSettings
import org.fejoa.crypto.KeyPairData
import org.fejoa.support.toUTF
import org.fejoa.test.testAsync
import kotlin.test.Test
import kotlin.test.assertTrue


class EnvelopeTest {
    @Test
    fun testBasics() = testAsync {
        val config = CryptoSettings.default
        val signingKeys = CryptoHelper.crypto.generateKeyPair(config.signature)
        val signerId = "Signer".toUTF()
        val data = "Test data 1234 and some compressible test data 1234 1234 1234 1234".toUTF()
        val plainEnvelope = PlainEnvelope.pack(data, signerId,
                KeyPairData(signingKeys, config.signature.algo), BoxSpec.ZipType.DEFLATE)

        // test deflate
        assertTrue { plainEnvelope.content.size < data.size }

        var envelopedData = plainEnvelope.getData()
        assertTrue { envelopedData contentEquals data }

        val valid = plainEnvelope.verify(signingKeys.publicKey)
        assertTrue(valid)

        // sym encryption
        val symKey = CryptoHelper.crypto.generateSymmetricKey(config.symmetric)
        val symEnvelope = SymEncEnvelope.pack(plainEnvelope.toProtoBuffer().toByteArray(),
                "keyId".toUTF(), symKey, config.symmetric.algo)

        val symEnvClone = Envelope.read(symEnvelope.toProtoBuffer().toByteArray()) as SymEncEnvelope
        val symDecrypted = Envelope.read(symEnvClone.decrypt(symKey)) as PlainEnvelope
        envelopedData = symDecrypted.getData()
        assertTrue { envelopedData contentEquals data }

        // asym encryption
        // We can only encrypt a limited amount of data using RSA. The asym envelope is suppose to encrypt sym keys. Try
        // a very long sym key:
        val fakeSymKey = ByteArray(64)
        val symKeyPlain = PlainEnvelope.pack(fakeSymKey, ByteArray(0), null)
        val asymKey = CryptoHelper.crypto.generateKeyPair(config.asymmetric)
        val asymEnvelope = AsymEncEnvelope.pack(symKeyPlain.toProtoBuffer().toByteArray(),
                asymKey.publicKey, config.asymmetric.algo)
        val asymEnvClone = Envelope.read(asymEnvelope.toProtoBuffer().toByteArray()) as AsymEncEnvelope
        envelopedData = (Envelope.read(asymEnvClone.decrypt(asymKey.privateKey)) as PlainEnvelope).getData()
        assertTrue { envelopedData contentEquals fakeSymKey }
    }
}