package org.fejoa.server

import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import kotlinx.coroutines.experimental.suspendCancellableCoroutine
import kotlinx.coroutines.experimental.withTimeout
import org.fejoa.*
import org.fejoa.crypto.*
import org.fejoa.network.*
import org.fejoa.repository.sync.AccessRight

import java.io.File
import java.util.ArrayList

import org.fejoa.server.JettyServer.Companion.DEFAULT_PORT
import org.fejoa.server.JettyServer.Companion.FEJOA_PORTAL_PATH
import org.fejoa.storage.HashValue
import org.fejoa.support.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

// TODO this doesn't compile for JS at the moment but should be moved to the Future class
// see: https://discuss.kotlinlang.org/t/cant-compile-invokeoncompletion-with-oncancelling-option/6852
suspend fun <T> Future<T>.awaitCancellable() = suspendCancellableCoroutine<T> { cont ->
    cont.invokeOnCompletion(onCancelling = true) {
        cancel()
    }
    whenCompleted({value, exception ->
        if (exception != null)
            cont.resumeWithException(exception)
        else // cast to T in case T is nullable and is actually null
            cont.resume(value as T)
    })
}

class ClientTest {
    companion object {
        internal val TEST_DIR = "jettyTest"
        internal val SERVER_TEST_DIR_1 = TEST_DIR + "/Server1"
        internal val SERVER_TEST_DIR_2 = TEST_DIR + "/Server2"
    }

    private val cleanUpDirs: MutableList<String> = ArrayList()
    private var server1: JettyServer? = null
    private val port1 = DEFAULT_PORT
    private val url1 = "http://localhost:$port1/$FEJOA_PORTAL_PATH"
    private var server2: JettyServer? = null
    private val port2 = DEFAULT_PORT + 1
    private val url2 = "http://localhost:$port2/$FEJOA_PORTAL_PATH"

    private fun setUpServer(dir: String, port: Int): JettyServer {
        val server = JettyServer(dir, port = port)
        server.setDebugNoAccessControl(true)
        server.start()
        return server
    }

    @Before
    fun setUp() {
        cleanUpDirs.add(TEST_DIR)

        server1 = setUpServer(SERVER_TEST_DIR_1, port1)
        server2 = setUpServer(SERVER_TEST_DIR_2, port2)
    }

    @After
    fun tearDown() {
        Thread.sleep(1000)
        server1!!.stop()
        server2!!.stop()

        for (dir in cleanUpDirs)
            File(dir).deleteRecursively()
    }

    private suspend fun waitForBranchesSynced(logger: DefaultLogger, branches: List<String>): Boolean {
        (1 .. 100).forEach {
            val syncedBranches = logger.getLogTypeMap(Logger.Category.INFO)
                    .getEvents(Logger.TYPE.SYNC_MANAGER)
                    .map { it.first.data as SyncEvent }
                    .filter { branches.contains(it.branch)
                            && it.context == SyncEvent.Context.BRANCH
                            && it.status == SyncEvent.Status.SYNCED
                    }

            if (syncedBranches.size >= branches.size) {
                val done = branches.none { branch -> syncedBranches.find { it.branch == branch } == null }
                if (done)
                    return true
            }
            delay(50)
        }
        return false
    }

    private suspend fun setupClient(serverDir: String, serverUrl: String, clientDir: String, user: String,
                                    password: String, logger: Logger): Client {
        val client = Client.create(clientDir, user, password)

        client.userData.context.passwordGetter = object : PasswordGetter {
            override suspend fun get(purpose: PasswordGetter.Purpose, resource: String, info: String): String {
                return password
            }
        }
        val registerResult = client.registerAccount(user, serverUrl, password)
        assertTrue(registerResult.first.code == ReturnType.OK)

        val serverAccountIO = platformGetAccountIO(AccountIO.Type.SERVER, serverDir, user)
        assertTrue(serverAccountIO.exists())

        // start syncing (nothing to do yet)
        client.logger.children += logger

        // sync client1 with server
        val remote = registerResult.second!!
        client.setBranchLocation(remote, LoginAuthInfo(),  UserData.USER_DATA_CONTEXT)
        client.userData.commit()
        return client
    }

    @Test
    fun testClient() = runBlocking {
        val clientDir = PathUtils.appendDir(TEST_DIR, "testClient")

        val log: MutableList<String> = ArrayList()
        class MergedLogger(val name: String) : Logger {
            override fun onEvent(category: Logger.Category, event: Logger.Event, level: Logger.LogLevel) {
                log += "$name: $category $event"
            }
        }

        val logger1 = DefaultLogger()
        val client1 = setupClient(SERVER_TEST_DIR_1, url1, clientDir, "user1", "password1", logger1)
        client1.logger.children += MergedLogger("Client1")
        client1.start()

        val logger2 = DefaultLogger()
        val client2 = setupClient(SERVER_TEST_DIR_2, url2, clientDir, "user2", "password2", logger2)
        client2.logger.children += MergedLogger("Client2")
        client2.start()

        try {
            val client1Done = waitForBranchesSynced(logger1, listOf(client1.userData.branch,
                    *client1.userData.getInQueues().map { it.branchInfo.get().branch }.toTypedArray(),
                    *client1.userData.getOutQueues().map { it.branchInfo.get().branch }.toTypedArray()))
            assertTrue(client1Done)

            val client2Done = waitForBranchesSynced(logger2, listOf(client2.userData.branch,
                    *client2.userData.getInQueues().map { it.branchInfo.get().branch }.toTypedArray(),
                    *client2.userData.getOutQueues().map { it.branchInfo.get().branch }.toTypedArray()))
            assertTrue(client2Done)

            client2.withLooper {
                contactRequestHandler.handleManager.addListener {
                    // auto accept
                    client2.context.looper.schedule {
                        it.accept()
                    }
                }
            }

            client1.withLooper {
                sendContactRequest(client2.userData.remotes.listRemotes().first(), null, null, true)
            }

            var contactExist = false
            for (i in 0..500) {
                client1.withLooper {
                    if (userData.contacts.list.get(client2.userData.id.get()).id.exists())
                        contactExist = true
                }
                if (contactExist)
                    break
                delay(50)
            }
            assertTrue(contactExist)

            assertTrue(client2.withLooper {
                userData.contacts.list.get(client1.userData.id.get()).id.exists()
            })

            // create a branch and grant access to a contact
            val storageContext = "org.fejoa.storage"
            val storageBranchName = UserData.generateBranchName()
            client1.withLooper {
                val keyData = CryptoSettings.default.symmetric.generateSecretKeyData()
                val storageBranch = userData.createStorageDir(storageContext, storageBranchName.toHex(),
                        "data branch", keyData)
                val remote = userData.remotes.listRemotes().first()
                storageBranch.updateRemote(RemoteRef(remote.id, LoginAuthInfo()))
                userData.commit()

                val contact = userData.contacts.list.listContacts().first()

                grantAccess(contact, storageBranch, AccessRight.PULL_PUSH)
            }

            val branchAcceptedFuture = Future<Boolean>()
            client2.grantAccessHandler.handles.addListener {
                // auto accept
                client2.context.looper.schedule {
                    it.accept()
                    branchAcceptedFuture.setResult(true)
                }
            }

            withTimeout(5000L) {
                branchAcceptedFuture.awaitCancellable()
            }

            client2.withLooper {
                val contactClient1 = userData.contacts.list.listContacts().first()
                val remoteRefs = contactClient1.branches.get(storageContext).get(storageBranchName)
                        .locations.listRemoteRefs()
                assertTrue(remoteRefs.first().authInfo.type == AuthType.TOKEN)
            }

            val branchUpdateReceivedFuture = Future<Boolean>()
            client2.branchUpdateHandler.handles.addListener {
                assertTrue(it.command.branch == storageBranchName)
                branchUpdateReceivedFuture.setResult(true)
            }

            withTimeout(5000L) {
                branchUpdateReceivedFuture.awaitCancellable()
            }

            // test pulling the shared branch
            val syncJob = client2.withLooper {
                val contact = userData.contacts.list.listContacts().first()
                val branchMap =contact.branches.get(storageContext)
                val branchName = branchMap.list().first()
                val branch = branchMap.get(HashValue.fromHex(branchName))
                val remoteRef = branch.locations.listRemoteRefs().first()
                val branchRemote = contact.remotes.listRemotes().firstOrNull {
                    it.id == remoteRef.remoteId
                } ?: throw Exception("Unknown remote")

                val storageDir = userData.getStorageDir(branch, false)
                val syncManager = getSyncManager()
                syncManager.sync(userData.context, storageDir, branchRemote, remoteRef.authInfo)
            }

            val result = syncJob.await()
            assertTrue { result.code == ReturnType.OK}
        } finally {
            client1.withLooper { stop() }
            client2.withLooper { stop() }
            // keep the coroutine alive for a bit so that we can finish
            delay(500)
        }
    }
}

