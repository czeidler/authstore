package org.fejoa.server

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.sync.Mutex
import kotlinx.coroutines.experimental.sync.withLock
import org.fejoa.AccountIO
import org.fejoa.FejoaContext
import org.fejoa.ServerAccessTracker
import org.fejoa.crypto.CompactPAKE_SHA256_CTR
import org.fejoa.crypto.CryptoHelper
import org.fejoa.repository.sync.TransactionManager
import org.fejoa.storage.StorageDir
import org.fejoa.support.Locker
import javax.servlet.http.HttpSession


class TokenAuthManager {
    class AuthRequest(val tokenId: String, val authToken: ByteArray)

    private val ongoing: MutableMap<String, AuthRequest> = HashMap()

    fun initAuthRequest(tokenId: String): AuthRequest {
        val authRequest = AuthRequest(tokenId, CryptoHelper.crypto.generateSalt16())
        ongoing[tokenId] = authRequest
        return authRequest
    }

    fun finishAuthRequest(tokenId: String): AuthRequest? {
        return ongoing.remove(tokenId)
    }
}

class Session(val baseDir: String, private val session: HttpSession) {
    private val ROLES_KEY = "roles"
    private val LOGIN_COMPACTPAKE_PROVER_KEY = "loginCompactPAKEProver"
    private val TOKEN_AUTH_PROTOCOL_HANDLER = "tokenAuthProtocolHandler"

    val transactionManager = TransactionManager()

    private fun getSessionLock(): Any {
        // get an immutable lock
        return session.id.intern()
    }

    private fun getLoginCompactPAKEProverKey(user: String): String {
        return user + ":" + LOGIN_COMPACTPAKE_PROVER_KEY
    }

    fun setLoginCompactPAKEProver(user: String, prover: CompactPAKE_SHA256_CTR.ProverState0?) {
        session.setAttribute(getLoginCompactPAKEProverKey(user), prover)
    }

    fun getLoginCompactPAKEProver(user: String): CompactPAKE_SHA256_CTR.ProverState0? {
        return session.getAttribute(getLoginCompactPAKEProverKey(user))?.let { it as CompactPAKE_SHA256_CTR.ProverState0}
    }

    fun getTokenAuthManager(user: String): TokenAuthManager {
        val key = user + ":" + TOKEN_AUTH_PROTOCOL_HANDLER
        return session.getAttribute(key)?.let { it as TokenAuthManager}
                ?: TokenAuthManager().also {
                    session.setAttribute(key, it)
                }
    }

    fun getServerAccessManager(): ServerAccessTracker {
        return session.getAttribute(ROLES_KEY)?.let { it as ServerAccessTracker }
                ?: return ServerAccessTracker(DebugSingleton.isNoAccessControl).also { session.setAttribute(ROLES_KEY, it) }
    }

    fun getContext(user: String): FejoaContext {
        return FejoaContext(AccountIO.Type.SERVER, baseDir, user, CommonPool, object : Locker {
            val mutex = Mutex()
            override suspend fun <T> runSync(block: suspend () -> T): T {
                return mutex.withLock { block.invoke() }
            }
        })
    }

    suspend fun getBranchIndex(user: String): StorageDir {
        return getContext(user).getLocalIndexStorage()
    }
}
