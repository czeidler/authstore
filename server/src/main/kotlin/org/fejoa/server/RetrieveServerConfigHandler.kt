package org.fejoa.server

import kotlinx.coroutines.experimental.runBlocking
import kotlinx.serialization.serializer
import org.fejoa.AccountIO
import org.fejoa.network.JsonRPCRequest
import org.fejoa.network.RetrieveServerConfigJob
import org.fejoa.network.ReturnType
import org.fejoa.platformGetAccountIO
import java.io.InputStream


class RetrieveServerConfigHandler : JsonRequestHandler(RetrieveServerConfigJob.METHOD) {
    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?,
                        session: Session) = runBlocking {

        val request = JsonRPCRequest.parse(RetrieveServerConfigJob.RPCParams::class.serializer(), json)
        val params = request.params

        if (!session.getServerAccessManager().hasAccountAccess(params.user)) {
            val response = request.makeError(ReturnType.ACCESS_DENIED, "User ${params.user} not authenticated")
            return@runBlocking responseHandler.setResponseHeader(response)
        }

        val userDataConfig = platformGetAccountIO(AccountIO.Type.SERVER, session.baseDir, params.user).readServerConfig()

        val response = request.makeResponse(RetrieveServerConfigJob.PPCResult(userDataConfig))
                .stringify(RetrieveServerConfigJob.PPCResult::class.serializer())
        responseHandler.setResponseHeader(response)
    }
}