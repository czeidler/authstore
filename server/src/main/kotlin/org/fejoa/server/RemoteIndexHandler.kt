package org.fejoa.server

import kotlinx.coroutines.experimental.runBlocking
import kotlinx.serialization.serializer
import org.fejoa.network.JsonRPCRequest
import org.fejoa.network.RemoteIndexJob
import org.fejoa.network.ReturnType
import java.io.InputStream


class RemoteIndexRequestHandler : JsonRequestHandler(RemoteIndexJob.METHOD) {

    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?, session: Session)
            = runBlocking {
        val request = JsonRPCRequest.parse(RemoteIndexJob.RPCParams::class.serializer(), json)
        val params = request.params


        if (!session.getServerAccessManager().hasAccountAccess(params.user)) {
            responseHandler.setResponseHeader(request.makeError(ReturnType.ACCESS_DENIED, "insufficient rights"))
            return@runBlocking
        }

        val branchName = UserThreadContext.withLock(params.user) {
            val index = session.getBranchIndex(params.user)
            index.branch
        }
        val response = request.makeResponse(RemoteIndexJob.RPCResult(branchName))
                .stringify(RemoteIndexJob.RPCResult::class.serializer())
        responseHandler.setResponseHeader(response)
    }
}
