package org.fejoa.server

import org.apache.http.entity.ContentType
import org.apache.http.entity.mime.HttpMultipartMode
import org.apache.http.entity.mime.MultipartEntityBuilder
import org.eclipse.jetty.server.Request
import org.fejoa.network.*
import org.fejoa.support.toUTFString
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest

import javax.servlet.MultipartConfigElement
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.io.*
import java.util.ArrayList
import javax.servlet.http.HttpServlet


class Portal(private val baseDir: String) : HttpServlet() {
    private val jsonHandlers = ArrayList<JsonRequestHandler>()

    init {
        addJsonHandler(JsonPingHandler())
        addJsonHandler(RegisterHandler())
        addJsonHandler(LoginHandler())
        addJsonHandler(LogoutHandler())
        addJsonHandler(AuthStatusHandler())
        addJsonHandler(RetrieveServerConfigHandler())
        addJsonHandler(BranchRequestHandler())
        addJsonHandler(WatchHandler())
        addJsonHandler(RemoteIndexRequestHandler())
        addJsonHandler(CommandHandler())
        addJsonHandler(TokenAuthHandler())
    }

    inner class ResponseHandler(private val response: HttpServletResponse) {
        var isHandled = false
            private set
        private val builder = MultipartEntityBuilder.create()
        // TODO don't buffer the output data but send it directly!
        private var outputStream: ByteArrayOutputStream? = null

        init {
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
        }

        fun setResponseHeader(header: String) {
            isHandled = true
            builder.addTextBody(HTTPRequestMultipartKeys.REQUEST_KEY, header, ContentType.DEFAULT_TEXT)
        }

        fun addData(): OutputStream {
            if (!isHandled)
                throw Exception("Unhandled")
            return outputStream ?: ByteArrayOutputStream().also { outputStream = it }
        }

        @Throws(IOException::class)
        fun finish() {
            if (outputStream != null) {
                builder.addBinaryBody(HTTPRequestMultipartKeys.DATA_KEY,
                        ByteArrayInputStream(outputStream!!.toByteArray()))
            }
            val entity = builder.build()
            response.contentType = entity.contentType.toString()
            entity.writeTo(response.outputStream)
        }
    }

    private fun addJsonHandler(handler: JsonRequestHandler) {
        jsonHandlers.add(handler)
    }

    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        handle(req, resp)
    }

    override fun doPost(req: HttpServletRequest, resp: HttpServletResponse) {
        handle(req, resp)
    }

    fun handle(httpServletRequest: HttpServletRequest,
                        response: HttpServletResponse) {
        response.status = HttpServletResponse.SC_OK

        val MULTI_PART_CONFIG = MultipartConfigElement(System.getProperty("java.io.tmpdir"))
        if (httpServletRequest.contentType != null && httpServletRequest.contentType.startsWith("multipart/form-data")) {
            httpServletRequest.setAttribute(Request.__MULTIPART_CONFIG_ELEMENT, MULTI_PART_CONFIG)
        }

        val request = DefaultMultipartHttpServletRequest(httpServletRequest)

        val session = Session(baseDir, httpServletRequest.session)
        val responseHandler = ResponseHandler(response)

        val messagePart = request.getPart(HTTPRequestMultipartKeys.REQUEST_KEY)
        val data = request.getPart(HTTPRequestMultipartKeys.DATA_KEY)

        if (messagePart == null) {
            responseHandler.setResponseHeader("empty request!")
            responseHandler.finish()
            return
        }

        val stringWriter = ByteArrayOutputStream()
        messagePart.inputStream.copyTo(stringWriter)

        val error = handleJson(responseHandler, stringWriter.toByteArray().toUTFString(),
                data?.inputStream, session)

        if (!responseHandler.isHandled || error != null)
            responseHandler.setResponseHeader(error!!)

        responseHandler.finish()
    }


    private fun handleJson(responseHandler: ResponseHandler, message: String, data: InputStream?, session: Session): String? {
        val request = try {
            JsonRPCMethodRequest.parse(message)
        } catch (e: Exception) {
            return JsonRPCMethodRequest.makeError(-1, ReturnType.INVALID_JSON_REQUEST, "can't parse json")
        }
        for (handler in jsonHandlers) {
            if (handler.method != request.method)
                continue

            try {
                handler.handle(responseHandler, message, data, session)
            } catch (e: Exception) {
                e.printStackTrace()
                return request.makeError(ReturnType.EXCEPTION, e.message ?: "")
            }

            if (responseHandler.isHandled)
                return null
        }

        return request.makeError(ReturnType.NO_HANDLER_FOR_REQUEST, "can't handle request")
    }
}