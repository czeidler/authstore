package org.fejoa.server

import kotlinx.coroutines.experimental.runBlocking
import kotlinx.serialization.internal.StringSerializer
import kotlinx.serialization.serializer
import org.fejoa.command.InQueue
import org.fejoa.network.CommandJob
import org.fejoa.network.JsonRPCRequest
import org.fejoa.network.ReturnType
import org.fejoa.repository.Repository
import java.io.InputStream

class CommandHandler : JsonRequestHandler(CommandJob.METHOD) {
    override fun handle(responseHandler: Portal.ResponseHandler, json: String, data: InputStream?,
                        session: Session) = runBlocking {
        val request = JsonRPCRequest.parse(CommandJob.Params::class.serializer(), json)
        val params = request.params

        if (data == null) {
            val response = request.makeError(ReturnType.DATA_STREAM_MISSING_REQUEST, "No command data")
            return@runBlocking responseHandler.setResponseHeader(response)
        }

        val command = CommandJob.CommandData.read(data.readAll())

        val context = session.getContext(params.user)
        val response = UserThreadContext.withLock(params.user) {
            val config = context.accountIO.readServerConfig()
            val inQueueDir = context.getStorage(config.inQueue, null, false)
            if ((inQueueDir.getBackingDatabase() as Repository).branchBackend.getBranchLog().getHead() == null)
                return@withLock request.makeError(ReturnType.ERROR, "Inqueue not found")

            val inQueue = InQueue(inQueueDir)
            inQueue.add(System.nanoTime(), command.command, command.receiverKey)

            request.makeResponse("Command received").stringify(StringSerializer)
        }

        responseHandler.setResponseHeader(response)
    }
}
