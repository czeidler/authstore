package org.fejoa.server

import org.apache.commons.cli.*
import org.eclipse.jetty.server.Handler
import org.eclipse.jetty.server.Server

import java.io.File
import java.net.InetSocketAddress
import org.eclipse.jetty.server.handler.ContextHandlerCollection
import org.eclipse.jetty.servlet.DefaultServlet
import org.eclipse.jetty.servlet.ServletContextHandler
import org.eclipse.jetty.servlet.ServletHolder
import org.eclipse.jetty.util.resource.Resource


object DebugSingleton {
    var isNoAccessControl = false
}

internal class JettyServer(baseDir: String, host: String? = null, port: Int = DEFAULT_PORT) {
    private val server: Server

    constructor(baseDir: String, port: Int) : this(baseDir, null, port)

    init {
        println(File(baseDir).absolutePath)
        if (host == null)
            server = Server(port)
        else
            server = Server(InetSocketAddress(host, port))

        val fejoaHandler = ServletContextHandler(ServletContextHandler.SESSIONS)
        fejoaHandler.contextPath = "/"
        fejoaHandler.addServlet(ServletHolder(Portal(baseDir)), "/$FEJOA_PORTAL_PATH")

        val resourceContext = resourceHandler()

        val contexts = ContextHandlerCollection()
        contexts.handlers = arrayOf(fejoaHandler, resourceContext)
        server.handler = contexts
    }

    private fun resourceHandler(): Handler {
        val url = JettyServer::class.java.classLoader.getResource("web/")

        val context = ServletContextHandler(ServletContextHandler.SESSIONS)
        context.contextPath = "/web"
        context.baseResource = Resource.newResource(url.toURI())

        val holder = ServletHolder("default", DefaultServlet::class.java)
        context.addServlet(holder, "/")
        return context
    }

    @Throws(Exception::class)
    fun start() {
        server.start()
    }

    @Throws(Exception::class)
    fun stop() {
        server.stop()
        server.join()
    }

    fun setDebugNoAccessControl(noAccessControl: Boolean) {
        DebugSingleton.isNoAccessControl = noAccessControl
    }

    companion object {
        val FEJOA_PORTAL_PATH = "fejoa-portal"
        val DEFAULT_PORT = 8180

        @Throws(Exception::class)
        @JvmStatic
        fun main(args: Array<String>) {
            val options = Options()

            val input = Option("h", "host", true, "Host ip address")
            input.isRequired = true
            options.addOption(input)

            val output = Option("p", "port", true, "Host port")
            output.isRequired = true
            options.addOption(output)

            val dirOption = Option("d", "directory", true, "Storage directory")
            dirOption.isRequired = false
            options.addOption(dirOption)

            val cmd: CommandLine
            try {
                cmd = DefaultParser().parse(options, args)
            } catch (e: ParseException) {
                println(e.message)
                HelpFormatter().printHelp("Fejoa Server", options)

                System.exit(1)
                return
            }

            val host = cmd.getOptionValue("host")
            val port: Int?
            try {
                port = Integer.parseInt(cmd.getOptionValue("port"))
            } catch (e: NumberFormatException) {
                println("Port must be a number")
                System.exit(1)
                return
            }

            var directory = "."
            if (cmd.hasOption("directory"))
                directory = cmd.getOptionValue("directory")

            val server = JettyServer(directory, host, port)
            server.start()
        }
    }
}

