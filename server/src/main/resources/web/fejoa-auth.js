function FejoaAuth() {
    this.authListeners = new Array();

    var that = this;
    window.addEventListener("message", function(event) {
        // only accept messages from ourselves
        if (event.source != window)
            return;

        if (event.data.type && (event.data.type == "fejoa_auth_update"))
            that._notifyAuthStatusChanged(event.data);
    }, false);

    this._notifyAuthStatusChanged = function(data) {
        for (var i = 0; i < this.authListeners.length; i++) {
            var listener = this.authListeners[i];
            listener(data);
        }
    }

    /*
    The listener must be of the form function(object data).
    The data has the fields:
        user:       the current user or an empty string
        authStatus: logged_in|logged_out|logging_in|registering
     */
    this.addAuthStatusListener = function(listener) {
    	if (typeof(listener) != "function" || this.authListeners.indexOf(listener) >= 0)
    		return;
        this.authListeners.push(listener);
    }

    this.removeAuthStatusListener = function(listener) {
        var index = this.authListeners.indexOf(listener);
        if (index < 0)
            return;
        this.authListeners.splice(index, 1)
    }
}
