package org.fejoa.network

import org.fejoa.jsbindings.MultiPart_parse
import org.fejoa.support.*
import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.Int8Array
import org.w3c.files.Blob
import org.w3c.files.BlobPropertyBag
import org.w3c.xhr.ARRAYBUFFER
import org.w3c.xhr.FormData
import org.w3c.xhr.XMLHttpRequest
import org.w3c.xhr.XMLHttpRequestResponseType
import kotlin.coroutines.experimental.suspendCoroutine


actual fun platformCreateHTTPRequest(url: String): RemoteRequest {
    return HTTPRequest(url)
}

class HTTPRequest(val url: String) : RemoteRequest {
    private var connection: XMLHttpRequest? = null

    private class Reply(val header: String, val dataInStream: AsyncInStream) : RemoteRequest.Reply {
        override suspend fun receiveHeader(): String {
            return header
        }

        override suspend fun inStream(): AsyncInStream {
            return dataInStream
        }

        override fun close() {
        }

    }

    private class Sender(val connection: XMLHttpRequest, val formData: FormData, val outgoingData: Boolean)
        : RemoteRequest.DataSender {
        private val outStream = AsyncByteArrayOutStream()

        override fun outStream(): AsyncOutStream {
            if (!outgoingData)
                throw Exception("Configured without outgoing data")
            return outStream
        }

        override suspend fun send(): RemoteRequest.Reply {
            val type = BlobPropertyBag()
            type.type = "application/octet-stream"
            val dataBlob = Blob(arrayOf(outStream.toByteArray()), type)
            formData.append(HTTPRequestMultipartKeys.DATA_KEY, dataBlob)

            connection.responseType = XMLHttpRequestResponseType.ARRAYBUFFER
            connection.send(formData)

            suspendCoroutine<Unit> { cont ->
                connection.onload = { cont.resume(Unit) }
                connection.onabort = { e ->
                    cont.resumeWithException(Exception("Connection aborted: ${e.target.asDynamic().status}"))
                }
                connection.onerror = { e ->
                    cont.resumeWithException(Exception("Connection failed: ${e.target.asDynamic().status}"))
                }
                connection.ontimeout = { e ->
                    cont.resumeWithException(Exception("Connection timeout: ${e.target.asDynamic().status}"))
                }
            }

            val response = connection.response.unsafeCast<ArrayBuffer>()
            val contentType = connection.getResponseHeader("Content-Type")
                    ?: throw Exception("Missing content type")
            val parsed = MultiPart_parse(response, contentType)
            val header = Int8Array(parsed[HTTPRequestMultipartKeys.REQUEST_KEY].unsafeCast<ArrayBuffer>())
                    .unsafeCast<ByteArray>().toUTFString()
            val data = Int8Array(parsed[HTTPRequestMultipartKeys.DATA_KEY].unsafeCast<ArrayBuffer>())
                    .unsafeCast<ByteArray>()

            val inStream = ByteArrayInStream(data).toAsyncInputStream()
            return Reply(header, inStream)
        }
    }

    override suspend fun send(header: String): RemoteRequest.Reply {
        return sendData(header, false).send()
    }

    override suspend fun sendData(header: String): RemoteRequest.DataSender {
        return sendData(header, true)
    }

    private fun sendData(header: String, outgoingData: Boolean): RemoteRequest.DataSender {
        cancel()

        val connection = XMLHttpRequest()
        connection.open("POST", url)

        val formData = FormData()
        formData.append(HTTPRequestMultipartKeys.REQUEST_KEY, header)

        this@HTTPRequest.connection = connection
        return Sender(connection, formData, outgoingData)
    }

    override fun cancel() {
        connection?.abort()
        connection = null
    }
}
