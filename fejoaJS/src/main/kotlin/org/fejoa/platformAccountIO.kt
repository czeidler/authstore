package org.fejoa

import kotlinx.serialization.json.JSON
import org.fejoa.jsbindings.await
import org.fejoa.storage.AccountStorage
import kotlin.js.json

actual fun platformGetAccountIO(type: AccountIO.Type, context: String, namespace: String): AccountIO {
    return JSAccountIO(context, namespace)
}

class JSAccountIO(val context: String, val namespace: String) : AccountIO {
    override suspend fun exists(): Boolean {
        return AccountStorage(context, namespace).configExists()
    }

    companion object {
        private const val LOGIN_DATA_KEY = "loginData"
        private const val USER_DATA_CONFIG_KEY = "loginData"
    }

    private suspend fun writeConfig(key: String, value: String) {
        val db = AccountStorage(context, namespace).openOrCreateConfig()
        val transaction = db.transaction(AccountStorage.CONFIG_STORE, "readwrite")
        val store = transaction.objectStore(AccountStorage.CONFIG_STORE)
        store.put(json(AccountStorage.CONFIG_KEY to key, "data" to value)).await()
    }

    private suspend fun readConfig(key: String): String {
        val db = AccountStorage(context, namespace).openConfig()
        val transaction = db.transaction(AccountStorage.CONFIG_STORE, "readonly")
        val store = transaction.objectStore(AccountStorage.CONFIG_STORE)
        return store.get(key).await()["data"].unsafeCast<String>()
    }

    override suspend fun writeLoginData(loginData: LoginParams) {
        writeConfig(LOGIN_DATA_KEY, JSON(indented = true).stringify(loginData))
    }

    override suspend fun readLoginData(): LoginParams {
        return JSON.parse(readConfig(LOGIN_DATA_KEY))
    }

    override suspend fun writeUserDataConfig(userDataConfig: UserDataConfig) {
        writeConfig(USER_DATA_CONFIG_KEY, JSON(indented = true).stringify(userDataConfig))
    }

    override suspend fun readUserDataConfig(): UserDataConfig {
        return JSON.parse(readConfig(USER_DATA_CONFIG_KEY))
    }

    override suspend fun writeServerConfig(serverConfig: ServerConfig) {
        TODO("not implemented")
    }

    override suspend fun readServerConfig(): ServerConfig {
        TODO("not implemented")
    }

}