requirejs.config({
    // path where all main libs are located, e.g. kotlin.js
    baseUrl: '/base/fejoaJS/build/lib',

    paths: { // paths are relative to this file
        'pako': '../../../jsbindings/build/resources/main/libs/pako.min',
        'fejoaJS': '../../build/classes/main/fejoaJS',
        'fejoaJS_test': '../../build/classes/test/fejoaJS_test',
    },

    deps: ['fejoaJS_test'],

    // start tests when done
    callback: window.__karma__.start
});
