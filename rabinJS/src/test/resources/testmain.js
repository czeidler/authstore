requirejs.config({
    // path where all main libs are located, e.g. kotlin.js
    baseUrl: '/base/rabinJS/build/lib',

    paths: { // paths are relative to this file
        'pako': '../../../jsbindings/build/resources/main/libs/pako.min',
        'rabinJS': '../../build/classes/main/rabinJS',
        'rabinJS_test': '../../build/classes/test/rabinJS_test',
    },

    deps: ['pako', 'rabinJS_test'],

    // start tests when done
    callback: window.__karma__.start
});
