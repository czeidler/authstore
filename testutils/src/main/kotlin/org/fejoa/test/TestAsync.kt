package org.fejoa.test

import kotlinx.coroutines.experimental.CoroutineScope


expect fun <T> testAsync(body: suspend CoroutineScope.() -> T)

expect fun <T> testAsync(cleanUp: suspend () -> Unit, body: suspend () -> T)

expect fun <T> testAsync(setUp: (suspend () -> Unit)?, cleanUp: (suspend () -> Unit)?, body: suspend () -> T)
