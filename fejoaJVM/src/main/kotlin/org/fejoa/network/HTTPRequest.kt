package org.fejoa.network

import kotlinx.coroutines.experimental.CoroutineDispatcher
import kotlinx.coroutines.experimental.asCoroutineDispatcher
import kotlinx.coroutines.experimental.async
import org.eclipse.jetty.util.MultiPartInputStreamParser
import org.fejoa.crypto.CryptoHelper
import org.fejoa.support.AsyncInStream
import org.fejoa.support.AsyncOutStream
import org.fejoa.support.toHex
import org.fejoa.support.toUTFString
import java.io.*
import java.net.*
import java.util.concurrent.Executors
import javax.servlet.ServletException


fun OutputStream.toAsyncOutStream(pool: CoroutineDispatcher): AsyncOutStream {
    val that = this
    return object : AsyncOutStream {
        override suspend fun write(buffer: ByteArray, offset: Int, length: Int): Int = async(pool) {
            that.write(buffer, offset, length)
            return@async length
        }.await()

        override suspend fun close() {
            that.close()
        }
    }
}

fun InputStream.toAsyncInStream(pool: CoroutineDispatcher): AsyncInStream {
    val that = this
    return object : AsyncInStream {
        override suspend fun read(buffer: ByteArray, offset: Int, length: Int): Int = async(pool) {
            return@async that.read(buffer, offset, length)
        }.await()

        override suspend fun close() {
            that.close()
        }
    }
}


class HTTPRequest(val url: String) : RemoteRequest {
    private var connection: HttpURLConnection? = null

    companion object {
        private val LINE_FEED = "\r\n"
        val pool = Executors.newCachedThreadPool().asCoroutineDispatcher()
    }

    init {
        if (CookieHandler.getDefault() == null)
            CookieHandler.setDefault(CookieManager(null, CookiePolicy.ACCEPT_ALL))
    }

    private fun getBoundary(): String {
        val randomValue = CryptoHelper.crypto.generateSalt16().toHex()
        return "===" + System.currentTimeMillis() + "_" + randomValue + "==="
    }

    private class Reply(val header: String, val connectionInputStream: InputStream, val dataInStream: InputStream) : RemoteRequest.Reply {
        private var closed = false
        private val inStream = dataInStream.toAsyncInStream(pool)

        override suspend fun receiveHeader(): String {
            return header
        }

        override suspend fun inStream(): AsyncInStream {
            return inStream
        }

        override fun close() {
            if (closed)
                return
            closed = true

            connectionInputStream.close()
            dataInStream.close()
        }

    }

    private class Sender(val connection: HttpURLConnection, val boundary: String, val outStream: AsyncOutStream,
                         val writer: PrintWriter) : RemoteRequest.DataSender {
        override fun outStream(): AsyncOutStream {
            return outStream
        }

        override suspend fun send(): RemoteRequest.Reply = async(pool) {
            writer.append(LINE_FEED)
            writer.append("--$boundary--").append(LINE_FEED)
            writer.append(LINE_FEED).flush()
            writer.close()

            val inputStream = connection.inputStream

            val contentType = connection.contentType.replace("Content-Type: ", "", true)
            val parser = MultiPartInputStreamParser(inputStream,
                    contentType, null, null)
            try {
                val messagePart = parser.getPart(HTTPRequestMultipartKeys.REQUEST_KEY)
                val dataPart = parser.getPart(HTTPRequestMultipartKeys.DATA_KEY)

                val bufferedInputStream = BufferedInputStream(messagePart.inputStream)
                val receivedData = ByteArrayOutputStream()
                bufferedInputStream.copyTo(receivedData)

                val receivedHeader = receivedData.toString()
                println("RECEIVED: " + receivedHeader)

                val dataInputStream = dataPart?.inputStream ?: ByteArrayInputStream(ByteArray(0))
                return@async Reply(receivedHeader, inputStream, dataInputStream)
            } catch (e: ServletException) {
                e.printStackTrace()
                throw IOException("Unexpected server response.")
            }
        }.await()
    }

    override suspend fun send(header: String): RemoteRequest.Reply {
        return sendData(header, false).send()
    }

    override suspend fun sendData(header: String): RemoteRequest.DataSender {
        return sendData(header, true)
    }

    private suspend fun sendData(header: String, outgoingData: Boolean): RemoteRequest.DataSender  = async(pool) {
        cancel()
        println("SEND:     " + header)

        val server = URL(url)

        val boundary = getBoundary()
        val connection = server.openConnection() as HttpURLConnection
        connection.useCaches = false
        connection.doOutput = true
        connection.doInput = true
        connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary)
        connection.setRequestProperty("Accept-Charset", "utf-8")

        connection.connect()
        val outputStream = connection.outputStream

        val writer = PrintWriter(OutputStreamWriter(outputStream, "UTF-8"), true)
        // header
        writer.append("--" + boundary).append(LINE_FEED)
        writer.append("Content-Type: text/plain; charset=UTF-8").append(LINE_FEED)
        writer.append("Content-Disposition: form-data; name=\"" + HTTPRequestMultipartKeys.REQUEST_KEY + "\"").append(LINE_FEED)
        writer.append(LINE_FEED)
        writer.append(header)

        // body
        if (outgoingData) {
            writer.append(LINE_FEED)
            writer.append("--" + boundary).append(LINE_FEED)
            writer.append("Content-Type: \"application/octet-stream\"").append(LINE_FEED)
            writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED)
            writer.append("Content-Disposition: form-data; name=\"" + HTTPRequestMultipartKeys.DATA_KEY
                    + "\";").append(LINE_FEED)
            writer.append(LINE_FEED)
            writer.flush()
        }
        this@HTTPRequest.connection = connection
        return@async Sender(connection, boundary, outputStream.toAsyncOutStream(pool), writer)
    }.await()

    override fun cancel() {
        connection?.disconnect()
        connection = null
    }

}

actual fun platformCreateHTTPRequest(url: String): RemoteRequest {
    return HTTPRequest(url)
}
