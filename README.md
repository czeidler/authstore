# IMPORTANT: Current Development

AuthStore is developed as part of the [Fejoa](fejoa.org) project.
Please visit [GitLab](https://gitlab.com/czeidler/fejoa) to access the latest source code or visit the [FejoaAuth](https://fejoa.org/fejoapage/auth.html) project page for more information.

# AuthStore: Password-based Authentication and Encrypted Data Storage in Untrusted Environments

This is the project page for AuthStore.
AuthStore allows secure password reuse at multiple AuthStore service providers.
AuthStore prevents service providers to learn any information about the user password and thus a malicious service provider can't impersonate the user at another service provider or decrypt data that is protected with the user password.

AuthStore uses key strengthening to protect the user password from dictionary attacks.
The user is in control of choosing appropriate key strengthening parameters to make it hard for an attacker to brute force the user password.

To prevent leaking the user password during the authentication process, AuthStore uses the CompactPake protocol.
CompactPake is secure against parameter attacks (see paper) and reveals no information about the entered passwords.
For example, if a user accidentally attempts to login at a service provider with a password used at another service provider, the current service provider can't uses the exchanged information to login at the other service provider or learn information about the password.

The code for the CompactPake protocol can be found at:

`lib/src/main/kotlin/org/fejoa/crypto/CompactPAKE_SHA256.kt`

To understand how to use the CompactPake class, the test case is a good reference:

`lib/src/test/kotlin/org/fejoa/crypto/CompactPAKETest.kt`

## Browser Extension

Using conventional password-based authentication on the web, users can't prevent web page service providers to learn the entered password.
For example, a web page service provider can simply add JS code to readout the plain user password and even record passwords while typing.

To provide secure password-based authentication on web pages, we developed a AuthStore browser extension that allows users to authenticate at supported AuthStore service providers.

![RegisterPopup](images/RegisterPopup.png)

![SecuritySettings](images/SecuritySettings.png)

When registering at a supported service provider the extension let the users choose how secure their passwords are protected.
The build in benchmark gives users an estimate of how much time is needed to strengthen their password, i.e., how long a login attempt will take.

### Password Manager
To provide a complete password-based authentication solution, the extension includes a password manager.
The password manager uses AuthStore storage to securely store user passwords.

![StorePasswordQuestion](images/StorePasswordQuestion.png)

### Build
To initialize the gradle wrapper run:

`gradle wrapper`

To build the browser extension run:

`./gradlew build -p browseraddon`

This will output the extension at:

`browseraddon/build/addon`

This extension directory can then be loaded into Chrome or Firefox (untested).


To build and run the AuthStore server run:

`./gradlew runServer`

The server will host a test authentication page at:

`http://localhost:8080/web/TestPage.html`



