importScripts("js/libs/require.js", "js/libs/base64-arraybuffer.js", "js/libs/BigInteger.min.js");

requirejs.config({
    // path where all main libs are located, e.g. kotlin.js
    baseUrl: 'js',

    paths: { // paths are relative to this file
        'pako': 'libs/pako.min',
    },

    deps: ['browseraddon'],

    callback: onPackagesLoaded
});

// we most likely receive the request before the browseradddon is loaded; cache the request here
var cachedMessage = null;

onmessage = function(message) {
    cachedMessage = message;
}

function onPackagesLoaded(browseraddon) {
    var worker = new browseraddon.org.fejoa.auth.KDFWorker();
    worker.handle_za3rmp$(cachedMessage);
}
