requirejs.config({
    // path where all main libs are located, e.g. kotlin.js
    baseUrl: 'js',

    paths: { // paths are relative to this file
        'pako': 'libs/pako.min',
    },

    deps: ['browseraddon'],

    callback: onPackagesLoaded
});

function onPackagesLoaded(browseraddon) {
    // Simply store the loaded module so that it can be used in the popup window
    window.browseraddon = browseraddon;
}