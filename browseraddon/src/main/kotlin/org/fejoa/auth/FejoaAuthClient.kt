package org.fejoa.auth

import kotlinx.coroutines.experimental.Unconfined
import org.fejoa.AccountIO
import org.fejoa.ConnectionAuthManager
import org.fejoa.FejoaContext
import org.fejoa.auth.passwordmanager.PMAccountManager
import org.fejoa.auth.passwordmanager.PMBackground
import org.fejoa.crypto.BaseKeyCache
import org.fejoa.crypto.BaseKeyParams
import org.fejoa.crypto.SecretKey
import org.fejoa.support.Future


class WebWorkerStrategy : BaseKeyCache.DeriveStrategy {
    override fun derive(baseKeyParams: BaseKeyParams, password: String): Future<SecretKey> {
        val kdfWorker = KDFWorkerClient()
        return kdfWorker.deriveKey(password, baseKeyParams.getSalt(), baseKeyParams.kdf)
    }
}

class FejoaAuthClient {
    val context = FejoaContext(AccountIO.Type.CLIENT, "fejoa-auth", "noUser", Unconfined)
    val authManager = ConnectionAuthManager(context)
    val accountManager = PMAccountManager(context)
    val pmBackground = PMBackground(accountManager)

    init {
        context.setBaseKeyCacheStrategy(WebWorkerStrategy())
        pmBackground.start()
    }
}