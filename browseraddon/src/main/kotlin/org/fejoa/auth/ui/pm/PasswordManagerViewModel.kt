package org.fejoa.auth.ui.pm

import kotlinx.coroutines.experimental.launch
import org.fejoa.auth.FejoaAuthClient
import org.fejoa.auth.passwordmanager.PMAccountManager
import org.fejoa.auth.passwordmanager.Wallet
import org.fejoa.auth.ui.*
import org.fejoa.crypto.CryptoSettings
import org.w3c.dom.*
import kotlin.properties.Delegates


class PasswordManagerViewModel(val window: Window, fejoaAuthClient: FejoaAuthClient, pageInfoManager: PageInfoManager,
                               val log: LogViewModel) {
    val document = window.document
    val accountManager = fejoaAuthClient.accountManager

    val kdfConfigViewModel = KDFConfigViewModel(window, "pm-")

    val createAccountInput = document.getElementById("pm-create-account-name").unsafeCast<HTMLInputElement>()
    val createPasswordInput = document.getElementById("pm-account-password").unsafeCast<HTMLInputElement>()
    val createPassword2Input = document.getElementById("pm-account-password2").unsafeCast<HTMLInputElement>()

    val openPasswordInput = document.getElementById("pm-open-account-password").unsafeCast<HTMLInputElement>()
    val createButton = document.getElementById("pm-create-button").unsafeCast<HTMLButtonElement>()
    val openButton = document.getElementById("pm-open-button").unsafeCast<HTMLButtonElement>()
    val closeButton = document.getElementById("pm-close-button").unsafeCast<HTMLButtonElement>()
    val deleteButton = document.getElementById("pm-delete-button").unsafeCast<HTMLButtonElement>()
    val cancelButton = document.getElementById("pm-cancel-button").unsafeCast<HTMLButtonElement>()
    val dropDownMenu = document.getElementById("pm-account-menu").unsafeCast<HTMLUListElement>()

    val busyStatus = document.getElementById("pm-busy-status").unsafeCast<HTMLSpanElement>()
    val accountStatus = document.getElementById("pm-account-status").unsafeCast<HTMLSpanElement>()
    val accountStatusBar = document.getElementById("pm-account-status-bar").unsafeCast<HTMLDivElement>()
    val selectedAccountTitle = document.getElementById("pm-account-view-title-selected").unsafeCast<HTMLHeadingElement>()

    val closedContainer = document.getElementById("pm-closed-account-container").unsafeCast<HTMLDivElement>()
    val openedContainer = document.getElementById("pm-opened-account-container").unsafeCast<HTMLDivElement>()
    val busyContainer = document.getElementById("pm-busy-account-container").unsafeCast<HTMLDivElement>()
    val deleteContainer = document.getElementById("pm-delete-account-container").unsafeCast<HTMLDivElement>()

    // Don't use a normal Kotlin list; Kotlin does some type checks and these fail for popup elements...
    var tabContentContainerList: Array<HTMLDivElement> = arrayOf(closedContainer, openedContainer, busyContainer,
            deleteContainer)
    fun addTabContentContainer(container: HTMLDivElement) {
        tabContentContainerList = arrayOf(*tabContentContainerList, container)
    }

    private val accountDropDownLinks = HashMap<String, HTMLLinkElement>()
    private var selectedAccount: String? = null
    //TODO val remoteController = RemoteViewModel(this, context)
    //TODO val remoteImportController = ImportRemoteController(this, context, log)

    var errorMessage by Delegates.observable("") { _, _, newValue ->
        log.error(newValue)
    }

    val accountListener: () -> Unit = {
        onAccountListUpdated()
    }

    init {
        PWGeneratorViewModel(window, pageInfoManager, fejoaAuthClient.pmBackground)

        KDFBenchmarkViewModel(window, KDFBenchmark(), kdfConfigViewModel, "pm-")

        accountManager.accountListeners += accountListener
        window.addEventListener("beforeunload", {
            accountManager.accountListeners.remove { accountListener }
        })

        createButton.addEventListener("click", {
            launch {
                errorMessage = ""
                val password = createPasswordInput.value
                val algo = kdfConfigViewModel.getKdfAlgo()
                val nIterations = kdfConfigViewModel.getNIterations()
                val accountName = createAccountInput.value
                createAccountInput.value = ""
                createPasswordInput.value = ""
                createPassword2Input.value = ""
                val previousSelected = selectedAccount
                try {
                    val kdf = CryptoSettings.default.kdf
                    if (kdf.algo != algo)
                        throw Exception("Unsupported kdf algo: $algo")
                    kdf.params.iterations = nIterations

                    // Already select the account so that as soon as createAccount sends an update the account will be
                    // displayed.
                    selectedAccount = accountName
                    accountManager.createAccount(accountName, password, kdf)
                    selectAccount(accountName)
                } catch (e: Exception) {
                    if (previousSelected != null)
                        selectAccount(previousSelected)
                    errorMessage ="Failed to create account: " + e.message
                }
            }
        })
        openButton.addEventListener("click", {
            launch {
                errorMessage = ""
                val password = openPasswordInput.value
                openPasswordInput.value = ""
                val account = accountManager.openAccount(selectedAccount!!, password)

                if (account == null) {
                    errorMessage = "Failed to open account"
                }
            }
        })
        closeButton.addEventListener("click", {
            launch {
                accountManager.closeAccount(selectedAccount!!)
            }
        })
        deleteButton.addEventListener("click", {
            launch {
                val current = selectedAccount ?: return@launch
                selectedAccount = null
                accountManager.deleteAccount(current)
            }
        })
        cancelButton.addEventListener("click", {
            launch {
                if (selectedAccount == null)
                    return@launch
                val entry = accountManager.getAccount(selectedAccount!!) ?: return@launch
                entry.future?.cancel()
            }
        })


        openPasswordInput.oninput = {_ -> validateOpenInfo()}
        createAccountInput.oninput = { _ -> validateCreateInfo()}
        createPasswordInput.oninput = {_ -> validateCreateInfo()}
        createPassword2Input.oninput = {_ -> validateCreateInfo()}

        onAccountListUpdated()
    }


    private fun validateOpenInfo() {
        val password = openPasswordInput.value
        openButton.disabled = password == ""
    }

    private fun validateCreateInfo() {
        val accountName = createAccountInput.value
        val password1 = createPasswordInput.value
        val password2 = createPassword2Input.value
        createButton.disabled = (accountManager.getAccount(accountName) != null
                || accountName == "" || password1 == "" || password1 != password2)
    }

    private fun onAccountListUpdated() {
        // repopulate the drop down
        while (dropDownMenu.firstChild != null)
            dropDownMenu.removeChild(dropDownMenu.firstChild!!)
        accountDropDownLinks.clear()

        accountManager.getAccounts().forEach {
            val accountName = it.name
            val li = document.createElement("li")
            li.addEventListener("click", {
                errorMessage = ""
                onAccountSelected(accountName)
            })
            val link = document.createElement("a").unsafeCast<HTMLLinkElement>()
            accountDropDownLinks[accountName] = link
            link.href = "#pm-account-view-tab"
            link.setAttribute("data-toggle", "tab")
            link.setAttribute("data-account", accountName)
            var text =  accountName
            when (it.type) {
                PMAccountManager.AccountEntry.Type.OPEN -> text += " (open)"
                PMAccountManager.AccountEntry.Type.CLOSED -> text += " (closed)"
                PMAccountManager.AccountEntry.Type.OPENING -> text += " (opening...)"
                PMAccountManager.AccountEntry.Type.CREATING -> text += " (creating...)"
            }
            link.innerText = text
            li.appendChild(link)
            dropDownMenu.appendChild(li)
        }

        //TODO: remoteImportController.populateImportMenu(dropDownMenu)

        if (accountManager.getAccounts().isEmpty())
            selectCreateTab()
        else {
            if (selectedAccount != null)
                onAccountSelected(selectedAccount!!)
            else {
                val accountEntry = accountManager.getDefaultAccount()
                        ?: accountManager.getAccounts()[0]
                selectedAccount = accountEntry.name
                onAccountSelected(accountEntry)
            }
            // activate the selected account
            selectAccount(selectedAccount!!)
        }
    }

    fun selectCreateTab() {
        js("this.window.\$('a[href=\"#pm-create-tab\"]').tab('show');")
    }

    fun hideDeleteContainer() {
        js("this.window.\$(\"#pm-delete-question\").collapse('hide');")
    }

    fun selectAccount(accountName: String) {
        selectedAccount = accountName
        accountDropDownLinks[accountName]?.click()
    }

    fun showContainer(container: HTMLDivElement) {
        tabContentContainerList.forEach {
            it.hidden = it !== container
        }
    }

    fun onAccountSelected(accountName: String) {
        val accountEntry = accountManager.getAccount(accountName) ?: return
        onAccountSelected(accountEntry)
    }

    fun onAccountSelected(accountEntry: PMAccountManager.AccountEntry) {
        // update the remote controller
        // TODO remoteController.pmAccount = accountEntry.account

        accountStatusBar.hidden = false
        selectedAccount = accountEntry.name
        selectedAccountTitle.innerText = accountEntry.name

        hideDeleteContainer()

        when (accountEntry.type) {
            PMAccountManager.AccountEntry.Type.CLOSED -> {
                showContainer(closedContainer)
                closeButton.disabled = true
                accountStatus.innerText = "Closed: "
            }
            PMAccountManager.AccountEntry.Type.OPEN -> {
                showContainer(openedContainer)
                accountStatus.innerText = "Open: "
                closeButton.disabled = false
                accountEntry.account?.getDefaultWallet()?.let{ updateOpenedContainer(it) }
            }
            PMAccountManager.AccountEntry.Type.OPENING,
            PMAccountManager.AccountEntry.Type.CREATING -> {
                showContainer(busyContainer)
                closeButton.disabled = true

                if (accountEntry.type == PMAccountManager.AccountEntry.Type.OPENING) {
                    busyStatus.innerText = "opening account..."
                } else {
                    busyStatus.innerText = "creating account..."
                }
            }
        }
    }

    fun updateOpenedContainer(wallet: Wallet) {
        //val origins = wallet.getPasswordOrigins()
        //openedContainer.innerText = "Account contains keys for ${origins.size} side(s)"
    }
}