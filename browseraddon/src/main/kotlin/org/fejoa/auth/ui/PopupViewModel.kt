package org.fejoa.auth.ui

import kotlinx.coroutines.experimental.launch
import org.w3c.dom.*


class PopupViewModel(val window: Window, val authManager: AuthManager, val pageInfo: PageInfo,
                     val log: LogViewModel) {
    val document = window.document
    val kdfConfigController = KDFConfigViewModel(window)

    val authUrl = pageInfo.getFejoaAuthPath()

    val loginStatus = document.getElementById("login-status").unsafeCast<HTMLDivElement>()

    val currentPageLabel = document.getElementById("current-page").unsafeCast<HTMLSpanElement>()
    val loggedInUser = document.getElementById("logged-in-user").unsafeCast<HTMLSpanElement>()
    val loginRegister = document.getElementById("login-register").unsafeCast<HTMLDivElement>()
    val loginUsernameInput = document.getElementById("login-username").unsafeCast<HTMLInputElement>()
    val loginPasswordInput = document.getElementById("login-password").unsafeCast<HTMLInputElement>()

    val registerUsernameInput = document.getElementById("register-username").unsafeCast<HTMLInputElement>()
    val registerPasswordInput = document.getElementById("register-password").unsafeCast<HTMLInputElement>()
    val registerPassword2Input = document.getElementById("register-password2").unsafeCast<HTMLInputElement>()

    val logoutButton = document.getElementById("logout-button").unsafeCast<HTMLButtonElement>()
    val loginButton = document.getElementById("login-button").unsafeCast<HTMLButtonElement>()
    val registerButton = document.getElementById("register-button").unsafeCast<HTMLButtonElement>()

    val busyContainer = document.getElementById("fa-busy-container").unsafeCast<HTMLDivElement>()
    val busyStatus = document.getElementById("fa-busy-status").unsafeCast<HTMLSpanElement>()
    val cancelButton = document.getElementById("fa-cancel-button").unsafeCast<HTMLButtonElement>()

    val statusListener = AuthManager.StatusListener(pageInfo.origin(), {
        onAuthStatusChanged()
    })

    init {
        KDFBenchmarkViewModel(window, KDFBenchmark(), kdfConfigController)

        authManager.statusListeners += statusListener
        window.addEventListener("beforeunload", {
            authManager.statusListeners.remove(statusListener)
        })

        currentPageLabel.innerText = pageInfo.url.hostname

        logoutButton.addEventListener("click", {
            launch {
                authManager.logOut(authUrl)
            }
        })
        loginButton.addEventListener("click", {

            enableLoginRegister(false)
            val password = loginPasswordInput.value
            loginPasswordInput.value = ""
            authManager.launchLogin(authUrl, loginUsernameInput.value, password)
        })
        registerButton.addEventListener("click", {
            enableLoginRegister(false)
            val password = registerPasswordInput.value
            val algo = kdfConfigController.getKdfAlgo()
            val nIterations = kdfConfigController.getNIterations()
            registerPasswordInput.value = ""
            registerPassword2Input.value = ""
            authManager.launchRegister(algo, authUrl, registerUsernameInput.value, password, nIterations)
        })
        cancelButton.addEventListener("click", {
            val origin = pageInfo.origin()
            val authStatus = authManager.getAuthStatus(origin) ?: return@addEventListener
            authStatus.promise?.cancel()
        })

        loginUsernameInput.oninput = {_ -> validateLoginInfo()}
        loginPasswordInput.oninput = {_ -> validateLoginInfo()}

        registerUsernameInput.oninput = {_ -> validateRegisterInfo()}
        registerPasswordInput.oninput = {_ -> validateRegisterInfo()}
        registerPassword2Input.oninput = {_ -> validateRegisterInfo()}

        // init the UI
        onAuthStatusChanged()
    }

    suspend fun start() {
        authManager.checkStatus(authUrl)
    }

    private fun validateLoginInfo() {
        val user = loginUsernameInput.value
        val password = loginPasswordInput.value
        loginButton.disabled = (user == "" || password == "")
    }
    private fun validateRegisterInfo() {
        val user = registerUsernameInput.value
        val password1 = registerPasswordInput.value
        val password2 = registerPassword2Input.value
        registerButton.disabled = (user == "" || password1 == "" || password1 != password2)
    }

    private fun enableLoginRegister(enable: Boolean) {
        loginUsernameInput.disabled = !enable
        loginPasswordInput.disabled = !enable

        registerUsernameInput.disabled = !enable
        registerPasswordInput.disabled = !enable
        registerPassword2Input.disabled = !enable

        validateLoginInfo()
        validateRegisterInfo()
    }

    private fun onAuthStatusChanged() {
        val authStatus = authManager.getAuthStatus(pageInfo.origin()) ?: return
        val user = authStatus.user

        if (authStatus.status == AuthManager.Status.LOGGING_IN || authStatus.status == AuthManager.Status.REGISTERING) {
            busyContainer.hidden = false
            loginStatus.hidden = true
            logoutButton.disabled = true
            loginRegister.hidden = true
            if (authStatus.status == AuthManager.Status.LOGGING_IN)
                busyStatus.innerText = "logging in as ${authStatus.user}..."
            else
                busyStatus.innerText = "registering as ${authStatus.user}..."
        } else if (user != null && user != "") {
            busyContainer.hidden = true
            loginStatus.hidden = false
            logoutButton.disabled = false
            loginRegister.hidden = true
            loggedInUser.innerText = user
            loginUsernameInput.value = ""
            registerUsernameInput.value = ""
        } else {
            busyContainer.hidden = true
            loginStatus.hidden = true
            logoutButton.disabled = true
            loginRegister.hidden = false
            enableLoginRegister(true)
        }

        onErrorOrStatusChanged(authStatus)
    }

    private fun onErrorOrStatusChanged(authAuth: AuthManager.AuthStatus) {
        val status = authAuth.statusMessage ?: ""
        val error = authAuth.errorMessage ?: ""
        log.status(status)
        log.error(error)
    }
}