package org.fejoa.auth.ui

import kotlinx.coroutines.experimental.launch
import org.fejoa.auth.FejoaAuthClient
import org.fejoa.auth.ui.pm.PasswordManagerViewModel
import org.fejoa.jsbindings.chrome
import org.w3c.dom.*


/**
 * Start the main popup and checks if the page supports fejoa auth
 */
class Popup(val fejoaAuthClient: FejoaAuthClient, val window: Window) {
    val pageInfoManager = PageInfoManager()
    val document = window.document
    val log = LogViewModel(window)

    val navBar = NavBar()

    private fun onPageInfoChanged() {
        pageInfoManager.pageInfo.first?.let {
            navBar.view = NavBar.VIEW.AUTH
            launch {
                PopupViewModel(window, AuthManager(fejoaAuthClient), it, log).start()
            }
        }
        pageInfoManager.pageInfo.second?.let {
            navBar.view = NavBar.VIEW.PASSWORD_MANAGER
        }
    }

    fun start() {
        NavBarViewModel(window, navBar, pageInfoManager)
        pageInfoManager.onPageInfoChanged.add(this::onPageInfoChanged)
        launch {
            pageInfoManager.updatePageInfo()
        }

        // password manager
        PasswordManagerViewModel(window, fejoaAuthClient, pageInfoManager, log)
    }
}


fun initPopup(window: Window) {
    val bgWindow = chrome.extension.getBackgroundPage().unsafeCast<dynamic>()
    if (bgWindow.fejoaAuthClient == undefined) {
        bgWindow.fejoaAuthClient = FejoaAuthClient()
    }
    val fejoaAuthClient = bgWindow.fejoaAuthClient.unsafeCast<FejoaAuthClient>()
    val controller = Popup(fejoaAuthClient, window)
    controller.start()
}