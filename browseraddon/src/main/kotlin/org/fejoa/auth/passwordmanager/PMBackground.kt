package org.fejoa.auth.passwordmanager

import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import org.fejoa.jsbindings.BASIC
import org.fejoa.jsbindings.chrome
import org.fejoa.jsbindings.TemplateType
import org.w3c.dom.url.URL
import kotlin.js.Json
import kotlin.js.json



class PMBackground(val accountManager: PMAccountManager) {
    private fun getCredentials(origin: String, userName: String, defaultAccountEntry: PMAccountManager.AccountEntry,
                               senderResponse: (arg: Json?) -> Unit) = async {
        val originCredentials = accountManager.getAccounts().filter {
            it.type == PMAccountManager.AccountEntry.Type.OPEN
        }.sortedWith(object: Comparator<PMAccountManager.AccountEntry> {
            override fun compare(a: PMAccountManager.AccountEntry, b: PMAccountManager.AccountEntry): Int {
                if (a == defaultAccountEntry)
                    return -1
                else
                    return 0
            }
        }).mapNotNull { it.account }.flatMap {
            it.getWallets()
        }.mapNotNull {
            it.getPassword(origin)
        }

        val userCredentials = originCredentials.firstOrNull { it.username == userName } ?: return@async
        val user = userCredentials.username
        val password = userCredentials.password
        senderResponse(json("user" to user, "password" to password))
    }

    private fun storeCredentials(origin: String, message: Json, defaultAccount: PasswordManagerAccount) {
        val user = message["user"]?.unsafeCast<String>() ?: return
        val password = message["password"]?.unsafeCast<String>() ?: return
        if (user == "" || password == "")
            return

        val options = json("type" to TemplateType.BASIC,
                "title" to "Fejoa Auth Password Manager",
                "message" to "Do you want to store your password securely?",
                "iconUrl" to "images/icon.png",
                "buttons" to arrayOf(
                        json("title" to "Store password",
                                "iconUrl" to "images/ok.png"),
                        json("title" to "Ignore",
                                "iconUrl" to "images/cancel.png")
                )
        )
        val STORE_CREDENTIALS_ID = "storeCredentialsNotification"
        chrome.notifications.create(STORE_CREDENTIALS_ID, options)
        buttonClick@ chrome.notifications.onButtonClicked.addListener({ notificationId, buttonIndex ->
            if (notificationId != STORE_CREDENTIALS_ID)
                return@buttonClick
            chrome.notifications.clear(STORE_CREDENTIALS_ID)
            when (buttonIndex) {
                0 -> {

                    val wallet = defaultAccount.getDefaultWallet() ?: return@addListener
                    launch {
                        wallet.putPassword(origin, user, password)
                        wallet.commit()
                    }
                }
            }
        })
    }

    fun start() {
        chrome.runtime.onMessage.addListener({ message, sender, senderResponse ->
            if (message == null)
                return@addListener false
            if (sender.url == null)
                return@addListener false
            val origin = URL(sender.url!!).origin
            val defaultAccountEntry = accountManager.getDefaultAccount() ?: return@addListener false
            val defaultAccount = defaultAccountEntry.account?: return@addListener false

            val requestName = message["method"] ?: return@addListener false
            if (requestName == "getCredentials") {
                val userName = message["user"].unsafeCast<String>()
                getCredentials(origin, userName, defaultAccountEntry, senderResponse)
                // the reply is async because we have to look up the password in the database
                return@addListener true
            } else if (requestName == "storeCredentials") {
                storeCredentials(origin, message, defaultAccount)
            }
            return@addListener false
        })
    }

    fun fillRegisterPassword(password: String) {
        chrome.tabs.getSelected(null, { tab ->
            if (tab.id == null)
                return@getSelected
            chrome.tabs.sendMessage(tab.id!!, json("method" to "fillRegisterPassword",
                    "password" to password))
        })
    }
}
