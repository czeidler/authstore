package org.fejoa.auth.passwordmanager

import kotlinx.coroutines.experimental.launch
import org.fejoa.Client
import org.fejoa.FejoaContext
import org.fejoa.UserData
import org.fejoa.crypto.CryptoSettings
import org.fejoa.crypto.generateSecretKeyData
import org.fejoa.support.Future
import org.fejoa.support.async
import org.fejoa.support.await


class PMAccountManager(val context: FejoaContext) {
    val accountListeners = ArrayList<() -> Unit>()
    private val accounts = ArrayList<AccountEntry>()

    class AccountEntry(val name: String, var type: Type, var account: PasswordManagerAccount? = null,
                       var future: Future<*>? = null) {
        enum class Type {
            CLOSED,
            OPEN,
            OPENING, // in the process of being opened
            CREATING // in the process of being created
        }
    }

    init {
        launch {
            readExistingAccounts()
        }
    }

    private suspend fun readExistingAccounts() {
        val accountsNames = context.platformStorage.listNamespaces()
        // fill index entries into the account entry list
        for (name in accountsNames) {
            val existingAccount = getAccount(name)
            if (existingAccount != null)
                continue
            accounts.add(AccountEntry(name, AccountEntry.Type.CLOSED))
        }
        if (accounts.size > 0)
            notifyAccountsUpdated()
    }

    private fun notifyAccountsUpdated() {
        accountListeners.forEach { it.invoke() }
    }

    fun getAccount(name: String): AccountEntry? {
        return accounts.firstOrNull { it.name == name}
    }

    fun getAccounts(): List<AccountEntry> {
        return accounts
    }

    fun getDefaultAccount(): AccountEntry? {
        return accounts.firstOrNull()
    }

    private fun updateAccountEntry(name: String, type: AccountEntry.Type,
                                   account: PasswordManagerAccount? = null,
                                   promise: Future<*>? = null): AccountEntry {
        var accountEntry = getAccount(name)
        if (accountEntry == null) {
            accountEntry = AccountEntry(name, type, account, promise)
            accounts.add(accountEntry)
        } else {
            accountEntry.type = type
            accountEntry.account = account
            accountEntry.future = promise
        }
        notifyAccountsUpdated()
        return accountEntry
    }

    private fun removeAccountEntry(accountName: String) {
        val accountEntry = getAccount(accountName) ?: return
        accounts.remove(accountEntry)
        notifyAccountsUpdated()
    }

    suspend fun createAccount(accountName: String, password: String, kdf: CryptoSettings.KDF): PasswordManagerAccount {
        val name = accountName.trim()
        if (name == "")
            throw Exception("Invalid account name")
        if (getAccount(accountName) != null)
            throw Exception("Account already exists")

        val future = async {
            val client = Client.create(context.context, name, password, kdf)
            client.context.baseKeyCache = context.baseKeyCache
            client
        }
        updateAccountEntry(name, AccountEntry.Type.CREATING, null, future)
        try {
            val client = future.await()
            val account = PasswordManagerAccount(client, name)
            // create one wallet
            account.createWallet()
            client.userData.commit()
            updateAccountEntry(name, AccountEntry.Type.OPEN, account)
            return account
        } catch (e: Exception) {
            removeAccountEntry(accountName)
            throw e
        }
    }

    suspend fun openAccount(accountName: String, password: String): PasswordManagerAccount? {
        val name = accountName.trim()

        val future = async {
            val client = Client.open(context.context, accountName, password)
            client.context.baseKeyCache = context.baseKeyCache
            client
        }


        updateAccountEntry(name, AccountEntry.Type.OPENING, null, future)

        return try {
            val client = future.await()
            val account = PasswordManagerAccount(client, name)
            updateAccountEntry(name, AccountEntry.Type.OPEN, account)
            account
        }  catch (e: dynamic) {
            // failed to decrypt or canceled
            updateAccountEntry(name, AccountEntry.Type.CLOSED)
            null
        }
    }

    suspend fun closeAccount(accountName: String) {
        val accountEntry = getAccount(accountName) ?: return
        accountEntry.account?.client?.close()
        updateAccountEntry(accountEntry.name, AccountEntry.Type.CLOSED)
    }

    suspend fun closeAccount(account: PasswordManagerAccount) {
        closeAccount(account.name)
    }

    suspend fun deleteAccount(accountName: String) {
        val accountEntry = getAccount(accountName) ?: return
        accounts.remove(accountEntry)
        context.platformStorage.deleteNamespace(accountName)
        notifyAccountsUpdated()
    }
}


class PasswordManagerAccount(val client: Client, val name: String) {
    val userData = client.userData
    private val wallets = ArrayList<Wallet>()
    val walletUpdatedListeners = ArrayList<() -> Unit>()

    companion object {
        const val BRANCH_CONTEXT = "org.fejoa.auth.pm"
    }

    init {
        launch {
            val branches = userData.getBranches(BRANCH_CONTEXT, false)
            branches.forEach {
                val storageDir = userData.getStorageDir(it)
                val wallet = Wallet(storageDir)
                wallets += wallet
            }
        }
    }

    private fun notifyWalletUpdated() {
        walletUpdatedListeners.forEach { it.invoke() }
    }

    suspend fun createWallet(): Wallet {
        val branchName = UserData.generateBranchName()

        val keyData = CryptoSettings.default.symmetric.generateSecretKeyData()
        val branch = userData.createStorageDir(BRANCH_CONTEXT, branchName.toHex(),
                "PM Wallet", keyData)
        val storageDir = userData.getStorageDir(branch)
        val wallet = Wallet(storageDir)
        addWallet(wallet)
        return wallet
    }

    // TODO: store a default wallet?
    fun getDefaultWallet(): Wallet? {
        return wallets.firstOrNull()
    }

    fun getWallet(id: String): Wallet? {
        return wallets.firstOrNull { it.getId() == id }
    }

    fun getWallets(): List<Wallet> {
        return wallets
    }

    private fun addWallet(wallet: Wallet) {
        wallets.add(wallet)
    }
}
