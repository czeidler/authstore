package org.fejoa.auth.passwordmanager

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON
import org.fejoa.StorageDirObject
import org.fejoa.storage.*
import org.fejoa.support.PathUtils


class DBWalletEntry(dir: IOStorageDir, path: String) : DBValue<Wallet.Entry>(dir, path) {
    constructor(parent: DBObject, relativePath: String)
            : this(parent.dir, PathUtils.appendDir(parent.path, relativePath))

    override suspend fun write(obj: Wallet.Entry) {
        dir.writeString(path, JSON.stringify(obj))
    }

    override suspend fun get(): Wallet.Entry {
        return JSON.parse(dir.readString(path))
    }
}

class DBMapWalletEntry(dir: IOStorageDir, path: String)
    : DBMap<String, DBWalletEntry>(dir, path) {
    override suspend fun list(): Collection<String> {
        return dir.listFiles(path)
    }

    override fun get(origin: String): DBWalletEntry {
        return DBWalletEntry(dir, PathUtils.appendDir(path, origin))
    }

    override suspend fun remove(key: String) {
        dir.remove(PathUtils.appendDir(path, key))
    }

    suspend fun listEntries(): List<Wallet.Entry> {
        return list().map { get(it).get() }
    }
}

class Wallet(storageDir: StorageDir): StorageDirObject(storageDir) {
    val PASSWORD_DIR = "passwords"
    val passwordMap = DBMapWalletEntry(storageDir, PASSWORD_DIR)

    @Serializable
    class Entry(val origin: String, val username: String, val password: String)

    fun getId(): String {
        return storageDir.branch
    }

    suspend fun putPassword(origin: String, username: String, password: String) {
        passwordMap.get(origin).write(Entry(origin, username, password))
        commit()
    }

    suspend fun getPassword(origin: String): Entry? {
        val entry = passwordMap.get(origin)
        if (!entry.exists())
            return null
        return entry.get()
    }

    suspend fun getPasswordOrigins(): Collection<String> {
        return storageDir.listDirectories(PASSWORD_DIR)
    }
}
