requirejs.config({
    // path where all main libs are located, e.g. kotlin.js
    baseUrl: '/base/jsbindings/build/lib',

    paths: { // paths are relative to this file
        'pako': '../../src/main/resources/libs/pako.min',
        'jsbindings': '../../build/classes/main/jsbindings',
        'jsbindings_test': '../../build/classes/test/jsbindings_test',
    },

    deps: ['pako', 'jsbindings_test'],

    // start tests when done
    callback: window.__karma__.start
});