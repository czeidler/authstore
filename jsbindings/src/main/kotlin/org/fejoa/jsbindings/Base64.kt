package org.fejoa.jsbindings

import org.khronos.webgl.ArrayBuffer

external object base64js {
    fun encode(buffer: ByteArray): String
    fun decode(str: String): ArrayBuffer
}