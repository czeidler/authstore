@file:JsModule("pako")
package io.github.nodeca.pako

import org.khronos.webgl.Uint8Array

external fun deflate(data: Uint8Array): ByteArray
external fun inflate(data: Uint8Array): ByteArray
