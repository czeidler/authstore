requirejs.config({
    // path where all main libs are located, e.g. kotlin.js
    baseUrl: '/base/libJS/build/lib',

    paths: { // paths are relative to this file
        'pako': '../../../jsbindings/build/resources/main/libs/pako.min',
        'libJS': '../../build/classes/main/libJS',
        'libJS_test': '../../build/classes/test/libJS_test',
    },

    deps: ['libJS_test'],

    // start tests when done
    callback: window.__karma__.start
});
