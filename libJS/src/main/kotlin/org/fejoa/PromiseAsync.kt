package org.fejoa

import org.fejoa.support.Future
import kotlin.coroutines.experimental.suspendCoroutine
import kotlin.js.Promise


suspend fun <T> Promise<T>.await() = suspendCoroutine<T> { cont ->
    then({ value -> cont.resume(value) }, { exception -> cont.resumeWithException(exception) })
}

fun <T>Promise<T>.toFuture(): Future<T> {
    val promise = Future<T>()
    this.then({
        promise.setResult(it)
    }, {
        promise.setError(it)
    })
    return promise
}
