package org.fejoa.crypto

import org.fejoa.await
import org.fejoa.toFuture
import org.fejoa.jsbindings.CryptoKey
import org.fejoa.jsbindings.CryptoKeyPair
import org.fejoa.jsbindings.crypto
import org.fejoa.support.*
import org.khronos.webgl.Int8Array
import org.khronos.webgl.Uint8Array
import kotlin.browser.window
import kotlin.js.Json
import kotlin.js.json


interface JSKey {
    val key: CryptoKey
}

class JsSecretKey(override val key: CryptoKey, algo: CryptoSettings.SYM_ALGO) : SecretKey, JSKey {
    override val algo: CryptoSettings.SYM_ALGO = algo
}

class JSPublicKey(override val key: CryptoKey, algo: CryptoSettings.ASYM_ALGO) : PublicKey, JSKey {
    override val algo: CryptoSettings.ASYM_ALGO = algo
}

class JSPrivateKey(override val key: CryptoKey, algo: CryptoSettings.ASYM_ALGO) : PrivateKey, JSKey {
    override val algo: CryptoSettings.ASYM_ALGO = algo
}

class SubtleCrypto : CryptoInterface {
    override suspend fun deriveKey(secret: String, salt: ByteArray, settings: CryptoSettings.KDF): SecretKey {
        val passwordKey = crypto.subtle.importKey(
                "raw",
                secret.toUTF(),
                json("name" to settings.algo.jsName),
                false,
                arrayOf("deriveBits", "deriveKey")
        ).await()

        val derivedKey = crypto.subtle.deriveKey(
                    json("name" to settings.algo.jsName,
                            "salt" to salt,
                            "iterations" to settings.params.iterations,
                            "hash" to settings.algo.hash.jsName
                    ),
                    passwordKey,
                    json("name" to settings.keyAlgo.jsName,
                            "length" to settings.keySize),
                    true,
                    arrayOf("encrypt", "decrypt")).await()

        return JsSecretKey(derivedKey, settings.keyAlgo)
    }

    override suspend fun generateKeyPair(settings: CryptoSettings.Asymmetric): KeyPair {
        val purpose = when(settings.algo.type) {
            CryptoSettings.ASYM_TYPE.SIGNING -> arrayOf("sign", "verify")
            CryptoSettings.ASYM_TYPE.ENCRYPTION -> arrayOf("encrypt", "decrypt")
        }
        val pairRaw = when (settings) {
            is CryptoSettings.ECAsymmetric -> {
                    crypto.subtle.generateKey(
                            json(
                                    "name" to settings.algo.jsName,
                                    "namedCurve" to settings.algo.jsCurve
                            ),
                            true,
                            purpose).await()
                }
            is CryptoSettings.RSAAsymmetric -> {
                    crypto.subtle.generateKey(
                            json(
                                    "name" to settings.algo.jsName,
                                    "modulusLength" to settings.modulusLength,
                                    "publicExponent" to Uint8Array(settings.publicExponent.jsValue),
                                    "hash" to settings.algo.hash.jsName
                            ),
                            true,
                            purpose).await()
                }
            else -> {
                throw Exception("Unsupported")
            }
        }

        val pair = pairRaw.unsafeCast<CryptoKeyPair>()
        return KeyPair(JSPublicKey(pair.publicKey, settings.algo),
                JSPrivateKey(pair.privateKey, settings.algo))
    }

    override suspend fun generateSymmetricKey(settings: CryptoSettings.Symmetric): SecretKey {
        val algorithm = json("name" to settings.algo.jsName,
                "length" to settings.keySize)

        val key = crypto.subtle.generateKey(algorithm, true,
                arrayOf("encrypt", "decrypt")).await()
        return JsSecretKey(key, settings.algo)
    }

    override suspend fun encryptAsymmetric(input: ByteArray, key: PublicKey, algo: CryptoSettings.ASYM_ALGO)
            : ByteArray {
        val encrypted = crypto.subtle.encrypt(
                json(
                    "name" to algo.jsName
                ),
                (key as JSPublicKey).key,
                input
        ).await()
        return Int8Array(encrypted).unsafeCast<ByteArray>()
    }

    override suspend fun decryptAsymmetric(input: ByteArray, key: PrivateKey, algo: CryptoSettings.ASYM_ALGO)
            : ByteArray {
        val decrypted = crypto.subtle.decrypt(
                json(
                        "name" to algo.jsName
                ),
                (key as JSPrivateKey).key,
                input
        ).await()
        return Int8Array(decrypted).unsafeCast<ByteArray>()
    }

    override suspend fun encryptSymmetric(input: ByteArray, secretKey: SecretKey, iv: ByteArray,
                                  algo: CryptoSettings.SYM_ALGO): ByteArray {
        val algorithm = json("name" to algo.jsName,
                "counter" to iv,
                "length" to iv.size)
        val cryptoKey = (secretKey as JsSecretKey).key
        val encrypted = crypto.subtle.encrypt(algorithm, cryptoKey, input).toFuture().await()
        return Int8Array(encrypted).unsafeCast<ByteArray>()
    }

    override suspend fun decryptSymmetric(input: ByteArray, secretKey: SecretKey, iv: ByteArray,
                                  algo: CryptoSettings.SYM_ALGO): ByteArray {
        val algorithm = json("name" to algo.jsName,
                "counter" to iv,
                "length" to iv.size)
        val cryptoKey = (secretKey as JsSecretKey).key
        val raw = crypto.subtle.decrypt(algorithm, cryptoKey, input).toFuture().await()
        return Int8Array(raw).unsafeCast<ByteArray>()
    }

    override suspend fun encode(key: Key): ByteArray {
        val format = when (key) {
            is JsSecretKey -> "raw"
            is JSPrivateKey -> "pkcs8"
            is JSPublicKey -> "spki"
            else -> throw Exception("Unknown key")
        }

        val raw = crypto.subtle.exportKey(
                format,
                (key as JSKey).key
        ).await()
        return Int8Array(raw).unsafeCast<ByteArray>()
    }

    private suspend fun importKey(format: String, rawKey: ByteArray, algo: Json,
                                  purpose: Array<String>): CryptoKey {
        return crypto.subtle.importKey(format,
                rawKey,
                algo,
                true,
                purpose).await()
    }

    override suspend fun secretKeyFromRaw(key: ByteArray, algo: CryptoSettings.SYM_ALGO): SecretKey {
        val rawKey = importKey("raw", key, json("name" to algo.jsName),
                arrayOf("encrypt", "decrypt"))
        return JsSecretKey(rawKey, algo)
    }

    private fun getAsymImportAlgo(algo: CryptoSettings.ASYM_ALGO): Json {
        return when (algo) {
            CryptoSettings.ASYM_ALGO.RSA_OAEP_SHA256 -> json(
                    "name" to algo.jsName,
                    "hash" to algo.hash.jsName)
            CryptoSettings.ASYM_ALGO.RSASSA_PKCS1_v1_5_SHA256 -> json(
                    "name" to algo.jsName,
                    "hash" to algo.hash.jsName)
            CryptoSettings.ASYM_ALGO.ECDSA_SECP256R1_SHA256 -> json(
                    "name" to algo.jsName,
                    "namedCurve" to algo.jsCurve)
        }
    }
    override suspend fun privateKeyFromRaw(key: ByteArray, algo: CryptoSettings.ASYM_ALGO): PrivateKey {
        val purpose = when (algo.type) {
            CryptoSettings.ASYM_TYPE.SIGNING -> arrayOf("sign")
            CryptoSettings.ASYM_TYPE.ENCRYPTION -> arrayOf("decrypt")
        }

        val rawKey = importKey("pkcs8", key, getAsymImportAlgo(algo), purpose)
        return JSPrivateKey(rawKey, algo)
    }

    override suspend fun publicKeyFromRaw(key: ByteArray, algo: CryptoSettings.ASYM_ALGO): PublicKey {
        val purpose = when (algo.type) {
            CryptoSettings.ASYM_TYPE.SIGNING -> arrayOf("verify")
            CryptoSettings.ASYM_TYPE.ENCRYPTION -> arrayOf("encrypt")
        }
        val rawKey = importKey("spki", key, getAsymImportAlgo(algo), purpose)
        return JSPublicKey(rawKey, algo)
    }

    override suspend fun sign(input: ByteArray, key: PrivateKey, algo: CryptoSettings.ASYM_ALGO): ByteArray {
        val raw = when (algo) {
            CryptoSettings.ASYM_ALGO.RSASSA_PKCS1_v1_5_SHA256 -> {
                crypto.subtle.sign(
                        json(
                            "name" to algo.jsName
                        ),
                        (key as JSKey).key,
                        input
                ).await()
            }
            CryptoSettings.ASYM_ALGO.ECDSA_SECP256R1_SHA256 -> {
                crypto.subtle.sign(
                        json(
                                "name" to algo.jsName,
                                "hash" to algo.hash.jsName
                        ),
                        (key as JSKey).key,
                        input
                ).await()
            }
            CryptoSettings.ASYM_ALGO.RSA_OAEP_SHA256 -> throw Exception("Invalid algorithm")
        }

        return Int8Array(raw).unsafeCast<ByteArray>()
    }

    override suspend fun verifySignature(message: ByteArray, signature: ByteArray, key: PublicKey,
                                         algo: CryptoSettings.ASYM_ALGO): Boolean {
        // browser expect a simple signature: convert spki to this signature
        val sig = if (algo == CryptoSettings.ASYM_ALGO.ECDSA_SECP256R1_SHA256 && signature.size > 64) {
            val entry = ASN1Entry.parse(signature) ?: throw Exception("Can't parse spki")
            var first = entry.children[0].byteArray
            if (first.size == 33)
                first = first.copyOfRange(1, 33)
            var second = entry.children[1].byteArray
            if (second.size == 33)
                second = second.copyOfRange(1, 33)
            val out = ByteArrayOutStream()
            out.write(first)
            out.write(second)
            out.toByteArray()
        } else
            signature

        return when (algo) {
            CryptoSettings.ASYM_ALGO.RSASSA_PKCS1_v1_5_SHA256 -> {
                crypto.subtle.verify(
                        json(
                                "name" to algo.jsName
                        ),
                        (key as JSKey).key,
                        sig,
                        message
                ).await()
            }
            CryptoSettings.ASYM_ALGO.ECDSA_SECP256R1_SHA256 -> {
                crypto.subtle.verify(
                        json(
                                "name" to algo.jsName,
                                "hash" to algo.hash.jsName
                        ),
                        (key as JSKey).key,
                        sig,
                        message
                ).await()
            }
            CryptoSettings.ASYM_ALGO.RSA_OAEP_SHA256 -> throw Exception("Invalid algorithm")
        }
    }
}
