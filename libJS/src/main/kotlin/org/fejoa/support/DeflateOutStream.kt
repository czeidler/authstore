package org.fejoa.support

import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.Uint8Array
import io.github.nodeca.pako.deflate


actual class DeflateOutStream actual constructor(val outStream: OutStream) : OutStream {
    val buffer = ByteArrayOutStream()

    override fun write(byte: Byte): Int {
        return buffer.write(byte)
    }

    override fun close() {
        val data = buffer.toByteArray()
        val compressed = deflate(Uint8Array(data.unsafeCast<ArrayBuffer>()))
        outStream.write(compressed)
        super.close()
    }
}
