package org.fejoa.support

import kotlin.js.Math.random


class RandomJS {
    constructor() {
    }

    constructor(seed: Long) {
        //TODO()
    }

    fun readByte(): Byte {
        return (readDouble() * 256 - 128).toByte()
    }

    fun read(buffer: ByteArray): Int {
        for (i in 0 until buffer.size)
            buffer[i] = readByte()
        return buffer.size
    }

    fun readFloat(): Float {
        return readDouble().toFloat()
    }

    fun readDouble(): Double {
        return random()
    }
}

actual typealias Random = RandomJS