package org.fejoa.support

import org.fejoa.jsbindings.base64js
import org.khronos.webgl.Int8Array


actual fun encodeBase64String(data: ByteArray): String {
    return base64js.encode(data)
}

actual fun decodeBase64(string: String): ByteArray {
    return Int8Array(base64js.decode(string)).unsafeCast<ByteArray>()
}
