package org.fejoa.repository.sync

import org.fejoa.network.RemotePipe
import org.fejoa.repository.BranchLog
import org.fejoa.repository.RepositoryTestBase
import org.fejoa.repository.ThreeWayMerge
import org.fejoa.storage.IODatabase
import org.fejoa.support.*
import org.fejoa.test.testAsync
import kotlin.test.*


class PullPushTest : RepositoryTestBase() {
    private fun connect(handler: RequestHandler): RemotePipe {
        return object : RemotePipe {
            private var backingOutStream: AsyncByteArrayOutStream? = AsyncByteArrayOutStream()
            private var backingInStream: AsyncInStream? = null

            override val outStream: AsyncOutStream = object : AsyncOutStream {
                override suspend fun write(buffer: ByteArray, offset: Int, length: Int): Int {
                    backingInStream = null
                    val outStream = backingOutStream
                            ?: AsyncByteArrayOutStream().also { backingOutStream = it }
                    return outStream.write(buffer, offset, length)
                }
            }

            override val inStream: AsyncInStream = object : AsyncInStream {
                override suspend fun read(buffer: ByteArray, offset: Int, length: Int): Int {
                    val stream = backingInStream ?: run {
                        val inputStream = ByteArrayInStream(backingOutStream!!.toByteArray()).toAsyncInputStream()
                        backingOutStream = null

                        val reply = AsyncByteArrayOutStream()
                        val result = handler.handle(object : RemotePipe {
                            override val inStream: AsyncInStream
                                get() = inputStream

                            override val outStream: AsyncOutStream
                                get() = reply
                        }, AccessRight.ALL)
                        if (result != RequestHandler.Result.OK)
                            throw Exception("Error in handler ${result.value}, ${result.description}")

                        val replyStream = ByteArrayInStream(reply.toByteArray()).toAsyncInputStream()
                        backingInStream = replyStream
                        replyStream
                    }

                    return stream.read(buffer, offset, length)
                }
            }
        }
    }

    @Test
    fun testPull() = testAsync({ setUp() }, { tearDown() }) {
        val branch = "pullBranch"
        val localDir = "PullLocalTest"
        val pullAllDir = "PullAllTest"
        val remoteDir = "PullRemoteTest"

        val requestRepo = createRepo(localDir, branch, false)
        val remoteRepo = createRepo(remoteDir, branch)
        val transactionManager = TransactionManager()
        val handler = RequestHandler(branch, transactionManager, remoteRepo.branchBackend,
                object : RequestHandler.BranchLogGetter {
                    override fun get(b: String): BranchLog? {
                        return if (branch == b)
                            remoteRepo.log
                        else
                            null
                    }
                })
        val senderPipe = connect(handler)

        var missingTip = GetLogEntryProtocol.getRemoteTip(senderPipe, "missingbranch").logTip
        assertNull(missingTip)

        val remoteTip = GetLogEntryProtocol.getRemoteTip(senderPipe, branch).logTip!!
        assertEquals(remoteRepo.log.getHead()!!.entryId, remoteTip.entryId)

        val pullRequest = PullRequest(requestRepo, null)
        var pulledTip = pullRequest.pull(senderPipe, branch, ThreeWayMerge())
        assertTrue(pulledTip!!.value.isZero)

        // change the remote repo
        val testRemoteRepo = TestRepository(remoteRepo)
        testRemoteRepo.putBlob("testFile", "Hello World")
        testRemoteRepo.commit("")
        var remoteHead = remoteRepo.getHead()

        pulledTip = pullRequest.pull(senderPipe, branch, ThreeWayMerge())
                ?: throw Exception("Remote head should not be null")
        testRemoteRepo.verify(requestRepo)

        assertTrue(pulledTip == remoteHead)
        val requestBranchLogEntry = requestRepo.branchLogIO.readFromLog(requestRepo.log.getHead()!!.message.data).head
        assertEquals(pulledTip, requestBranchLogEntry)

        // make another remote
        testRemoteRepo.putBlob("testFile2", "Hello World 2")
        testRemoteRepo.putBlob("sub/testFile", "Hello World 3")
        testRemoteRepo.putBlob("sub/testFile2", "Hello World 4")
        testRemoteRepo.commit("")
        remoteHead = remoteRepo.getHead()

        pulledTip = pullRequest.pull(senderPipe, branch, ThreeWayMerge())
                ?: throw Exception("Remote head should not be null")
        testRemoteRepo.verify(requestRepo)

        assertTrue(pulledTip == remoteHead)
        assertEquals(pulledTip, requestRepo.branchLogIO.readFromLog(requestRepo.log.getHead()!!.message.data).head)

        // pull all
        var pullAllRepo = createRepo(pullAllDir, branch)
        val transaction = pullAllRepo.getCurrentTransaction().getRawAccessor()
        ChunkIteratorProtocol.getAllChunks(senderPipe, branch, 2) {
            it.forEach {
                val putResult = transaction.putChunk(it.chunk).await()
                assertEquals(it.id, putResult.key)
            }
        }
        // just open with the know ref
        pullAllRepo = openRepo(pullAllDir, branch, remoteRepo.getRepositoryRef())
        testRemoteRepo.verify(pullAllRepo)
    }

    @Test
    fun testPullMerge() = testAsync({ setUp() }, { tearDown() }) {
        val branch = "pullBranch"
        val localDir = "PullMergeLocalTest"
        val remoteDir = "PullMergeRemoteTest"

        val requestRepo = createRepo(localDir, branch, false)
        val remoteRepo = createRepo(remoteDir, branch)
        val transactionManager = TransactionManager()
        val handler = RequestHandler(branch, transactionManager, remoteRepo.branchBackend,
                object : RequestHandler.BranchLogGetter {
                    override fun get(b: String): BranchLog? {
                        return if (branch == b)
                            remoteRepo.log
                        else
                            null
                    }
                })
        val senderPipe = connect(handler)

        // change the remote repo
        val testRemoteRepo = TestRepository(remoteRepo)
        testRemoteRepo.putBlob("testFile", "Hello World")
        testRemoteRepo.commit("")
        var remoteHead = remoteRepo.getHead()

        val pullRequest = PullRequest(requestRepo, null)
        var pulledTip = pullRequest.pull(senderPipe, branch, ThreeWayMerge())
                ?: throw Exception("Remote head should not be null")
        testRemoteRepo.verify(requestRepo)

        assertTrue(pulledTip == remoteHead)
        val requestBranchLogEntry = requestRepo.branchLogIO.readFromLog(requestRepo.log.getHead()!!.message.data).head
        assertEquals(pulledTip, requestBranchLogEntry)

        // make another remote
        testRemoteRepo.putBlob("testFile2", "Hello World 2")
        testRemoteRepo.putBlob("sub/testFile", "Hello World 3")
        testRemoteRepo.putBlob("sub/testFile2", "Hello World 4")
        testRemoteRepo.commit("")
        remoteHead = remoteRepo.getHead()

        pulledTip = pullRequest.pull(senderPipe, branch, ThreeWayMerge())
                ?: throw Exception("Remote head should not be null")
        testRemoteRepo.verify(requestRepo)

        assertTrue(pulledTip == remoteHead)
        assertEquals(pulledTip, requestRepo.branchLogIO.readFromLog(requestRepo.log.getHead()!!.message.data).head)

        // add something to the local repo and pull; expect a fast forward merge
        requestRepo.putBytes("locallyAdded", "data".toUTF())
        requestRepo.commit("Local commit".toUTF(), null)

        (requestRepo.probe("locallyAdded") != IODatabase.FileType.NOT_EXISTING).let { assertTrue { it } }

        pulledTip = pullRequest.pull(senderPipe, branch, ThreeWayMerge())
                ?: throw Exception("Remote head should not be null")

        (requestRepo.probe("locallyAdded") != IODatabase.FileType.NOT_EXISTING).let { assertTrue { it } }

        assertTrue { requestRepo.getHeadCommit()!!.parents.size == 1 }
    }

    @Test
    fun testPush() = testAsync({ setUp() }, { tearDown() }) {
        val branch = "pushBranch"
        val localDir = "PushLocalTest"
        val remoteDir = "PushRemoteTest"

        val localRepo = createRepo(localDir, branch)
        var remoteRepo = createRepo(remoteDir, branch)

        val transactionManager = TransactionManager()
        val handler = RequestHandler(branch, transactionManager, remoteRepo.branchBackend,
                object : RequestHandler.BranchLogGetter {
                    override fun get(branch: String): BranchLog? {
                        return remoteRepo.log
                    }
                })
        val senderPipe = connect(handler)

        val testLocalRepo = TestRepository(localRepo)
        // change the local repo
        testLocalRepo.putBlob("testFile", "Hello World!")
        testLocalRepo.commit("Initial commit")

        val localTransaction = localRepo.getCurrentTransaction()
        val pushRequest = PushRequest(localRepo)
        // push changes
        pushRequest.push(senderPipe, localTransaction, branch)
        // verify
        var remoteHeadRef = remoteRepo.branchLogIO.readFromLog(remoteRepo.log.getHead()!!.message.data)
        remoteRepo = openRepo(remoteDir, branch, remoteHeadRef)
        testLocalRepo.verify(remoteRepo)

        // add more
        testLocalRepo.putBlob("testFile2", "Hello World 2")
        testLocalRepo.putBlob("sub/testFile3", "Hello World 3")
        testLocalRepo.putBlob("sub/sub2/testFile4", "Hello World 4")
        testLocalRepo.commit("Commit 1")

        testLocalRepo.putBlob("sub/sub2/testFile4", "Hello World 5")
        testLocalRepo.commit("Commit 2")

        // push changes
        pushRequest.push(senderPipe, localTransaction, branch)
        // verify
        remoteHeadRef = remoteRepo.branchLogIO.readFromLog(remoteRepo.log.getHead()!!.message.data)
        remoteRepo = openRepo(remoteDir, branch, remoteHeadRef)

        testLocalRepo.verify(remoteRepo)
    }
}

