package org.fejoa.storage


interface MergeStrategy {
    /**
     * Merges theirs into ours
     */
    suspend fun merge(ours: Database, theirs: Database, commonAncestor: Database)
}


class KeepOursUnchanged : MergeStrategy {
    override suspend fun merge(ours: Database, theirs: Database, commonAncestor: Database) {
        // Do nothing just keep our branch
    }
}