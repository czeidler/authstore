package org.fejoa.repository

import kotlinx.coroutines.experimental.Unconfined
import kotlinx.serialization.SerialId
import kotlinx.serialization.Serializable
import kotlinx.serialization.protobuf.ProtoBuf
import org.fejoa.chunkcontainer.*
import org.fejoa.crypto.SecretKeyData
import org.fejoa.protocolbufferlight.ProtocolBufferLight
import org.fejoa.storage.*
import org.fejoa.support.*


class RepositoryConfig(val hashSpec: HashSpec,
                       val boxSpec: BoxSpec = BoxSpec(),
                       val revLog: RevLogConfig = RevLogConfig()) {
    val containerSpec: ContainerSpec
        get() = ContainerSpec(hashSpec.createChild(), boxSpec)

    companion object {
        val HASH_SPEC_TAG = 0
        val BOX_SPEC_TAG = 1
        val REV_LOG_TAG = 2

        /**
         * @param default if hashSpec or boxSpec are not included in the buffer these values are taken from default
         */
        suspend fun read(protoBuffer: ProtocolBufferLight): RepositoryConfig {
            val hashSpec = protoBuffer.getBytes(HASH_SPEC_TAG)?.let {
                return@let HashSpec.read(ByteArrayInStream(it).toAsyncInputStream(), null)
            } ?: throw Exception("Failed to read repo config hash spec")

            val boxSpec = protoBuffer.getBytes(BOX_SPEC_TAG)?.let {
                return@let BoxSpec.read(ByteArrayInStream(it).toAsyncInputStream())
            } ?: throw Exception("Failed to read repo config box spec")

            val revLog = protoBuffer.getBytes(REV_LOG_TAG)?.let {
                return@let ProtoBuf.load<RevLogConfig>(it)
            } ?: throw Exception("Rev log config expected")

            return RepositoryConfig(hashSpec, boxSpec, revLog)
        }
    }

    /**
     * @param default hashSpec and boxSpec are only written if they are different from the parameters in default
     */
    suspend fun write(protoBuffer: ProtocolBufferLight) {
        var outStream = AsyncByteArrayOutStream()

        hashSpec.write(outStream)
        protoBuffer.put(HASH_SPEC_TAG, outStream.toByteArray())

        outStream = AsyncByteArrayOutStream()
        boxSpec.write(outStream)
        protoBuffer.put(BOX_SPEC_TAG, outStream.toByteArray())

        protoBuffer.put(REV_LOG_TAG, ProtoBuf.dump(revLog))
    }
}


@Serializable
class RevLogConfig(@SerialId(0) val maxEntrySize: Long = 4 * 1024 * 1024)

class RepositoryRef(val objectIndexRef: ChunkContainerRef, val head: Hash, val config: RepositoryConfig) {
    companion object {
        val OBJECT_INDEX_REF_TAG = 0
        val HEAD_TAG = 1
        val CONFIG_TAG = 2

        suspend fun read(protoBuffer: ProtocolBufferLight): RepositoryRef {
            val config = protoBuffer.getBytes(CONFIG_TAG)?.let {
                val configBuffer = ProtocolBufferLight(it)
                return@let RepositoryConfig.read(configBuffer)
            } ?: throw Exception("Missing repo config")

            val objectIndexRef = protoBuffer.getBytes(OBJECT_INDEX_REF_TAG)?.let {
                return@let ChunkContainerRef.read(ByteArrayInStream(it).toAsyncInputStream(),
                        config.hashSpec.chunkingConfig)
            } ?: throw Exception("Missing object index ref")

            val head = protoBuffer.getBytes(HEAD_TAG)?.let {
                return@let Hash.read(ByteArrayInStream(it).toAsyncInputStream(), config.hashSpec.chunkingConfig)
            } ?: throw Exception("Missing repo head")

            return RepositoryRef(objectIndexRef, head, config)
        }
    }

    suspend fun write(protoBuffer: ProtocolBufferLight) {
        val configBuffer = ProtocolBufferLight()
        config.write(configBuffer)
        protoBuffer.put(CONFIG_TAG, configBuffer.toByteArray())

        var outStream = AsyncByteArrayOutStream()
        objectIndexRef.write(outStream)
        protoBuffer.put(OBJECT_INDEX_REF_TAG, outStream.toByteArray())

        outStream = AsyncByteArrayOutStream()
        head.write(outStream)
        protoBuffer.put(HEAD_TAG, outStream.toByteArray())
    }


}


/**
 * A repository can store multiple versions of data. Data is organized in a directory tree.
 */
class Repository private constructor(private val branch: String,
                                     val branchBackend: StorageBackend.BranchBackend,
                                     private val accessors: ChunkAccessors,
                                     transaction: ChunkAccessors.Transaction,
                                     val log: BranchLog,
                                     objectIndex: ObjectIndex,
                                     config: RepositoryConfig,
                                     val crypto: SecretKeyData?): Database {
    private val lock = SuspendQueue(Unconfined)
    private var transaction: LogRepoTransaction = LogRepoTransaction(transaction)
    private var headCommit: Commit? = null
    val commitCache = CommitCache(this)
    var config: RepositoryConfig = config
        private set
    var objectIndex: ObjectIndex = objectIndex
        private set
    // TODO restrict access to avoid possible coroutine concurrency problems when using ioDatabase without lock However,
    // currently it is only used in a safe way.
    var ioDatabase = IODatabaseCC(Directory("", config.hashSpec.createChild()), objectIndex, transaction, config.containerSpec)
        private set
    private val mergeParents = ArrayList<Hash>()
    val branchLogIO: BranchLogIO = if (crypto != null)
        RepositoryBuilder.getEncryptedBranchLogIO(crypto.key, crypto.algo)
    else
        RepositoryBuilder.getPlainBranchLogIO()

    /**
     * Reconfigures an empty repository (with no commits)
     *
     * All changes are discarded
     */
    suspend fun reset(config: RepositoryConfig) {
        if (headCommit != null)
            throw Exception("Repository is not empty")
        this.config = config

        // reset the object index
        val containerSpec = config.containerSpec
        val objectIndexCC = ChunkContainer.create(transaction.getObjectIndexAccessor(containerSpec),
                containerSpec)
        objectIndex = ObjectIndex.create(config, objectIndexCC, transaction.getRawAccessor())

        ioDatabase = IODatabaseCC(Directory("", config.hashSpec.createChild()), objectIndex, transaction,
                config.containerSpec)
    }

    companion object {
        /** The freshly created repository can be in an empty state and an uninitialized state.
         * In the uninitialized state the branch log head (BranchLogEntry) is null. In the empty state the head point to
         * a zero commit. This means there is no commit yet; the repository is initialized but empty.
         *
         * This distinction is necessary when synchronising repositories. For example, when the client is pushing a
         * repository, but the operation is canceled, already pushed chunks may remain in the chunkstore and be reused
         * for later operations. When another client tries to pull this uninitialized repository, it is essential to
         * know if the remote repository is empty (no commits) or uninitialized. For example, without this knowledge a
         * client may think an uninitialized repository is empty and then wrongly starts with a new root commit.
         *
         * @param init indicates if the repository should be initialized, i.e. it becomes an empty repository
         */
        suspend fun create(branch: String, branchBackend: StorageBackend.BranchBackend, config: RepositoryConfig,
                           crypto: SecretKeyData?, init: Boolean): Repository {
            val containerSpec = config.containerSpec
            val accessors: ChunkAccessors = RepoChunkAccessors(branchBackend.getChunkStorage(), config, crypto)
            val log: BranchLog = branchBackend.getBranchLog()
            val transaction = accessors.startTransaction()
            val objectIndexCC = ChunkContainer.create(transaction.getObjectIndexAccessor(containerSpec),
                    containerSpec)
            val objectIndex = ObjectIndex.create(config, objectIndexCC, transaction.getRawAccessor())
            val repo = Repository(branch, branchBackend, accessors, transaction, log, objectIndex, config, crypto)

            if (!init)
                return repo
            // Write a first zero log entry, this means the repo is initialized but there are no commits yet
            val repoRef = repo.getRepositoryRefUnlocked()
            val logIO = repo.branchLogIO
            branchBackend.getBranchLog().add(logIO.logHash(repoRef), logIO.writeToLog(repoRef), emptyList())

            return repo
        }

        suspend fun open(branch: String, ref: RepositoryRef, branchBackend: StorageBackend.BranchBackend,
                         crypto: SecretKeyData?): Repository {
            // if there are no commits simply call create
            if (ref.head.value.isZero)
                return create(branch, branchBackend, ref.config, crypto, false)

            val repoConfig = ref.config

            val containerSpec = repoConfig.containerSpec
            val accessors: ChunkAccessors = RepoChunkAccessors(branchBackend.getChunkStorage(), repoConfig, crypto)
            val log: BranchLog = branchBackend.getBranchLog()
            val transaction = accessors.startTransaction()
            val objectIndexCC = ChunkContainer.read(transaction.getObjectIndexAccessor(containerSpec),
                    ref.objectIndexRef)
            val objectIndex = ObjectIndex.open(repoConfig, objectIndexCC, transaction.getRawAccessor())

            val repository = Repository(branch, branchBackend, accessors, transaction, log, objectIndex, repoConfig,
                    crypto)
            repository.setHeadCommitUnlocked(ref.head)
            return repository
        }
    }

    fun getCurrentTransaction(): ChunkAccessors.Transaction {
        return transaction
    }

    override fun getBranch(): String {
        return branch
    }

    fun getHeadCommit(): Commit? {
        return headCommit
    }

    suspend fun setHeadCommit(commit: Hash) = lock.runSync {
        setHeadCommitUnlocked(commit)
    }

    private suspend fun setHeadCommitUnlocked(commit: Hash) {
        if (headCommit == null) {
            headCommit = Commit.read(commit, objectIndex)
        } else {
            headCommit = commitCache.getCommit(commit)
            if (headCommit == null)
                throw Exception("Invalid commit hash: ${commit.value}")
        }
        val rootDir = Directory.readRoot(headCommit!!.dir, objectIndex)
        ioDatabase.setRootDirectory(rootDir)
    }

    override suspend fun getHead(): Hash? = lock.runSync {
        getHeadUnlocked()
    }

    private suspend fun getHeadUnlocked(): Hash? {
        return getHeadCommit()?.getHash()?.clone()
    }

    private fun getParents(): Collection<HashValue> {
        return headCommit?.parents?.map { it.value } ?: emptyList()
    }

    override suspend fun merge(mergeParents: Collection<Database>, mergeStrategy: MergeStrategy)
            : Database.MergeResult = lock.runSync {
        var result = Database.MergeResult.FAST_FORWARD
        val allowFastForward = mergeParents.size == 1
        mergeParents.forEach {
            val singleResult = mergeSingleBranch(it, mergeStrategy, allowFastForward)
            if (singleResult == Database.MergeResult.MERGED)
                result = singleResult
        }
        if (result == Database.MergeResult.FAST_FORWARD) {
            val currentEntryId = branchBackend.getBranchLog().getHead()?.entryId
            val repoRef = getRepositoryRefUnlocked()
            val newEntryId = branchLogIO.logHash(repoRef)
            if (currentEntryId == null || currentEntryId != newEntryId)
                log.add(newEntryId, branchLogIO.writeToLog(repoRef), transaction.getObjectsWritten())
        } else
            this.mergeParents.addAll(mergeParents.mapNotNull { it.getHead() })
        return@runSync result
    }

    suspend fun isModified(): Boolean = lock.runSync {
        return@runSync isModifiedUnlocked()
    }

    private suspend fun isModifiedUnlocked(): Boolean {
        if (headCommit == null)
            return ioDatabase.treeAccessor.root.getChildren().isNotEmpty()

        val headHash = headCommit!!.dir
        val dirHash = ioDatabase.flush()
        if (headHash != dirHash)
            return true
        return false
    }

    private suspend fun mergeSingleBranch(theirs: Database, mergeStrategy: MergeStrategy,
                                          allowFastForward: Boolean): Database.MergeResult {
        if (theirs !is Repository)
            throw Exception("Unsupported repository")
        val theirsHead = theirs.getHeadUnlocked()

        val oursIsModified = isModifiedUnlocked()
        val theirsIsModified = theirs.isModifiedUnlocked()
        if (allowFastForward && !theirsIsModified) {
            if (theirsHead == null)
                return Database.MergeResult.FAST_FORWARD

            if (!oursIsModified && getHeadUnlocked() == theirsHead)
                return Database.MergeResult.FAST_FORWARD
        }
        if (theirsHead == null)
            throw Exception("Invalid branch")

        // pull missing objects into the our repository
        copyObjectIndexChunks(theirs.transaction.getRawAccessor(), theirs.objectIndex)
        val commit = theirs.getHeadCommit() ?: throw Exception("Invalid branch")
        copyMissingObjectRefs(commit, theirs.objectIndex, theirs.commitCache)

        if (allowFastForward && !oursIsModified && !theirsIsModified) {
            if (headCommit == null) {
                // we are empty; just use the other branch's head
                setHeadCommitUnlocked(theirsHead)
                return Database.MergeResult.FAST_FORWARD
            }
        }

        // 1) Find common ancestor
        // 2) Merge head with otherBranch
        val chains = CommonAncestorsFinder.find(commitCache, headCommit!!,
                theirs.commitCache, theirs.getHeadCommit()!!)

        val shortestChain = chains.shortestChain
                ?: throw IOException("Branches don't have common ancestor.")
        val ancestor = shortestChain.oldest

        if (allowFastForward && !oursIsModified && !theirsIsModified) {
            if (ancestor.getHash().value == headCommit!!.getHash().value) {
                // remote is ahead
                setHeadCommitUnlocked(theirsHead)
                return Database.MergeResult.FAST_FORWARD
            }
            if (ancestor.getHash().value == theirsHead.value) {
                // we are ahead
                return Database.MergeResult.FAST_FORWARD
            }
        }

        // flush to get a consistent state
        val objectIndexRef = objectIndex.flush()
        val ancestorRepo = Repository.open(branch,
                RepositoryRef(objectIndexRef, ancestor.getHash(), config), branchBackend, crypto)

        // merge branches
        mergeStrategy.merge(this, theirs, ancestorRepo)
        return Database.MergeResult.MERGED
    }

    /**
     * Adds the missing object refs to our object index
     */
    private suspend fun copyMissingObjectRefs(theirsCommit: Commit, theirsObjectIndex: ObjectIndex,
                                              theirsCommitCache: CommitCache) {
        // add commit
        writeCommit(theirsCommit)
        // add directory
        val theirsRoot = Directory.readRoot(theirsCommit.dir, theirsObjectIndex)
        writeTree(theirsRoot)
        // add blobs
        val diff = ioDatabase.getRootDirectory().getDiff(theirsRoot,
                includeAllAdded = true, includeAllRemoved = false).getAll()
        diff.filter { (it.type == DiffIterator.Type.ADDED || it.type == DiffIterator.Type.MODIFIED)
                && it.theirs!!.isFile()}
                .forEach {
                    val theirs = it.theirs!!
                    val container = theirsObjectIndex.getBlob(it.path, theirs.getHash())
                            ?: throw Exception("Blob not found ${it.path}")
                    objectIndex.putBlob(it.path, container)
                }
        for (parentRef in theirsCommit.parents) {
            val parent = theirsCommitCache.getCommit(parentRef) ?: throw Exception("Can't load commit")
            copyMissingObjectRefs(parent, theirsObjectIndex, theirsCommitCache)
        }
    }

    /**
     * Copies all missing chunks, as listed in the object index, from the source to chunk store to our store.
     */
    private suspend fun copyObjectIndexChunks(source: ChunkTransaction,
                                              theirsObjectIndex: ObjectIndex) {
        val chunkFetcher = ChunkFetcher.createLocalFetcher(transaction, source)
        chunkFetcher.enqueueObjectIndexJob(theirsObjectIndex)
        chunkFetcher.fetch()
    }

    override suspend fun commit(message: ByteArray, signature: CommitSignature?): Hash = lock.runSync {
        val hash = commitInternal(message, signature, mergeParents = mergeParents)
        mergeParents.clear()
        return@runSync hash
    }

    var allowCommit = true
    private suspend fun commitInternal(message: ByteArray, commitSignature: CommitSignature? = null,
                                       mergeParents: Collection<Hash>): Hash {
        if (!allowCommit)
            throw Exception("Commits are not allowed in this repo")
        // check if we need to commit
        if (!isModifiedUnlocked() && mergeParents.isEmpty())
            return headCommit?.getHash() ?: Hash.createChild(config.hashSpec.createChild())

        // flush in any case to write open write handles and to be able to determine if we need to commit
        val rootTree = ioDatabase.flush()
        if (mergeParents.isEmpty() && headCommit != null && headCommit!!.getHash() == rootTree)
            return headCommit!!.getHash()

        // write entries to the objectIndex
        ioDatabase.getModifiedChunkContainer().forEach {
            objectIndex.putBlob(it.key, it.value)
        }
        ioDatabase.clearModifiedChunkContainer()
        writeTree(ioDatabase.getRootDirectory())
        // write the rootTree to the object index
        val commit = Commit(rootTree, Hash.createChild(config.hashSpec))

        if (headCommit != null)
            commit.parents.add(headCommit!!.getHash())

        for (mergeParent in mergeParents)
            commit.parents.add(mergeParent)

        if (commitSignature != null)
            commit.message = commitSignature.signMessage(message, rootTree.value, getParents())
        else
            commit.message = message
        writeCommit(commit)
        headCommit = commit

        val objectIndexRef =  objectIndex.flush()
        val repoRef = RepositoryRef(objectIndexRef, commit.getHash(), config)
        transaction.finishTransaction()
        log.add(branchLogIO.logHash(repoRef), branchLogIO.writeToLog(repoRef), transaction.getObjectsWritten())
        transaction = LogRepoTransaction(accessors.startTransaction())
        ioDatabase.setTransaction(transaction)

        return commit.getHash()
    }

    suspend fun getRepositoryRef(): RepositoryRef = lock.runSync {
        getRepositoryRefUnlocked()
    }

    private suspend fun getRepositoryRefUnlocked(): RepositoryRef {
        val head = getHeadUnlocked() ?: Hash(config.hashSpec, Config.newDataHash())
        return RepositoryRef(objectIndex.chunkContainer.ref, head, config)
    }

    private suspend fun writeTree(tree: Directory): Hash {
        val containerSpec = config.containerSpec
        val chunkContainer = ChunkContainer.create(transaction.getTreeAccessor(containerSpec),
                containerSpec)
        val outStream = ChunkContainerOutStream(chunkContainer)
        tree.writeRoot(outStream)
        outStream.close()
        objectIndex.putDir(chunkContainer)
        return chunkContainer.ref.hash
    }

    private suspend fun writeCommit(commit: Commit) {
        val containerSpec = config.containerSpec
        val chunkContainer = ChunkContainer.create(transaction.getCommitAccessor(containerSpec),
                containerSpec)
        val outStream = ChunkContainerOutStream(chunkContainer)
        commit.write(outStream)
        outStream.close()
        objectIndex.putCommit(chunkContainer)
    }

    override suspend fun getDiff(baseCommit: HashValue, endCommit: HashValue): DatabaseDiff = lock.runSync {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun getHash(path: String): HashValue = lock.runSync {
        return@runSync ioDatabase.getHash(path)
    }

    override suspend fun probe(path: String): IODatabase.FileType = lock.runSync {
        return@runSync ioDatabase.probe(path)
    }

    override suspend fun open(path: String, mode: RandomDataAccess.Mode): RandomDataAccess = lock.runSync {
        return@runSync ioDatabase.open(path, mode)
    }

    override suspend fun remove(path: String) = lock.runSync {
        ioDatabase.remove(path)
    }

    override suspend fun listFiles(path: String): Collection<String> = lock.runSync {
        return@runSync ioDatabase.listFiles(path)
    }

    override suspend fun listDirectories(path: String): Collection<String> = lock.runSync {
        return@runSync ioDatabase.listDirectories(path)
    }

    override suspend fun readBytes(path: String): ByteArray = lock.runSync {
        return@runSync ioDatabase.readBytes(path)
    }

    override suspend fun putBytes(path: String, data: ByteArray) = lock.runSync {
        ioDatabase.putBytes(path, data)
    }

    suspend fun printContent(dataPrinter: (data: ByteArray) -> String = { it.toUTFString() })
            = printContent("", 0, dataPrinter)

    suspend fun printContent(path: String, nSubDir: Int,
                             dataPrinter: (data: ByteArray) -> String = { it.toUTFString() }): String {
        var out = ""
        for (dir in listDirectories(path)) {
            for (i in 0 until nSubDir)
                out += "  "
            out += dir + "\n"
            out += printContent(PathUtils.appendDir(path, dir), nSubDir + 1, dataPrinter)
        }
        for (file in listFiles(path)) {
            for (i in 0 until nSubDir)
                out += "  "
            out += file
            out += " -> "
            try {
                out += dataPrinter.invoke(readBytes(PathUtils.appendDir(path, file)))
            } catch (e: Throwable) {
                out += "Error loading $path"
            }
            out += "\n"
        }
        return out
    }

    suspend fun printHistory(): String {
        var out = ""
        val head = getHeadUnlocked() ?: return out

        val handled: MutableSet<HashValue> = HashSet()
        val ongoing: MutableList<Hash> = ArrayList()
        ongoing.add(head)
        while (ongoing.isNotEmpty()) {
            val current = ongoing.removeAt(0)
            if (handled.contains(current.value))
                continue
            else
                handled.add(current.value)
            out += current.value.toHex()
            val currentCommit = commitCache.getCommit(current)
            if (currentCommit == null) {
                out += "Failed to read commit\n"
                continue
            }
            if (currentCommit.getHash().value != current.value)
                throw Exception()
            if (currentCommit.parents.isNotEmpty()) {
                out += " parents=["
                currentCommit.parents.forEachIndexed { index, hash ->
                    ongoing.add(hash)
                    out += hash.value
                    if (index != currentCommit.parents.lastIndex)
                        out += ", "
                }
                out += "]"
            }
            out += "\n"
        }
        return out
    }
}