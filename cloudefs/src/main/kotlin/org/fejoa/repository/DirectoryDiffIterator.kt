package org.fejoa.repository


class DirectoryDiffIterator(basePath: String, ours: Directory?, theirs: Directory)
    : DiffIterator<DirectoryEntry>(basePath, ours?.getChildren() ?: emptyList(), theirs.getChildren(),
        object : DiffIterator.EntryAccessor<DirectoryEntry> {
            override fun getName(entry: DirectoryEntry): String {
                return entry.name
            }
            override suspend fun contentEquals(entry: DirectoryEntry, entry2: DirectoryEntry): Boolean {
                return entry.getHash().value == entry2.getHash().value
            }
        })
