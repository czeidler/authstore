package org.fejoa.repository

import kotlinx.serialization.*
import kotlinx.serialization.internal.SerialClassDescImpl

import org.fejoa.protocolbufferlight.ProtocolBufferLight
import org.fejoa.storage.Config
import org.fejoa.storage.HashValue
import org.fejoa.support.decodeBase64
import org.fejoa.support.encodeBase64


@Serializer(forClass = BranchLogEntry.Message::class)
object BranchLogMessageJsonSerializer : KSerializer<BranchLogEntry.Message> {
    override val serialClassDesc: KSerialClassDesc
        get() = SerialClassDescImpl("org.fejoa.repository.BranchLogEntry.Message")

    override fun save(output: KOutput, obj: BranchLogEntry.Message) {
        output.writeStringValue(obj.data.encodeBase64())
    }

    override fun load(input: KInput): BranchLogEntry.Message {
        return BranchLogEntry.Message(input.readStringValue().decodeBase64())
    }
}

@Serializable
class BranchLogEntry(@SerialId(TIME_TAG) val time: Long = 0L,
                     @SerialId(ENTRY_ID_TAG) var entryId: HashValue = Config.newDataHash(),
                     @SerialId(MESSAGE_TAG) var message: Message = Message(),
                     @SerialId(CHANGES_TAG) @Optional val changes: MutableList<HashValue> = ArrayList()) {
    class Message(val data: ByteArray = ByteArray(0))

    companion object {
        const val TIME_TAG = 0
        const val ENTRY_ID_TAG = 1
        const val MESSAGE_TAG = 2
        const val CHANGES_TAG = 3

        fun read(buffer: ByteArray): BranchLogEntry {
            val protoBuf = ProtocolBufferLight(buffer)
            val time = protoBuf.getLong(TIME_TAG) ?: throw Exception("Time missing")
            val entryId = protoBuf.getBytes(ENTRY_ID_TAG) ?: throw Exception("Entry id missing")
            val message = protoBuf.getBytes(MESSAGE_TAG) ?: throw Exception("Message missing")
            val changes = protoBuf.getByteArrayList(CHANGES_TAG)?.map { HashValue(it) }?.toMutableList()
                ?: ArrayList()
            return BranchLogEntry(time, HashValue(entryId), Message(message), changes)
        }
    }

    fun toProtoBuffer(): ProtocolBufferLight {
        val buffer = ProtocolBufferLight()
        buffer.put(TIME_TAG, time)
        buffer.put(ENTRY_ID_TAG, entryId.bytes)
        buffer.put(MESSAGE_TAG, message.data)
        buffer.put(CHANGES_TAG, changes.map { it.bytes })
        return buffer
    }
}

@Serializable
class BranchLogList(val entries: MutableList<BranchLogEntry> = ArrayList()) {
    fun add(entry: BranchLogEntry) {
        entries.add(entry)
    }

    fun add(time: Long, id: HashValue, message: ByteArray, changes: MutableList<HashValue>) {
        entries.add(BranchLogEntry(time, id, BranchLogEntry.Message(message), changes))
    }
}


interface BranchLog {
    fun getBranchName(): String
    suspend fun add(id: HashValue, message: ByteArray, changes: List<HashValue>)
    suspend fun add(entry: BranchLogEntry)
    /**
     * Only add the entry if the last entry has the expected entry id
     */
    suspend fun add(entry: BranchLogEntry, expectedEntryId: HashValue?): Boolean
    suspend fun getEntries(): List<BranchLogEntry>
    suspend fun getHead(): BranchLogEntry?
}