package org.fejoa.repository.sync

import kotlinx.serialization.SerialId
import kotlinx.serialization.Serializable
import kotlinx.serialization.protobuf.ProtoBuf
import org.fejoa.network.RemotePipe
import org.fejoa.protocolbufferlight.ProtocolBufferLight
import org.fejoa.repository.BranchLogEntry


//TODO wrap the request into a transaction?
object GetLogEntryProtocol {
    @Serializable
    class RemoteTipRequest(@SerialId(0) val branch: String)

    class RemoteTipReply(val logTip: BranchLogEntry? = null) {
        companion object {
            const val LOG_HEAD_TAG = 0

            fun read(data: ByteArray): RemoteTipReply {
                val buffer = ProtocolBufferLight(data)
                val head = buffer.getBytes(LOG_HEAD_TAG)?.let { BranchLogEntry.read(it) }
                return RemoteTipReply(head)
            }
        }

        fun toProtoBuffer(): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            if (logTip != null)
                buffer.put(LOG_HEAD_TAG, logTip.toProtoBuffer().toByteArray())
            return buffer
        }

        fun asBlock(): SyncProtocol.ProtocolBlock {
            return SyncProtocol.ProtocolBlock(SyncProtocol.BlockType.GET_LOG_TIP, toProtoBuffer().toByteArray())
        }
    }

    suspend fun getRemoteTip(remotePipe: RemotePipe, branch: String): RemoteTipReply {
        val outputStream = remotePipe.outStream

        val request = SyncProtocol.Sender(outputStream, SyncProtocol.CommandType.GET_REMOTE_TIP)
        request.write(SyncProtocol.ProtocolBlock(SyncProtocol.BlockType.GET_LOG_TIP.value,
                ProtoBuf.dump(RemoteTipRequest(branch))))

        // parse reply
        val inputStream = remotePipe.inStream
        val response = request.submitWithErrorCheck(inputStream)
        val branchLogBlock = response.next()
        if (branchLogBlock.type != SyncProtocol.BlockType.GET_LOG_TIP.value)
            throw Exception("Unexpected block type ${branchLogBlock.type}")
        return RemoteTipReply.read(branchLogBlock.data)
    }

    suspend fun handle(request: SyncProtocol.Receiver, remotePipe: RemotePipe,
                       logGetter: RequestHandler.BranchLogGetter) {
        val requestBlock = request.next()
        if (requestBlock.type != SyncProtocol.BlockType.GET_LOG_TIP.value)
            throw Exception("Unexpected type")

        val branchLogRequest = ProtoBuf.load<RemoteTipRequest>(requestBlock.data)
        val branch = branchLogRequest.branch
        val sender = request.reply(remotePipe.outStream)
        val localBranchLog = logGetter[branch]

        val head = localBranchLog?.getHead()
        sender.write(RemoteTipReply(head).asBlock())
        return sender.submit()
    }
}
