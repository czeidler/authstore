package org.fejoa.repository.sync

import org.fejoa.network.RemotePipe
import org.fejoa.protocolbufferlight.ProtocolBufferLight

import org.fejoa.storage.*
import org.fejoa.support.await


object HasChunkProtocol {
    class HasChunksRequest(val chunks: List<HashValue>) {
        companion object {
            const val CHUNKS_TAG = 0

            fun read(data: ByteArray): HasChunksRequest {
                val buffer = ProtocolBufferLight(data)
                val chunks: List<HashValue> = buffer.getByteArrayList(CHUNKS_TAG)?.map {
                    HashValue(it)
                } ?: emptyList()
                return HasChunksRequest(chunks)
            }
        }

        fun toProtoBuffer(): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            val rawChunks = chunks.map { it.bytes }
            buffer.put(CHUNKS_TAG, rawChunks)
            return buffer
        }

        fun asBlock(): SyncProtocol.ProtocolBlock {
            return SyncProtocol.ProtocolBlock(SyncProtocol.BlockType.HAS_CHUNKS, toProtoBuffer().toByteArray())
        }
    }

    class HasChunksReply(val hasChunks: List<HashValue>){
        companion object {
            const val HAS_CHUNKS_TAG = 0

            fun read(data: ByteArray): HasChunksReply {
                val buffer = ProtocolBufferLight(data)
                val chunks: List<HashValue> = buffer.getByteArrayList(HAS_CHUNKS_TAG)?.map {
                    HashValue(it)
                } ?: emptyList()
                return HasChunksReply(chunks)
            }
        }

        fun toProtoBuffer(): ProtocolBufferLight {
            val buffer = ProtocolBufferLight()
            val rawChunks = hasChunks.map { it.bytes }
            buffer.put(HAS_CHUNKS_TAG, rawChunks)
            return buffer
        }

        fun asBlock(): SyncProtocol.ProtocolBlock {
            return SyncProtocol.ProtocolBlock(SyncProtocol.BlockType.HAS_CHUNKS, toProtoBuffer().toByteArray())
        }
    }

    suspend fun hasChunks(remotePipe: RemotePipe, branch: String, chunks: List<HashValue>, maxChunksPerRequest: Int)
            : List<HashValue> {
        var transactionId: Long = -1
        val result: MutableList<HashValue> = ArrayList()
        val windowedRequest = chunks.windowed(maxChunksPerRequest, maxChunksPerRequest, true)
        windowedRequest.forEachIndexed { index, list ->
            val finishTransaction = index == windowedRequest.lastIndex
            val transId = hasChunks(remotePipe, branch, transactionId, finishTransaction, list, result)
            if (transactionId >= 0 && transId != transactionId)
                throw Exception("Unexpected transaction id $transId but $transactionId expected")
            transactionId = transId
        }
        return result
    }

    private suspend fun hasChunks(remotePipe: RemotePipe, branch: String, remoteTransactionId: Long,
                                  finishTransaction: Boolean, chunks: List<HashValue>,
                                  resultHasChunks: MutableList<HashValue>): Long {
        val outputStream = remotePipe.outStream
        val request = SyncProtocol.Sender(outputStream, SyncProtocol.CommandType.HAS_CHUNKS)

        TransactionProtocol.send(request, remoteTransactionId, branch, AccessRight.PULL, finishTransaction) {
            // get chunks
            request.write(HasChunksRequest(chunks).asBlock())
        }

        // receive reply
        val reply = request.submitWithErrorCheck(remotePipe.inStream)
        return TransactionProtocol.receiveReply(reply, remoteTransactionId) {
            while (reply.hasNext()) {
                val block = reply.next()
                when (block.type) {
                    SyncProtocol.BlockType.HAS_CHUNKS.value -> {
                        val hasChunksBlock = HasChunksReply.read(block.data)
                        resultHasChunks += hasChunksBlock.hasChunks
                    }
                }
            }
        }
    }

    suspend fun handle(branch: String, request: SyncProtocol.Receiver, remotePipe: RemotePipe,
                                transactionManager: TransactionManager,
                                branchBackend: StorageBackend.BranchBackend) {
        val hasChunks: MutableList<HashValue> = ArrayList()
        TransactionProtocol.Handler(request, branch, transactionManager, branchBackend).receive {
            // read requested chunks
            while (request.hasNext()) {
                val block = request.next()
                when (block.type) {
                    SyncProtocol.BlockType.HAS_CHUNKS.value -> {
                        val hasChunksBlock = HasChunksRequest.read(block.data)
                        hasChunks.addAll(hasChunksBlock.chunks)
                    }
                    else -> {
                    }
                }
            }
        }.reply(remotePipe.outStream) { reply, transaction ->
            val existingChunks: MutableList<HashValue> = ArrayList()
            hasChunks.forEachIndexed { index, hashValue ->
                if (transaction.chunkTransaction.hasChunk(hashValue).await())
                    existingChunks.add(hashValue)
            }
            reply.write(HasChunksReply(existingChunks).asBlock())
            reply.submit()
        }
    }
}
