package org.fejoa.repository.sync

import org.fejoa.storage.Hash
import org.fejoa.network.RemotePipe
import org.fejoa.repository.*
import org.fejoa.storage.*
import org.fejoa.support.*


class PullRequest(private val requestRepo: Repository, private val commitSignature: CommitSignature?) {
    /**
     * @returns the remote tip
     */
    suspend fun pull(remotePipe: RemotePipe, branch: String, mergeStrategy: MergeStrategy): Hash? {
        val remoteLogEntry = GetLogEntryProtocol.getRemoteTip(remotePipe, branch).logTip ?: return null
        val remoteTipMessage = remoteLogEntry.message.data
        val remoteRepoRef = requestRepo.branchLogIO.readFromLog(remoteTipMessage)
        if (remoteRepoRef.head.value.isZero) {
            val branchLog = requestRepo.branchBackend.getBranchLog()
            val localHead = branchLog.getHead()
            if (localHead == null) {
                // add the remote log entry to init the repo
                branchLog.add(remoteLogEntry)
                requestRepo.reset(remoteRepoRef.config)
            }

            return remoteRepoRef.head
        }
        val transaction = requestRepo.getCurrentTransaction()

        // up to date?
        val localTip = requestRepo.getHead()
        if (localTip == remoteRepoRef.head)
            return remoteRepoRef.head

        val chunkFetcher = createRemotePipeFetcher(transaction, branch, remotePipe)
        chunkFetcher.enqueueRepositoryJob(remoteRepoRef)
        chunkFetcher.fetch()

        val remoteRep = Repository.open(branch, remoteRepoRef, requestRepo.branchBackend, requestRepo.crypto)
        val merged = requestRepo.merge(listOf(remoteRep), mergeStrategy)
        val newHead = when (merged) {
            Database.MergeResult.MERGED -> {
                requestRepo.commit("Merge after pull".toUTF(), commitSignature)
            }
            Database.MergeResult.FAST_FORWARD -> {
                requestRepo.getHeadCommit()!!.getHash()
            }
        }

        return remoteRepoRef.head
    }

    companion object {
        fun createRemotePipeFetcher(transaction: ChunkAccessors.Transaction, branch: String,
                                    remotePipe: RemotePipe): ChunkFetcher {
            return ChunkFetcher(transaction, object : ChunkFetcher.FetcherBackend {
                override suspend fun fetch(transaction: ChunkTransaction, requestedChunks: List<HashValue>) {
                    GetProtocol.getChunks(remotePipe, branch, requestedChunks, 100) {
                        it.forEach {
                            if (it.missingChunks.isNotEmpty())
                                throw Exception("Unexpected missing chunks")
                            it.chunks.forEach {
                                val result = transaction.putChunk(it.chunk).await()
                                if (result.key != it.id)
                                    throw IOException("Hash miss match. Expected:" + it.id + ", got: " + result.key)
                            }
                        }
                    }
                }
            })
        }
    }
}
