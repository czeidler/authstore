package org.fejoa.repository

import org.fejoa.storage.Hash
import org.fejoa.storage.HashValue


class CommitCache(private val repository: Repository) {
    private val commitCache = HashMap<HashValue, Commit>()
    private val queue: MutableList<HashValue> = ArrayList()
    private val MAX_ENTRIES = 50

    /**
     * Get a commit that belongs to the repository. The search is started from the head.
     */
    suspend fun getCommit(hashValue: Hash): Commit? {
        val commit = commitCache[hashValue.value]
        if (commit != null) {
            queue.remove(hashValue.value)
            queue.add(hashValue.value)
            return commit
        }
        return loadCommit(hashValue)
    }

    private suspend fun loadCommit(hashValue: Hash): Commit {
        val commit = Commit.read(hashValue, repository.objectIndex)
        commitCache[hashValue.value] = commit
        queue += hashValue.value
        while (queue.size > MAX_ENTRIES) {
            val old = queue.removeAt(0)
            commitCache.remove(old)
        }

        return commit
    }

    suspend fun isAncestor(baseCommit: Hash, ancestorCandidate: Hash): Boolean {
        val commitBox = getCommit(baseCommit) ?: throw Exception("Can't load commit: $baseCommit")
        for (parent in commitBox.parents) {
            val parentCommit = loadCommit(parent)
            val parentHash = parentCommit.getHash()
            if (parentHash == ancestorCandidate)
                return true
            if (isAncestor(parentHash, ancestorCandidate))
                return true
        }
        return false
    }
}
