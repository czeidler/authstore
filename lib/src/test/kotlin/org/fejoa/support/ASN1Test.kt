package org.fejoa.support

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue


class ASN1Test {
    fun testASN1Length(number: Int, expected: ByteArray) {
        val result = (ASN1Length.writeLength(ByteArrayOutStream(), number) as ByteArrayOutStream).toByteArray()
        assertTrue(expected contentEquals result)
        val parsed = ASN1Length.readLength(ByteArrayInStream(result))
        assertEquals(number, parsed)
    }

    @Test
    fun testASN1Length() {
        testASN1Length(9, byteArrayOf(0x09))
        testASN1Length(127, byteArrayOf(0x7F))
        testASN1Length(128, byteArrayOf(0x81.toByte(), 0x80.toByte()))
        testASN1Length(129, byteArrayOf(0x81.toByte(), 0x81.toByte()))
        testASN1Length(289, byteArrayOf(0x82.toByte(), 0x01, 0x21))
        testASN1Length(495, byteArrayOf(0x82.toByte(), 0x01.toByte(), 0xEF.toByte()))
    }

    @Test
    fun testSequence() {
        val test1 = "MEYCIQCZCuoker5o+zQRrRoV9oJc1Qyz+VQBrTBJoRQ7mnhgGAIhAO65/sWXzs4bX9E9FcQ8IxxCpyFUScn70+tDkTSbar+d"
                .decodeBase64()
        val seq1 = ASN1Entry.parse(test1) ?: throw Exception("Can't parse input")
        assertTrue(test1 contentEquals seq1.toByteArray())

        val ecKeyPair = "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgEk2JvRBASk2+ZGUKEujsy4szBcKPyA4XVdHg7SvIfeuhRANCAARBDN585mK86lDXS+Kx9HWD4EAhJOyFi/ck6iyhdLNOma9AMiV9ihFsikuoL8cFa3S0rdqaA+nmZbcoz6ju7WrW"
                .decodeBase64()
        val seqEC = ASN1Entry.parse(ecKeyPair) ?: throw Exception("Can't parse input")
        assertEquals(3, seqEC.children.size)
        val seqEcExt = ASN1Entry.parse(seqEC.children[2].byteArray) ?: throw Exception("Can't parse input")
        assertEquals(3, seqEcExt.children.size)
        val tag1 = seqEcExt.getContextSpecific(1)
        assertNotNull(tag1)
        assertTrue(ecKeyPair contentEquals  seqEC.toByteArray())

    }
}