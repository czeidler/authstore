package org.fejoa.support

import kotlin.math.max
import kotlin.test.Test
import kotlin.test.assertEquals


class CyclicPolyFingerprintTest {
    @Test
    fun testCyclicPolyFingerprint() {
        val string = "abcdefghaijklmnopqrstuvwxyz"
        val table = CYCLIC_POLY_TABLES.table1
        val fingerprinter = CyclicPolyFingerprint.create(table, 16)

        for (i in 0 until string.length) {
            fingerprinter.write(string[i].toByte())

            val startIndex = max(0, i - (16 - 1))
            var manualFP = 0L
            for (a in startIndex .. i) {
                val charByte = string[a].toByte()
                manualFP = CyclicPolyFingerprint.circularShr(manualFP, 1)
                manualFP = manualFP xor table[charByte.toInt() and 0xFF]
            }

            assertEquals(manualFP, fingerprinter.fingerprint)
        }

    }
}
