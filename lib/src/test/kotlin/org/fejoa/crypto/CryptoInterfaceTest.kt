package org.fejoa.crypto

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON
import org.fejoa.support.decodeBase64
import org.fejoa.support.encodeBase64
import org.fejoa.support.toUTF
import org.fejoa.support.toUTFString
import org.fejoa.test.testAsync
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.Test


class CryptoInterfaceTest {
    @Test
    fun testCrypto() = testAsync {
        val settings = CryptoSettings.default
        CryptoSettings.setDefaultEC(settings)
        doTest(settings)

        CryptoSettings.setDefaultRSA(settings)
        doTest(settings)
    }

    private suspend fun testAsymEnc(crypto: CryptoInterface, plain: String,
                                    expectedEnc: ByteArray, publicKey: PublicKey, privateKey: PrivateKey,
                                    algo: CryptoSettings.ASYM_ALGO) {
        // encryption/decrytion
        val encryptedAsym = crypto.encryptAsymmetric(plain.toUTF(), publicKey, algo)
        assertTrue(plain.toUTF() contentEquals crypto.decryptAsymmetric(encryptedAsym, privateKey, algo))
        // decryption the given encrypted value
        val decryptedAsym = crypto.decryptAsymmetric(expectedEnc, privateKey, algo)
        assertTrue(plain.toUTF() contentEquals decryptedAsym)
    }

    private suspend fun testAsymSign(crypto: CryptoInterface, plain: String, expectedSignature: ByteArray,
                                     publicKey: PublicKey, privateKey: PrivateKey,
                                     algo: CryptoSettings.ASYM_ALGO) {
        // signing/verifying
        val signature = crypto.sign(plain.toUTF(), privateKey, algo)
        assertTrue(crypto.verifySignature(plain.toUTF(), signature, publicKey, algo))
        // verifying the given expected signature
        assertTrue(crypto.verifySignature(plain.toUTF(), expectedSignature, publicKey, algo))
    }

    private suspend fun testSym(crypto: CryptoInterface, plain: String, expectedEnc: ByteArray, iv: ByteArray,
                                key: SecretKey, settings: CryptoSettings.Symmetric) {
        // encryption
        val encrypted = crypto.encryptSymmetric(plain.toUTF(), key, iv, settings.algo)
        assertTrue(expectedEnc contentEquals encrypted)

        // decryption
        val decrypted = crypto.decryptSymmetric(expectedEnc, key, iv, settings.algo)
        assertTrue(plain.toUTF() contentEquals decrypted)
    }

    private suspend fun doTest(settings: CryptoSettings) {
        val crypto = CryptoHelper.crypto

        // symmetric encryption
        val clearTextSym = "hello crypto symmetric"
        val iv = crypto.generateBits(settings.symmetric.algo.ivSize)
        var secretKey = crypto.generateSymmetricKey(settings.symmetric)
        val encryptedSymmetric = crypto.encryptSymmetric(clearTextSym.toUTF(), secretKey, iv,
                settings.symmetric.algo)
        testSym(crypto, clearTextSym, encryptedSymmetric, iv, secretKey, settings.symmetric)

        // encode/ decode secret key
        val encodedSecretKey = crypto.encode(secretKey)
        val decodedSecretKey = crypto.secretKeyFromRaw(encodedSecretKey, settings.symmetric.algo)
        testSym(crypto, clearTextSym, encryptedSymmetric, iv, decodedSecretKey, settings.symmetric)

        // KDF
        // test if kdf gives the same value twice
        val password = "testPassword348#"
        val salt = crypto.generateSalt()
        val kdfKey1 = crypto.deriveKey(password, salt, settings.kdf)
        assertEquals(32, crypto.encode(kdfKey1).size)
        val kdfKey2 = crypto.deriveKey(password, salt, settings.kdf)
        assertTrue(crypto.encode(kdfKey1) contentEquals
                crypto.encode(kdfKey2))
        val kdfEncryptedPassword = crypto.encryptSymmetric(password.toUTF(), kdfKey1, iv,
                settings.symmetric.algo)
        testSym(crypto, password, kdfEncryptedPassword, iv, kdfKey1, settings.symmetric)

        // asymmetric encryption + signature
        val encKeyPair = crypto.generateKeyPair(settings.asymmetric)
        val clearTextAsym = "hello crypto asymmetric"
        val encryptedAsymmetric = crypto.encryptAsymmetric(clearTextAsym.toUTF(), encKeyPair.publicKey,
                settings.asymmetric.algo)
        testAsymEnc(crypto, clearTextAsym, encryptedAsymmetric, encKeyPair.publicKey, encKeyPair.privateKey,
                settings.asymmetric.algo)

        val signKeyPair = crypto.generateKeyPair(settings.signature)
        val signature = crypto.sign(clearTextAsym.toUTF(), signKeyPair.privateKey, settings.signature.algo)
        testAsymSign(crypto, clearTextAsym, signature, signKeyPair.publicKey, signKeyPair.privateKey, settings.signature.algo)

        // test encoding/ decoding public/private keys
        // encode/decode encryption
        val encodedPrivateEnc = crypto.encode(encKeyPair.privateKey)
        val decodedPrivateEnc = crypto.privateKeyFromRaw(encodedPrivateEnc, encKeyPair.privateKey.algo)
        val encodedPublicEnc = crypto.encode(encKeyPair.publicKey)
        val decodedPublicEnc = crypto.publicKeyFromRaw(encodedPublicEnc, encKeyPair.publicKey.algo)
        testAsymEnc(crypto, clearTextAsym, encryptedAsymmetric, decodedPublicEnc, decodedPrivateEnc,
                settings.asymmetric.algo)

        // encode/decode signing
        val encodedPrivateSign = crypto.encode(signKeyPair.privateKey)
        val decodedPrivateSign = crypto.privateKeyFromRaw(encodedPrivateSign, signKeyPair.privateKey.algo)
        val encodedPublicSign = crypto.encode(signKeyPair.publicKey)
        val decodedPublicSign = crypto.publicKeyFromRaw(encodedPublicSign, signKeyPair.publicKey.algo)
        testAsymSign(crypto, clearTextAsym, signature, decodedPublicSign, decodedPrivateSign,
                settings.signature.algo)
    }

    @Test
    fun testConvergentEncryption() = testAsync {
        val settings = CryptoSettings.default

        val cryptoInterface = CryptoHelper.crypto

        // encrypt symmetric
        val clearTextSym = "hello crypto symmetric"
        val iv = cryptoInterface.generateBits(settings.symmetric.algo.ivSize)
        val secretKey = cryptoInterface.generateSymmetricKey(settings.symmetric)
        val encryptedSymmetric = cryptoInterface.encryptSymmetric(clearTextSym.toUTF(), secretKey, iv,
                settings.symmetric.algo)
        val encryptedSymmetric2 = cryptoInterface.encryptSymmetric(clearTextSym.toUTF(), secretKey, iv,
                settings.symmetric.algo)

        assertTrue(encryptedSymmetric contentEquals encryptedSymmetric2)
    }


    // all byte arrays are base64 encoded
    @Serializable
    class TestDataSymEnc(val plain: String,
                         val iv: String,
                         val algo: CryptoSettings.SYM_ALGO,
                         val key: String,
                         val encrypted: String)

    @Serializable
    class TestDataAsymEnc(val plain: String,
                          val algo: CryptoSettings.ASYM_ALGO,
                          val publicKey: String,
                          val privateKey: String,
                          val encrypted: String)

    @Serializable
    class TestDataSign(val message: String,
                       val algo: CryptoSettings.ASYM_ALGO,
                       val publicKey: String,
                       val privateKey: String,
                       val signature: String)

    @Serializable
    class TestData(val symAES_CTR: TestDataSymEnc,
                   val asymEncRSA_OAEP: TestDataAsymEnc,
                   val signRSASSA_PKCS1_v1_5_SHA256: TestDataSign,
                   val signECDSA_SECP256R1_SHA256: TestDataSign
    )

    private suspend fun testTestData(data: TestData) {
        testTestData(data.symAES_CTR)
        testTestData(data.asymEncRSA_OAEP)
        testTestData(data.signRSASSA_PKCS1_v1_5_SHA256)
        testTestData(data.signECDSA_SECP256R1_SHA256)
    }

    private suspend fun testTestData(data: TestDataSymEnc) {
        val crypto = CryptoHelper.crypto

        val symKey = crypto.secretKeyFromRaw(data.key.decodeBase64(), data.algo)
        val plain = crypto.decryptSymmetric(data.encrypted.decodeBase64(), symKey, data.iv.decodeBase64(),
                data.algo)
        assertEquals(data.plain, plain.toUTFString())
    }

    private suspend fun testTestData(data: TestDataAsymEnc) {
        val crypto = CryptoHelper.crypto

        val privateKey = crypto.privateKeyFromRaw(data.privateKey.decodeBase64(), data.algo)
        val publicKey = crypto.publicKeyFromRaw(data.publicKey.decodeBase64(), data.algo)

        //  test the key
        val testData = "Enc test data for key testing"
        val encrypted = crypto.encryptAsymmetric(testData.toUTF(), publicKey, data.algo)
        val plainTestData = crypto.decryptAsymmetric(encrypted, privateKey, data.algo)
        assertEquals(testData, plainTestData.toUTFString())

        val plain = crypto.decryptAsymmetric(data.encrypted.decodeBase64(), privateKey, data.algo)
        assertEquals(data.plain, plain.toUTFString())
    }

    private suspend fun testTestData(data: TestDataSign) {
        val crypto = CryptoHelper.crypto

        val privateKey = crypto.privateKeyFromRaw(data.privateKey.decodeBase64(), data.algo)
        val publicKey = crypto.publicKeyFromRaw(data.publicKey.decodeBase64(), data.algo)

        // test the key
        val testData = "Sign test data for key testing"
        val signature = crypto.sign(testData.toUTF(), privateKey, data.algo)
        assertTrue(crypto.verifySignature(testData.toUTF(), signature, publicKey, data.algo))

        // verify the data signature
        assertTrue(crypto.verifySignature(data.message.toUTF(), data.signature.decodeBase64(), publicKey, data.algo))
    }

    // data produced on JVM
    val jvmTestData = "{\"symAES_CTR\":{\"plain\":\"hello crypto symmetric\",\"iv\":\"H\\/TkfHL42fgUKWXl1gqZ5g==\",\"algo\":\"AES_CTR\",\"key\":\"HhGIDwfxWvfsZJbJWJB9WNheN2AlTNjaqVGDemnjPs8=\",\"encrypted\":\"UA0LeZ2lkFaEa6D5QdFlvenIVlU1ZA==\"},\"asymEncRSA_OAEP\":{\"plain\":\"hello crypto asymmetric\",\"algo\":\"RSA_OAEP_SHA256\",\"publicKey\":\"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAr8\\/8WL4Myj7KcrQOZTOrMaVZqlt\\/+hKgx7CEbOdDYAJJGl\\/ce04dZjQmKwX63EM+kG8ZlPkzBEhasOYCS1waQNoU6riAztVe9MOTwqnRK+VtpO1zqL8TYMWeXJ0VEV3YOYCtcGQGYM3j8onyllieBtIXiCHn2E34J3LziKSpHPlwABf3QL0k44gns2PQpnwrSqUZ\\/JLyjdPJ2e5J6m7LfksCXEORcFfsub2nSR2eiZ2UYT3MeUzBxoWHxPJMl8Hd3RAoRamlLOfU\\/\\/r5lnwuvjj1PFnVDY1iwWwCMJWKMGTt\\/+d3EIhL2CJmdFbABAMw+eoc2LiDXqDLyAFOLlPCgwIDAQAB\",\"privateKey\":\"MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCvz\\/xYvgzKPspytA5lM6sxpVmqW3\\/6EqDHsIRs50NgAkkaX9x7Th1mNCYrBfrcQz6QbxmU+TMESFqw5gJLXBpA2hTquIDO1V70w5PCqdEr5W2k7XOovxNgxZ5cnRURXdg5gK1wZAZgzePyifKWWJ4G0heIIefYTfgncvOIpKkc+XAAF\\/dAvSTjiCezY9CmfCtKpRn8kvKN08nZ7knqbst+SwJcQ5FwV+y5vadJHZ6JnZRhPcx5TMHGhYfE8kyXwd3dEChFqaUs59T\\/+vmWfC6+OPU8WdUNjWLBbAIwlYowZO3\\/53cQiEvYImZ0VsAEAzD56hzYuINeoMvIAU4uU8KDAgMBAAECggEBAICsSzcz2S6IKCktCMAewbOzHd3l29NNoOilkzYVSCVTZudrs3V3ZWLw1eJ1HPXqLTcOaP20KReAUQfZP2KiRQCj290yLeuzFC01msZE\\/TcQUl\\/7\\/cNKHUedOtrziIDOWzsypz1zp4tgBO308lwA\\/Z9mUhyrHAEeh3lv0zd1s+Pkm7XaOekW70C4AtvgUV7kzoJsN2rOWp+\\/+qQfx\\/2mxyxuJh2XFzLNK2Om\\/V+th5pYTMpOgI9wVg2Xf3n8oZmbt39xbvcFjGGLH\\/QRdNsk4UQlO9gRDuO5KF6fezneUUhjTOjLOM46xWC1bUMGLQTxjsy\\/3Vt0NyUbgxgmw\\/qAmekCgYEA\\/xBV\\/28HMQS2eu3S7LqAsh9DdxQPYyhQP8XocTKPEkIgetfwtxHUwKFjGJCBbe80vBIKBBesHGEeOtrsP0nC8u\\/58ngNoeNx8xjsOfM7lQRTSFSCkMMso1LE3ydAG\\/mSa0xK5gJmDpnFpwTLDD5bg8u+xHc4jAvmoAjIKLuF4v8CgYEAsHUu7cMdQLZXEgZb4+5tAoxkavwU1k0PjzOO6UWjHs0iVXSCIvjXdOluotFLmKRxrWZzHW4rqYLxLpSCklbM7bzsi607J\\/QE\\/J6j1A9gEQuHAPcfM72wPA1h+7W9O8CUDCPWMzrO\\/yMYb9LBzjIwkScnFImmFNS49q5Egi\\/IFH0CgYEA0boc\\/GvO\\/f9iDLKBPSYdrFkIoGFeRapgUg\\/pkSqBegHlsxzcUavXxIzvbsNjqYGHWx16HuNYIf25XaullmSQaDHq0\\/TjQLpEeB+GHiBiXaWw1UioE2Bo0+K6\\/sLHdDlCq\\/ePnpFaWacXXr4SN38OwNBHjER7uqwOdojCDLwk2wECgYA8PTTxHCvPXLAqyhrynAGA3jalwzrF0fbe1yYRgqFmMXdoHucSU0sScWJAmRyqlnDsVk40HNkw\\/C+jzDUrChDZlCZYpKHOSOKu7uVUzo7oIrxK2xI+0qHoTlVF3cwlKuAfqNhGEux5cxyziAp7fuzPkMMporgc4mXmaKWgn\\/k47QKBgAO6TyMVpNDMKkQ5uVcTKJLVQw81+Po4DBEHg6IxBBnFK34Zz0I\\/EBupIkSsLE6pyhLm7DrrmOrEy4O8EEKMWanrVTlGKhsjQYVjR4thtiouBdJX7OyiGn5BdiJHDYrNuVNnjebuw2JO+bQCRaxpsKkOWm036P2fEtOaIjnEL7SW\",\"encrypted\":\"rXOCso6Ry6SKt6fba871\\/Kxq3bnbFRuw2hhrxnvrNWcL\\/r1L7vqihVikeqZBbMMzf+r1FKYIXJNI7ik5DzS2VwqZyxf2FAuQyiTMlaIS4xU4t\\/vI2OCopOQzRVUd9Oa1nVOtUtMq7+u70Kjrotpaa8o4f0UTN4IRPVMF\\/R8ZlLgSBMT6vV2qQeuEt9uFSrox9Bu4ijKiHZrjbxmk4Nxh3J9SvHPIZiwnWFpeHAugGoGvjs7\\/weo5XRjvKwOOmFPlJW+ZD6fEhgMIg81noMTGJT\\/82TUw\\/yNdghUOe+0J1a3WqzcrirczGlhrw\\/WLa0KSYmhSYPNXYSF1vUJKB0Dtaw==\"},\"signRSASSA_PKCS1_v1_5_SHA256\":{\"message\":\"Sign me message\",\"algo\":\"RSASSA_PKCS1_v1_5_SHA256\",\"publicKey\":\"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjPtgb0f9ad5sONHvuGWlwaoBJ6q9TfcG7QE+pkyZZ0142yrCDRe9fzQ9vapUYdrds2POpWJTlaNES1NPhyGs+gDHRZnge4xbAW1qiMuVxZXp4wBefWZU72A\\/HkU6xW3lU95yhpjcGnaHPd2bepBn9\\/KtjU6bdhTykdbXCANa0U3TcBcQJ7dJglISrfNfKC89Vook4kaTmBqm7cFhBslSvJV9UIZGSc0T7vm\\/1BmgBsT6Dsi7EbZ1ukdvqERDHEkwzxGGpdJb69V4zmZVTMPUpV3T0Oy\\/ySLQbWkml52Oo\\/T8xKOa0faDhgTbj+Fe1htm1hw4cYkzlEs6fdfJqIhNwQIDAQAB\",\"privateKey\":\"MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCM+2BvR\\/1p3mw40e+4ZaXBqgEnqr1N9wbtAT6mTJlnTXjbKsINF71\\/ND29qlRh2t2zY86lYlOVo0RLU0+HIaz6AMdFmeB7jFsBbWqIy5XFlenjAF59ZlTvYD8eRTrFbeVT3nKGmNwadoc93Zt6kGf38q2NTpt2FPKR1tcIA1rRTdNwFxAnt0mCUhKt818oLz1WiiTiRpOYGqbtwWEGyVK8lX1QhkZJzRPu+b\\/UGaAGxPoOyLsRtnW6R2+oREMcSTDPEYal0lvr1XjOZlVMw9SlXdPQ7L\\/JItBtaSaXnY6j9PzEo5rR9oOGBNuP4V7WG2bWHDhxiTOUSzp918moiE3BAgMBAAECggEAHdUjtuiY60oxxGRIQdDgnnaHAAnVPSZExOo6g5455+4w4M\\/PeE3Jv50iY3wjlwFlYIyCMeT4ovWFghsC\\/SlXHSAFCulg40P0Df1FSeUqsEDk2TA+znJVDsJjGNi1cwmqOXEc8vZtvPvP6nt2Zx0fowwrRXeY1gfgvJOltQBgu2FVIRaHdWK9sLDJTIYWkifqwIMkOroUY9YWwX8NnP1nSLjWHAqOviSbiuq1ga\\/xTVuJV3HICM1JVotL1dTPJNBZUjKjpSjUPRKm7XwfEdwN3v9JDCU7kpqXb\\/91Q0VZcLBsMPTlTWTCOF1dGLRWXyCQI2AeFyNvMK+iC0YZuRFOsQKBgQD6LTNXN2GFm8NKH1h+o+UcWUzZiiWtMNbkCazM8bL\\/zrCAMjMG921dxLtoGYjbs0RGCcDxMmXvKNTARyDwL5wq7YxmhAH4yqz3g4wtT7y3lixKMum2alzLgSUcTZ5hh7TqUw7WlloDgZqTqpp+37p6ZQCtlJiNmKD0ZHtP2OZhLwKBgQCQQ3yPJcD\\/IUm1ChNAWve1LSBDCi+JVK9x0sTGXbBrxhfPw9wq2vz1KJPXKuv6Dpht9xdN7DZF9kzozi+1\\/I+Fk1NACMYZCKoEzBaL2ybeiQGwZKuwsUkepRr2YD96Fjwkb1iLxw4lP5FjeSZnGQde0dT\\/w5XIiZsDOJetJQIkDwKBgCEegsF1D3AEouHZLHn3cIlEOSxDYaF\\/6bLepQWsqam7zxnpi1\\/MuanENZseZkPPg6ZR\\/DzyExSKL0AVKoMhimcUc5XOvAQXsLZDr\\/b\\/q1gShWJ9QvtnMUmvmx4YGHOI56ZtwHUgCmKUccZCu5k4PROe82wHU\\/rKzeQMuVZkvHr5AoGAQug5zb4py2np4GAC27cU6V87djw0HdI76GNjRcjP+LwP8RkadHnKFf2yy3kppni8c0vE8CvcZAg+VT7DW\\/j9q2rPkoQ\\/M\\/8Ts1PgJQ8H8O\\/D68ZU5obirabPYXL0HbBQRmmpvHIeemIIEQVwLSKri2twCQrx3ecMEKtflTrzyjsCgYBQnK4j2trSEVHEvsnYf+CxFTv8S6RoA1bUk9JzJnufKHIr\\/+KggiDmjC\\/eQiL0utvbSCUgG\\/wuTYBzsjtgxCMxqt0zCHxP1gZwCJ7EjpKw2uqFd7B3gun416RZ5qfJgqx9Jt3XH7adj5P1JAvftALjF4CYJJQm513c28EQ+p1O6g==\",\"signature\":\"GN2ei7QDkj8JCVa2GuPz9BkUn4Mltcn5x0qAyO2EqBGGl7JyIWdYgVGODtKdc8e5m50IhBVzhlooxagVoeqzJk6cJ24NBGrFEZHYjK47Od8Ty5rEsV+E8eOoO2Y4Oj45+qakB1AkYX5KzQGchhRJ3BvLYiI0lEEZgoNoRn7T7CjJNd5SQiMSQUQ\\/58BCM4hxNJ1NUjkv83SmO5PlEbse65GQraak+Ik3CGtYlWsA9XHlfIB1wuVz6Gmd+7sfJXHJNL6JSEjugZAYgCNqnV+28vXrmOt9N\\/ScXbp9lnZ5d4oTB1+3vjkaN8ADGaByiDMO809eeyM2MKwyNAIwYQblwg==\"},\"signECDSA_SECP256R1_SHA256\":{\"message\":\"Sign me message\",\"algo\":\"ECDSA_SECP256R1_SHA256\",\"publicKey\":\"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEX53hhskS3XaNqUH6WJMA\\/HVFdjBV8jTT9MSsO9xGmIuZszJht0djkwb66hYYwLBBzu6t1EwwRIPYtHnIL\\/LdSA==\",\"privateKey\":\"MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgxApd+hORYjJL6i4son9hH1FPspqBr8h9JmXD3nk6RPOgCgYIKoZIzj0DAQehRANCAARfneGGyRLddo2pQfpYkwD8dUV2MFXyNNP0xKw73EaYi5mzMmG3R2OTBvrqFhjAsEHO7q3UTDBEg9i0ecgv8t1I\",\"signature\":\"MEYCIQCZCuoker5o+zQRrRoV9oJc1Qyz+VQBrTBJoRQ7mnhgGAIhAO65\\/sWXzs4bX9E9FcQ8IxxCpyFUScn70+tDkTSbar+d\"}}"

    // data produced on chrome
    val chromeTestData = "{\"symAES_CTR\":{\"plain\":\"hello crypto symmetric\",\"iv\":\"vyEnir6P0dcsnvNqPDp4fA==\",\"algo\":\"AES_CTR\",\"key\":\"+Ol14AJOLtTxyChiXMe3qVAj9ZYPR79NQty8Z4jEMMU=\",\"encrypted\":\"Bqm2s+7yk2a74NJLraQXDkV69xkAqQ==\"},\"asymEncRSA_OAEP\":{\"plain\":\"hello crypto asymmetric\",\"algo\":\"RSA_OAEP_SHA256\",\"publicKey\":\"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArEspQsnlJxobBsQ3lOZQewggoH4Q+YWIIEkwKBQ2sXZdFjZNFramQpV9oXYQFiyu1o+F9TNMYNxvf0yFcYYdj1IaiaxnAz00BXt3S+AxoTaINqgfzstEyp\\/QnjyZBh9wBcBtDDQWyqbWpSSzrVqdIaW0UnsrFCUaPQej6IKoE8Vg\\/N5lxvoUrX73EVNXcsOaQ2wAUul4V4g3hVUUx83bxpR7kraXGNyRbAz+y78a1pJWfvRe2F\\/La1U3ish5vjNHDqkWizKlDb5uwfnqVhlJGO9uVOrH9BEsiKLVRs+N0TjGkursk40KBc\\/B+bge3amsvlkNctQthBM2V2LEq79h2wIDAQAB\",\"privateKey\":\"MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCsSylCyeUnGhsGxDeU5lB7CCCgfhD5hYggSTAoFDaxdl0WNk0WtqZClX2hdhAWLK7Wj4X1M0xg3G9\\/TIVxhh2PUhqJrGcDPTQFe3dL4DGhNog2qB\\/Oy0TKn9CePJkGH3AFwG0MNBbKptalJLOtWp0hpbRSeysUJRo9B6PogqgTxWD83mXG+hStfvcRU1dyw5pDbABS6XhXiDeFVRTHzdvGlHuStpcY3JFsDP7LvxrWklZ+9F7YX8trVTeKyHm+M0cOqRaLMqUNvm7B+epWGUkY725U6sf0ESyIotVGz43ROMaS6uyTjQoFz8H5uB7dqay+WQ1y1C2EEzZXYsSrv2HbAgMBAAECggEAA1cPrTZJxXjJlld+dloCrGfrbgETpiqzCWEx2E0QBF+1jUUvOGYBEiBgx50I0c069Hji0W6Vp\\/kOBks++QTvFMS0NAAOHS92DzcmCGgHvkbW3CxlNXaWVlFD05V8J\\/S7ELl6w7ZMzM\\/wMFJmU1GC41CZjtHJc4IGoLFBubOyu3NbdNArafnQrntoyEbZbfRPPWEHnYWhzU+dxS5z8IrI8cElmwbybq3gO8u2ulFGSbdbK+ZOo7X4DWhyxCSmAiD19yfu4Bx1g391g5WLTFyFG1dRIjGvAqJHMJD+nPeWTVXX94k\\/RZBFYlF49n1Pb2M3nCGbe+93\\/zQL5m+i3QnouQKBgQDhNX9Ot7pflT+DUMUaZaElz9wNct0Dp2\\/dW2mFbpOKXmt25RxrDvZMfGhfz7YzT4kgC9e\\/Tdrt6rGaxeCEKe58\\/TvlBoiAxtmO09ev18wauPa70e\\/sTSO+PA4g4EzXknLz07O4z8zz1t+5QFijlO8vs\\/wIFKaKJcdD4algZdqP0wKBgQDD2ZTX9lFoQ9pzHDWjwLg6SC+8XDiEnhPnJozu9YsFCDrg+nZQYhenOzbm6X8pN4RZW4qH9mXh9c7\\/4vI0fZtQCf3cYKFZ+\\/F3Pp6TK5\\/M+tYLkgc1dH7Yavg1ECEiQy0hJyTnozNAerqUxWhc0YNhQ77srRLAjO8jOavUxeOo2QKBgQCw520RUJE98+boJ9IsYhirsxOQHOIvtJ7SqrPA9JNA5dpUkrQ4BJDElBUI7giZVajJW4Ql6epHaUwJ39X0+1szI1ihFviy74CGCTIsFr9lXlWyQ77LR+0DyfjvKkIyM\\/\\/IOEu8qL6969PhmQIHvT4FosQfNNl9Ox\\/D2sdJybhdsQKBgQC4nRRaUt1ADJ4R3eiOOK8weTVOOBwqnpwiS0uUQS9Pigd+ZXo7oYusXvwMxNPSNXvzg3\\/dVB2L11rOCM7JPhG2bAS5xPizyvM9tXBjSD71SQmVlIxpDiw3svXPhynEk1AqLSzzkPpR8J6KeF89B13xxJtnnGnNycvuB4H7KqLEWQKBgBAc45apfBLnxvBWPpjhkZ0pmoBCJmvREmtWJ0N9sEuwDReNdbQTKgRYjVbwpnThzpxMBRYn+KpdC\\/Cl2R2QmdxA47GBiK9culZ+kyPP071x8CztLy8AEOabN2p6fbpTRpkczSGVSbK8LkGvUwYeR\\/RaVK5t+VYuhraxqBbxWYpJ\",\"encrypted\":\"pFULUU9fuycKErrlhghPNl0wcXVc+YraE1JX0CUZK6OnWuMunzAs7kZ+37zfKMUL96ywhCzwFXC9\\/6rjuXqjr3HTbp6HyOnG8Xz4FVBpNrZsoEjSoaTNr8umg3P28kDhh2ST1QDC2bqjn7se+qU27yfPVQZVl1LfyGp0YDMsWMWAwY8WGqgqua1ZEqpo\\/31gtugmsQr5YNBk1GOn8GEOSkv7HC5kAzElX+3hEoss27MgHsCwTSwZHapbnbENe1dNuaXafXDsnTsihjHZ6Z+Y3iGT\\/ZhYNA8mw\\/473Wsowzu1l6Odn63UvShbbPKmOSvaiN3c7PPk8RTKDA5U5Vl\\/9g==\"},\"signRSASSA_PKCS1_v1_5_SHA256\":{\"message\":\"Sign me message\",\"algo\":\"RSASSA_PKCS1_v1_5_SHA256\",\"publicKey\":\"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwAb\\/R3Eav00bwJKG6PCYCFDSko6RjiFbanFxHphXxo43wvH\\/0MLwlpN\\/tbRmV1MhfoweL0kbnklChwlDZdwqhUqtREVEvRl17QRSIAKbxAd6obzDKchbPvMb1zVHdeLF9IWmGpTLmBkl72l4o0WodDE+vSdwfHcHjUuSCxSGmPsE0IrCVtXo0dZ\\/BVGCQCbhl\\/jqSllFnHXWYwCmTqakhoEaoiQIV2RSeeX1wg+q+P1YbAVnCjASl14M\\/aWFyEDwM2z+SPorx3xEsWyzikdwYVS5c49B9SkXncVNC3fN3EQlsz1XIaczE0hW0JhL9I1uPhRCYqMGpv4mdxq4trwNUQIDAQAB\",\"privateKey\":\"MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDABv9HcRq\\/TRvAkobo8JgIUNKSjpGOIVtqcXEemFfGjjfC8f\\/QwvCWk3+1tGZXUyF+jB4vSRueSUKHCUNl3CqFSq1ERUS9GXXtBFIgApvEB3qhvMMpyFs+8xvXNUd14sX0haYalMuYGSXvaXijRah0MT69J3B8dweNS5ILFIaY+wTQisJW1ejR1n8FUYJAJuGX+OpKWUWcddZjAKZOpqSGgRqiJAhXZFJ55fXCD6r4\\/VhsBWcKMBKXXgz9pYXIQPAzbP5I+ivHfESxbLOKR3BhVLlzj0H1KRedxU0Ld83cRCWzPVchpzMTSFbQmEv0jW4+FEJiowam\\/iZ3Gri2vA1RAgMBAAECggEABs7os6F9YN13bQKKRngrR8DdKmWOyxLV29EFq\\/Tw8xZSlfaVtZLdIVWS8FVi+y3Jxk5qTo5hbIwCkBOXwGTdMWh2UAaYWzpWcu7AM6gsBM1Sv35tY\\/iZeu1Me3rv25c3TLCwCjQO1jJuuiLg7FcD\\/on1bxLsJc7kOrynGFNOugiSnTSyrNIYy2zwuIHZ0aFRtGvwKpq4z64yYse41MnxBuY4dRIMNdTgoMv1TNln8UV6mj8NV9XfCI79u5YAvICie1uGi6AQJV\\/1pdydcW\\/rHBgzy74UeLEylRwCwgQMyK\\/pIQTdvmf7OsR\\/bUmZi2Y0ftzMMNkQZC7JLSnM5Yya\\/QKBgQDhMduPRmxeDmFG4spd5uKQPRd7FaKFpbFqIaiNll5p08GxikjDoChDAsLWBHbfuXa6E5fXWIMXHYgZ0RClUxg+P0duFE8nHL4OH+xXhndnBl5yT4bQqjnZhsCN\\/YpjysnDyAOJG55a5ZSeeHs3pWkKNKH\\/Z7EaxB1WXnpaaTjN3QKBgQDaS6R\\/g5tM\\/WJFf9Qp\\/0tG6GS0\\/Nloub0ihf1Ki47TXN3ocP8XFHzgBnJR2GuGorzURPvEwWqPu5t4W43kHtCJ7Kdu5ePwgK5v88tMuO\\/KCkn6D7EfuWvNLEf6Bb\\/vK3xB4lbCzTlQxiPuD0RPkvRnJqyxufpn89NXV1iOMLOoBQKBgHJxvh1QMt6R\\/EnkE+go4FdhVL+OvSuii0lelaRvGMg7f6JUnqociJmVm6LcXfVQ8c+bCX29U9G5hmNoZkc+tYFc81Y0KA2+g6\\/19Ux938yR5gm99Muy1vd\\/jfgbM+uHMYa37WQOjymupxrqK+SZwYoYOYPtFIp\\/xFESbqfn9aBZAoGBAKmC8bMfNFp9rHEBiXQvTXo7ffBZJE5BZVBIsd7QLlGNHLih\\/kQi4bFo8B8EKsM63d0L0GICO8hvS5QTrERHdXKU9ZOVKwKYOlyu+OCT\\/4hpovZudwGGj+KcDyxkiJ2lq\\/DRXMQabvZ9VmtHKamNGQJ8sr+0BnHwkd2R8y8\\/PFMtAoGBAJ33Jw09hhnWBZBWGmOEmYke+HqTBgNEDY867hkQQ582ph5nQT\\/PR7mgaJAGXlWGzs56mxRcNxrXYju4SBcV8bZ4aYmxEEvAnm5JBaYPQsTbTT9f2jEW\\/roU94w1D6uCeQ6pZV93aD5c8qy1\\/oy33ZbIZ8Y4zF7NMagHLmkQhbgO\",\"signature\":\"FmCecsgyI\\/PCPF6ROntQaFujpC4m0vCJO1jdu3PXpdSuZCjEyS+m7rl+ABJN3X5kHomfeU73iguVt\\/8GRNOwQ3KU7hEfZC9jjxlUPqUEBeh+qs+mzlJwuAAmySK2xCO+ZnV+sHqeE\\/rPwmnYXL4qlRUXvb8FE\\/QaubHMtxPBHzPPS8dbBnFGvTFnLsN52iGJ8dwwleMWgaTaxE99PJKsr8MwaRQIZvTn+C3ug4YE4IFH\\/OGR+kCTkfYSEHqFWCqy1I1GhNQsgINwqv8hx7pmWxozU3ZqIuphRzWpd9fm\\/3fgYLPtO5u\\/7tYmH1hLTIfH4QG9TDUkldLYkgTB8squyA==\"},\"signECDSA_SECP256R1_SHA256\":{\"message\":\"Sign me message\",\"algo\":\"ECDSA_SECP256R1_SHA256\",\"publicKey\":\"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEQQzefOZivOpQ10visfR1g+BAISTshYv3JOosoXSzTpmvQDIlfYoRbIpLqC\\/HBWt0tK3amgPp5mW3KM+o7u1q1g==\",\"privateKey\":\"MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgEk2JvRBASk2+ZGUKEujsy4szBcKPyA4XVdHg7SvIfeuhRANCAARBDN585mK86lDXS+Kx9HWD4EAhJOyFi\\/ck6iyhdLNOma9AMiV9ihFsikuoL8cFa3S0rdqaA+nmZbcoz6ju7WrW\",\"signature\":\"N95Z3jW\\/sXaTCsQ7K81qbDfI3TjOMpMmv7CFB8Yee+nMtM8lcgWti8XzNwFxpvAFEkLB09FnYNjI8PUXnD3qjg==\"}}"
    // data produced on Firefox
    val firefoxTestData = "{\"symAES_CTR\":{\"plain\":\"hello crypto symmetric\",\"iv\":\"VN\\/NcKOvWaDBwFuCV2qqeg==\",\"algo\":\"AES_CTR\",\"key\":\"HwzqJE3Ja\\/ki1N8Bq3uIEtxi77Sq3jsRedwVcv7STa0=\",\"encrypted\":\"2t9+nhkj\\/yIl37PgKuX+qSuomJ2ZjQ==\"},\"asymEncRSA_OAEP\":{\"plain\":\"hello crypto asymmetric\",\"algo\":\"RSA_OAEP_SHA256\",\"publicKey\":\"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAy20bqp55YR5m\\/1hedkUjsc+FHiOfX+6\\/wrkC18AksH8Ohx4WSIrSBPuN2Wqc35vCf12FBXALXabFVsyLM7hBeMdgvGtgK6UophRpqtdb39kPU3e7kd1EajeqmjGPs7iZ6J8oVJ7dxcb6\\/CKqZPewxTFd0E7+BtYH9tEN6Rp738OYekdIBMNVCC6zg0OpzO7BJqwbtd3ebCNRGyhZ5FWhuxjWxh8YcsEW0HoDqBWl3yjKmOax9BZ3Msh4ByZuihdji\\/YjhmLZl3Aj4x3LpTYYj+UuZ6+UCWsebxVG+I2bEjvdGDt2mjtcSqRRvM1nrJIpoaSLqgseqdJ8ApBBcT2CmQIDAQAB\",\"privateKey\":\"MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDLbRuqnnlhHmb\\/WF52RSOxz4UeI59f7r\\/CuQLXwCSwfw6HHhZIitIE+43Zapzfm8J\\/XYUFcAtdpsVWzIszuEF4x2C8a2ArpSimFGmq11vf2Q9Td7uR3URqN6qaMY+zuJnonyhUnt3Fxvr8Iqpk97DFMV3QTv4G1gf20Q3pGnvfw5h6R0gEw1UILrODQ6nM7sEmrBu13d5sI1EbKFnkVaG7GNbGHxhywRbQegOoFaXfKMqY5rH0FncyyHgHJm6KF2OL9iOGYtmXcCPjHculNhiP5S5nr5QJax5vFUb4jZsSO90YO3aaO1xKpFG8zWeskimhpIuqCx6p0nwCkEFxPYKZAgMBAAECggEAD8x9rBQaZYOzcxxdWxPGkGjcs\\/kHR9o71AyDXyyMjOqT6cQumIazhfCK1TpgTEYwEQKQh1oVf+SpCd3hPRc6en4IPMnyvPZa6v8bL8oBrX0RoXq4kL0IXbQiEKSjE6MAIl64sU2\\/TTzFbFnoNZGTuH+Q2NHxt6fdaXqrTmcgp2xJa\\/GQIW3B75hlnIFoYq15mcMC\\/IvBVKOFeRa1IjPV4m1eeDXUi5d+a9HqP0BgcieCCmpwn9dFZpeGs3NA1L+CryUBnNcp5XoMKoij28kITepR0Rqo8Y9hBo8HFLaSJRLkVJRgdF9\\/3i26R5WMAZlpeb0C3Jzca9quNIcTPsyfiwKBgQDzPNExBe6eqM363kXZcoGzwjCUTgKjzZb6ogZCS2gOw62dCEfYvN0exDNiihBgSa+EsOvmVJBFnd1Hanh4ynaA8xFahsKvI0XH+9YnF8EIfWZynYsQqv0rTRdq9unt4xYOLMciRTANup6YSTzLwO8uAXH\\/b2tuT+\\/Tw5fdmL86hwKBgQDWGYrGT+fmHHeJetVyuw1iEBis\\/a1Qi6TrL3+mIiNT\\/781eIRyv5qnKf4U922+gMu9qmSMwV7Zi3vCqDMfRKMAQYLPy9cNaLE8+96DF61XAm1x8RCVxCmrC2OeX3WlTjZTuooH08jtwiElew0N71ZpTl\\/Vp8oHPnXa144mgdkB3wKBgDCBR0IcabpP9Ut6ubkkIYNETUW1zMb3f40ZdGASA302Pk\\/bT+CDwSCahZi36Y4aA5tVYKV85YxMb80HCWElFr6eOi6wQvCD+y1nJgyDqyXTQ\\/PFQO8g+f2BkRLujDW5HDPVcByAaZXDSk8aA7qqDchB14bVpDnKTQwQrB8vj6rPAoGAeQzL98Ap\\/2LM+0Al+s5jjLRJH1X2bfnmbsrWQ1o92zpYI6VWTYKjsCv\\/6l7S\\/8jWaRAz9ZlHRXylprNu5BjsiDKCAXbgykYNH8vQTRJQAUfwFWIU3HiijoDL\\/DlAzQvhj4mCjVZi3nrKPuTJxr0xgCy95pENmvbnr6n8Ry1XQsUCgYBaNQEfCYvAo4ns7OWO88lex87oTcGFGU5f\\/iGW7Ax76xWnwkFeijdpHp+AnL2mTY51PvGQrc3CBV\\/amA7iXViuIwUjXLW8r0NeNxpRoBT9Bc\\/kwH58tw2rMGXhpVtvcK0ZIjop\\/whlnrwcl7Yqty5o8SNVu8vjYcMpQPu9O7wn7g==\",\"encrypted\":\"dglpD+iCWARcwOoCWdxNFbD26evWj50TSL0wX+3TgaozmS+p7k\\/RE0TOQog956XFa9Me+eOLhWb\\/PDbdB70UzA4IwZMAe\\/KgeeW5SWH21OSBwd\\/BuB+lCL3ovTTgipBDQ135XcS1XUHVuRMyvjWWt4GZHp2FuRZzCBXPDmAToachD88kx4nuPcNnE6gaYd7NO7JLRpzFbmuYIher8BKVAsF1A1dAFoPjhTxX4JRpckRGzezScZe6y+HnGJnSEjg03AE2GWFAPRBvk1bTPx8kTF5\\/dPDa10CG\\/Kl5PzMuV0GpmaTs9latrQTdSFxvzmAbUzq\\/rCv7inPxBesaGWUn9g==\"},\"signRSASSA_PKCS1_v1_5_SHA256\":{\"message\":\"Sign me message\",\"algo\":\"RSASSA_PKCS1_v1_5_SHA256\",\"publicKey\":\"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArYfkPKda1ep0Fbr+FOcLsPuMk2zCVxcaz\\/S96UJpXHZso\\/sxpYknHIKW6XddVyR4vV86SCfXGM3J4oXH974ih5ZMEYyN7TOwo4enYt6BSnkWdbTUfmSnxuRf+97XtKbJzeZ+wK8h3aXcJpDaAF121tw\\/Ke2T7e1RQ6kFVqti8yf3p21jZAxEXscfK+n73RGioLOAFzJivTSU0R9AJgcKo\\/krtSayo2c4AR0+F7ikfbVIAtm6BsMf0hkhwjgp7nhX\\/iZXdy2YFUh54TgNZpDNixKBGKjf17Ue6lR1URm3UXPewox6+30DacVkBJP8vETeJnozSBxbOIgEusni3EPZ1QIDAQAB\",\"privateKey\":\"MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCth+Q8p1rV6nQVuv4U5wuw+4yTbMJXFxrP9L3pQmlcdmyj+zGliSccgpbpd11XJHi9XzpIJ9cYzcnihcf3viKHlkwRjI3tM7Cjh6di3oFKeRZ1tNR+ZKfG5F\\/73te0psnN5n7AryHdpdwmkNoAXXbW3D8p7ZPt7VFDqQVWq2LzJ\\/enbWNkDERexx8r6fvdEaKgs4AXMmK9NJTRH0AmBwqj+Su1JrKjZzgBHT4XuKR9tUgC2boGwx\\/SGSHCOCnueFf+Jld3LZgVSHnhOA1mkM2LEoEYqN\\/XtR7qVHVRGbdRc97CjHr7fQNpxWQEk\\/y8RN4mejNIHFs4iAS6yeLcQ9nVAgMBAAECggEAG7X7JwpskYN+pSe2wlzA+7K32KUCBclNQU8qcpDl5VuN7sPCwlX8FmvuMHscvzk2RLtrwbEHAtF9\\/zvw2mmU9wTzwcupsGrUNtEHVF0G1X8vrH1Df6mQN00p3L1RwenuiQBZVmjBJjWByfDQt0U1oIsMVSQ4vsJqez0G+rektgJ1EsDn1J5yIXDyiZld34rsLFZGDttRwfC5fojdQiypIVnPHuzgNWquOngpctnNXf3GHlNG7rlmYZ60c8CpxA9VCdZGysHPKURtqaxQdaEkksGYbijBHhQ\\/uMvJkcrENnqGYD6l8E5DCm3JE\\/Nud7Z7dCXGFJMIxIKMUZp9tMZNUQKBgQDUranIsjZTn3Tkzq+JMvjuxOgwvxtJNGWO3jVTRJR5FWJ7dxOHrWFY7oTBw6MlbpJu3q0dP3hEa9awcLqynBTEZDfhwdOjr3C+hiNy7IJnSuHjBkZVtiBbN0nD68NUwyoFN23Kr+lCe6wissyjYJv1ueNoSsY7S7k7GsQYzQ9KjQKBgQDQ4NaoV+7+tI5qZJGte+G+s2jqtP9SDuTVBReM\\/caV9bHRFptKtGVaRLg2yNsv+w\\/pltCYRw7ZzxlCn2ipSnYK3t423c7VOFFnebTlLtwQtkK2222TvH95ydPH0y9eeh1CpawL0YKfSoSN+be7hI9zqsbBifx6Uh5s9j2\\/zbjeaQKBgCh18XsRDGzURTTSbWq4m8sxAstjUBmZMkj\\/1T1GKsjuXJIIkVqajY6Rsgf65CrW4lzdWCncTVEXAv0GNT6NmxXRyx\\/T9COL2PMmKdBPVm+4O0kphHAB+h3gYtQ3Ii4aAwCDDwwkc0gGpzTitzPNyV3yQ2Uv9I6LVW39JZliSapxAoGAVhEO4fa5p4HGXNhrnkw5TFaomG1T9v\\/kElIyzgmTVh8S0dC61CFY0cUXEpvYlQCiIeHeuaAt4lmPUwBUI81mrjv+3CSxIPI\\/uJ9Rhk20p74x5x6rHADDle8vsFq4Xfl7LQcOyR\\/e7rvJuqZ2qnGImmR32S9+NPi5SLR95RZjYmECgYASUeIeRodd+jAJbSITNotqDQ+PcDEfmmBkiCBdR4CwKOGgb9RupiBOISsDThCmhUw5K4O2mNl88ay34p6cuejH\\/ht5xKvxMkYDT73TUYpXM81swoZrimtvC1UwajdagXR5+hb5j++HbzRsEANyw6w\\/qNvNQ9pgyWicemW96F4UYA==\",\"signature\":\"eGqTNdJqeOdDZN5RYxsy8bD8nPos9RWj1X1ab\\/cloxj356nyxmBrgCaS1vy22mlrSXtfXxsHcuVrifF6I3Rh+I89fRU5fJqVxnQp\\/AkPtXmFWx1plg4xMEFDpfDSTOEPRUAssw0MeJq2TGFm\\/qn1HUz6TUYZ8bixppJqVtjUCvCOl8iq\\/sFEO3mOH2YzJCa0KI5NCEYyRbaoXxfQe8SGaXyForSOcB4WTJsjSaWv9VPhmP640AvMXZSMdlkF\\/xu+dl3UECvr8DT0fQq2qEXcc+PY03X5o4M0Y2LWRnm4OIuyRlQhLsjTlcIWCPM96rbVB1BTeuYftwlaYZRovmg73Q==\"},\"signECDSA_SECP256R1_SHA256\":{\"message\":\"Sign me message\",\"algo\":\"ECDSA_SECP256R1_SHA256\",\"publicKey\":\"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAELr7myLlahJ8z\\/6yaIET8XIBEGa9UNaqM2cleGkZXOJ1g8Zm2enhXtnAUohNcLxDlpLo9jFsf3ffqkAGUlu26zg==\",\"privateKey\":\"MEECAQAwEwYHKoZIzj0CAQYIKoZIzj0DAQcEJzAlAgEBBCDCUOQEd0zIOQDEBkwJ5eViMB7dW1MB\\/ICs0idNKh13Xg==\",\"signature\":\"3Fs431BwqqWsCPW2u7ew+TGXNzKBrLN9e8qgGKYRfs9\\/SgUBjpAEmg9iJvp20IqER+P4mL\\/hgzdmgV3+Fewh+w==\"}}"

    @Test
    fun testTestData() = testAsync {
        testTestData(JSON.parse<TestData>(jvmTestData))
        testTestData(JSON.parse<TestData>(chromeTestData))
        //testTestData(JSON.parse<TestData>(firefoxTestData))
    }

    private suspend fun gatherTestData(symmetric: CryptoSettings.Symmetric): TestDataSymEnc {
        val crypto = CryptoHelper.crypto

        val plain = "hello crypto symmetric"
        val iv = crypto.generateBits(symmetric.algo.ivSize)
        var secretKey = crypto.generateSymmetricKey(symmetric)
        val encrypted = crypto.encryptSymmetric(plain.toUTF(), secretKey, iv, symmetric.algo)

        return TestDataSymEnc(plain, iv.encodeBase64(), symmetric.algo, crypto.encode(secretKey).encodeBase64(),
                encrypted.encodeBase64())
    }

    private suspend fun gatherTestDataAsymEnc(settings: CryptoSettings.Asymmetric): TestDataAsymEnc {
        val crypto = CryptoHelper.crypto

        val keyPair = crypto.generateKeyPair(settings)
        val plain = "hello crypto asymmetric"
        val encrypted = crypto.encryptAsymmetric(plain.toUTF(), keyPair.publicKey, settings.algo)

        return TestDataAsymEnc(plain, settings.algo,
                publicKey = crypto.encode(keyPair.publicKey).encodeBase64(),
                privateKey = crypto.encode(keyPair.privateKey).encodeBase64(),
                encrypted = encrypted.encodeBase64())
    }

    private suspend fun gatherTestDataSign(settings: CryptoSettings.Asymmetric): TestDataSign {
        val crypto = CryptoHelper.crypto

        val keyPair = crypto.generateKeyPair(settings)
        val message = "Sign me message"
        val signature = crypto.sign(message.toUTF(), keyPair.privateKey, settings.algo)

        return TestDataSign(message, settings.algo,
                publicKey = crypto.encode(keyPair.publicKey).encodeBase64(),
                privateKey = crypto.encode(keyPair.privateKey).encodeBase64(),
                signature = signature.encodeBase64())
    }

    @Test // enable to gather data
    fun gatherTestData() = testAsync {
        val symAES_CTR = gatherTestData(CryptoSettings.getAES_CTR())
        val asymEncRSA_OAEP = gatherTestDataAsymEnc(CryptoSettings.getRSA_OAEP_SHA256())
        val signRSASSA_PKCS1_v1_5_SHA256 = gatherTestDataSign(CryptoSettings.getRSASSA_PKCS1_v1_5_SHA256())
        val signECDSA_SECP256R1_SHA256 = gatherTestDataSign(CryptoSettings.getECDSA_SECP256R1_SHA256())

        // print as a json string
        val data = TestData(
                symAES_CTR = symAES_CTR,
                asymEncRSA_OAEP = asymEncRSA_OAEP,
                signRSASSA_PKCS1_v1_5_SHA256 = signRSASSA_PKCS1_v1_5_SHA256,
                signECDSA_SECP256R1_SHA256 = signECDSA_SECP256R1_SHA256
        )
        println(JSON.stringify(data))
    }
}
