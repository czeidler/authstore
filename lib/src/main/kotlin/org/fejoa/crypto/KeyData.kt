package org.fejoa.crypto

import kotlinx.serialization.Serializable
import org.fejoa.storage.HashValue
import org.fejoa.support.decodeBase64
import org.fejoa.support.encodeBase64


@Serializable
class EncData(val encData: String, val iv: String, val algo: CryptoSettings.SYM_ALGO) {
    constructor(encryptedData: ByteArray, iv: ByteArray, algo: CryptoSettings.SYM_ALGO)
            : this(encryptedData.encodeBase64(), iv.encodeBase64(), algo)

    fun getIv(): ByteArray {
        return iv.decodeBase64()
    }

    fun getEncData(): ByteArray {
        return encData.decodeBase64()
    }
}

open class SecretKeyData(val key: SecretKey, val algo: CryptoSettings.SYM_ALGO)

class SecretKeyIvData(key: SecretKey, val iv: ByteArray, algo: CryptoSettings.SYM_ALGO)
    : SecretKeyData(key, algo)

suspend fun CryptoSettings.Symmetric.generateSecretKeyData(): SecretKeyData {
    val secret = CryptoHelper.crypto.generateSymmetricKey(this)
    return SecretKeyData(secret, algo)
}

class PublicKeyData(val publicKey: PublicKey, val algo: CryptoSettings.ASYM_ALGO)

class PrivateKeyData(val privateKey: PrivateKey, val algo: CryptoSettings.ASYM_ALGO)

class KeyPairData(val keyPair: KeyPair, val algo: CryptoSettings.ASYM_ALGO) {
    suspend fun getId(): HashValue {
        return keyPair.getId()
    }
}

fun KeyPairData.asPublicKeyData(): PublicKeyData {
    return PublicKeyData(keyPair.publicKey, algo)
}

