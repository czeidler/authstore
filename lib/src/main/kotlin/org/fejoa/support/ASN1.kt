package org.fejoa.support


class ASN1Length {
    companion object {
        fun writeLength(outStream: OutStream, number: Int): OutStream {
            if (number < 128) {
                outStream.write(number.toByte())
                return outStream
            }

            // long format
            val mask = 0xFF
            var nBytes = 1
            for (i in 3 downTo 1) {
                val m = mask shl (i * 8)
                if (m and number != 0) {
                    nBytes = i + 1
                    break
                }
            }
            outStream.write(((1 shl 7) or nBytes).toByte())
            for (i in nBytes - 1 downTo 0) {
                val part = (mask shl (i * 8)) and number
                outStream.write((part shr (i * 8)).toByte())
            }
            return outStream
        }

        fun readLength(inStream: InStream): Int {
            val byte1 = inStream.read()
            if (byte1 == 0x80) // invalid length
                return -1
            if (((1 shl 7) and byte1) == 0)
                return byte1
            val nBytes = (1 shl 7).inv() and byte1

            var number = 0
            for (i in nBytes -1 downTo 0) {
                val byte = inStream.read()
                number = number or (byte shl (i * 8))
            }
            return number
        }
    }
}

enum class ASN1_TAG_CLASS(val value: Int) {
    UNIVERSAL(0x0),
    APPLICATION(0x1),
    CONTEXT_SPECIFIC(0x2),
    PRIVATE(0x3)
}

enum class ASN1_TYPE(val value: Int) {
    INTEGER(2),
    BIT_STRING(3),
    OCTET_STRING(4),
    OBJECT_IDENTIFIER(6),
    SEQUENCE(16),
}

enum class ASN1_FORM(val value: Int) {
    PRIMITIVE(0),
    CONSTRUCTED(1)
}

/*
 * Implements a subset of ASN.1
 */
class ASN1Entry(val tag: Int, var byteArray: ByteArray = ByteArray(0),
                val children: MutableList<ASN1Entry> = ArrayList(),
                val tagClass: ASN1_TAG_CLASS = ASN1_TAG_CLASS.UNIVERSAL) {

    constructor(type: ASN1_TYPE, byteArray: ByteArray = ByteArray(0),
                tagClass: ASN1_TAG_CLASS = ASN1_TAG_CLASS.UNIVERSAL)
            : this(type.value, byteArray, tagClass = tagClass)

    companion object {
        fun parse(byteArray: ByteArray): ASN1Entry? {
            return ASN1Entry.parse(ByteArrayInStream(byteArray))
        }

        fun parse(inStream: InStream): ASN1Entry? {
            val id = inStream.read()
            if (id < 0)
                return null

            val tagClassValue = (id and 0b11000000) shr 6
            val tagClass = ASN1_TAG_CLASS.values().first { it.value == tagClassValue }
            val formValue = (id and 0b00100000) shr 5
            val form = ASN1_FORM.values().first { it.value == formValue }
            val tag = id and 0b00011111
            if (tag >= 0b00011111)
                throw Exception("Tag values > 30 not supported yet")

            val length = ASN1Length.readLength(inStream)
            val buffer = if (length >= 0) ByteArray(length).also { inStream.read(it) } else inStream.readAll()

            val children = when {
                form == ASN1_FORM.CONSTRUCTED -> parseChildren(ByteArrayInStream(buffer))
                else -> ArrayList()
            }

            return ASN1Entry(tag, buffer, children, tagClass)
        }

        private fun parseChildren(inStream: InStream): MutableList<ASN1Entry> {
            val children: MutableList<ASN1Entry> = ArrayList()

            while (true) {
                val child = ASN1Entry.parse(inStream) ?: break
                children.add(child)
            }
            return children
        }
    }

    fun write(outStream: OutStream) {
        if (tagClass == ASN1_TAG_CLASS.CONTEXT_SPECIFIC && tag > 30)
            throw Exception("Tag values > 30 not supported yet")

        var firstByte = tagClass.value shl 6
        val form = if (children.isEmpty()) ASN1_FORM.PRIMITIVE else ASN1_FORM.CONSTRUCTED
        firstByte = firstByte or (form.value shl 5)
        firstByte = firstByte or tag

        val outData = if (children.isNotEmpty()) {
            val out = ByteArrayOutStream()
            children.forEach {
                it.write(out)
            }
            out.toByteArray()
        } else
            byteArray

        val out = ByteArrayOutStream()
        ASN1Length.writeLength(out, outData.size)
        val lengthArray = out.toByteArray()

        outStream.write(firstByte.toByte())
        outStream.write(lengthArray)
        outStream.write(outData)
    }

    fun getContextSpecific(tag: Int): ASN1Entry? {
        return children.firstOrNull { it.tagClass == ASN1_TAG_CLASS.CONTEXT_SPECIFIC && it.tag == tag }
    }

    fun addContextSpecific(tag: Int): ASN1Entry {
        val entry = ASN1Entry(tag, tagClass = ASN1_TAG_CLASS.CONTEXT_SPECIFIC)
        children.add(entry)
        return entry
    }

    fun toByteArray(): ByteArray {
        val out = ByteArrayOutStream()
        write(out)
        return out.toByteArray()
    }
}
