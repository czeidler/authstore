package org.fejoa.support


object DeflateUtils {
    suspend fun deflate(data: ByteArray): ByteArray {
        val out = ByteArrayOutStream()
        val deflator = DeflateOutStream(out)
        deflator.write(data)
        deflator.close()
        return out.toByteArray()
    }

    suspend fun inflate(data: ByteArray): ByteArray {
        val out = ByteArrayOutStream()
        val inflator = InflateOutStream(out)
        inflator.write(data)
        inflator.close()
        return out.toByteArray()
    }
}