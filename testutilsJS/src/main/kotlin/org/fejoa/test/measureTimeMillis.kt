package org.fejoa.test

import kotlin.js.Date

actual inline fun measureTimeMillis(block: () -> Unit): Long {
    val startTime = Date().getTime()
    block.invoke()
    return (Date().getTime() - startTime).toLong()
}