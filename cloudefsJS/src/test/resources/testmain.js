requirejs.config({
    // path where all main libs are located, e.g. kotlin.js
    baseUrl: '/base/cloudefsJS/build/lib',

    paths: { // paths are relative to this file
        'pako': '../../../jsbindings/build/resources/main/libs/pako.min',
        'cloudefsJS': '../../build/classes/main/cloudefsJS',
        'cloudefsJS_test': '../../build/classes/test/cloudefsJS_test',
    },

    deps: ['cloudefsJS_test'],

    // start tests when done
    callback: window.__karma__.start
});
