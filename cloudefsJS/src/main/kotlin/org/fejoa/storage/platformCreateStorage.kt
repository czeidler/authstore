package org.fejoa.storage

import kotlinx.serialization.SerialContext
import kotlinx.serialization.json.JSON
import org.fejoa.crypto.CryptoHelper
import org.fejoa.jsbindings.*
import org.fejoa.repository.BranchLog
import org.fejoa.repository.BranchLogEntry
import org.fejoa.repository.BranchLogMessageJsonSerializer
import org.fejoa.support.Future
import org.fejoa.support.async
import org.fejoa.support.await
import kotlin.browser.window
import kotlin.coroutines.experimental.suspendCoroutine
import kotlin.js.Date
import kotlin.js.json


/**
 * IndexDB does not provide a way to list all databases.
 *
 * This class maintains such a list manually
 */
class IndexDatabaseNameTracker private constructor(val db: IDBDatabase) {
    companion object {
        private val OBJECT_STORE_NAME = "databases"
        private val KEY = "key"
        private val VALUE = "value"

        suspend fun open(): IndexDatabaseNameTracker {
            val dbName = "databaseNameIndex"

            val indexDB = window.indexDB()
            val openRequest = indexDB.open(dbName, 1)

            openRequest.onupgradeneeded = {
                var db = openRequest.result
                db.createObjectStore(OBJECT_STORE_NAME, json("keyPath" to KEY))
            }

            return IndexDatabaseNameTracker(openRequest.await())
        }
    }

    suspend fun add(name: String) {
        val transaction = db.transaction(OBJECT_STORE_NAME, "readwrite")
        val store = transaction.objectStore(OBJECT_STORE_NAME)
        store.put(json(KEY to name, VALUE to name)).await()
    }

    suspend fun remove(name: String) {
        val transaction = db.transaction(OBJECT_STORE_NAME, "readwrite")
        val store = transaction.objectStore(OBJECT_STORE_NAME)
        store.delete(name).await()
    }

    suspend fun list(): List<String> {
        val transaction = db.transaction(OBJECT_STORE_NAME, "readonly")
        val store = transaction.objectStore(OBJECT_STORE_NAME)
        val eventRequest = store.openCursor()

        val output: MutableList<String> = ArrayList()
        eventRequest.onsuccess = { event ->
            event.target.result.unsafeCast<IDBCursor?>()?.let {
                output.add(it.value[VALUE].unsafeCast<String>())
                it.advance(1)
            }
        }
        transaction.await()
        return output
    }
}

class IndexDBStorageBackend(val context: String) : StorageBackend {
    class IndexDBChunkTransaction(val db: IDBDatabase) : ChunkTransaction {
        override fun finishTransaction(): Future<Unit> {
            // TODO()
            return Future.completedFuture(Unit)
        }

        override fun cancel(): Future<Unit> {
            // TODO
            return Future.completedFuture(Unit)
        }

        override fun getChunk(boxHash: HashValue): Future<ByteArray> = async {
            val transaction = db.transaction(AccountStorage.CHUNKS_OBJECT_STORE, "readonly")
            val store = transaction.objectStore(AccountStorage.CHUNKS_OBJECT_STORE)
            val result = store.get(boxHash.toHex()).await()[CHUNK_VALUE_KEY].unsafeCast<ByteArray>()
            result
        }

        override fun putChunk(data: ByteArray): Future<PutResult<HashValue>> = async {
            val key = HashValue(CryptoHelper.sha256Hash(data))

            val transaction = db.transaction(AccountStorage.CHUNKS_OBJECT_STORE, "readwrite")
            val store = transaction.objectStore(AccountStorage.CHUNKS_OBJECT_STORE)
            val hexKey = key.toHex()
            var exist = false
            store.count(hexKey).then {
                if (it > 0)
                    exist = true
                store.put(json(CHUNK_KEY to key.toHex(), CHUNK_VALUE_KEY to data))
            }.await()

            PutResult(key, exist)
        }

        override fun hasChunk(boxHash: HashValue): Future<Boolean> = async {
            val transaction = db.transaction(AccountStorage.CHUNKS_OBJECT_STORE, "readonly")
            val store = transaction.objectStore(AccountStorage.CHUNKS_OBJECT_STORE)
            val result = store.count(boxHash.toHex()).await()
            return@async result > 0
        }

        override fun iterator(): ChunkTransaction.Iterator {
            return object : ChunkTransaction.Iterator {
                private var list: MutableList<Pair<HashValue, ByteArray>> = ArrayList()
                private var isLoading = false
                private var hasNextFuture: Future<Boolean> = Future()

                /**
                 * Note: loadList is never canceled so if the iterator is not advanced all items are load into memory.
                 */
                private fun loadList(): Future<Unit> = async {
                    val transaction = db.transaction(AccountStorage.CHUNKS_OBJECT_STORE, "readonly")
                    val store = transaction.objectStore(AccountStorage.CHUNKS_OBJECT_STORE)
                    val eventRequest = store.openCursor()

                    eventRequest.onsuccess = { event ->
                        event.target.result.unsafeCast<IDBCursor?>()?.let {
                            val key = HashValue.fromHex(it.value[CHUNK_KEY].unsafeCast<String>())
                            val chunk = it.value[CHUNK_VALUE_KEY].unsafeCast<ByteArray>()
                            list.add(key to chunk)
                            it.advance(1)
                            hasNextFuture.setResult(true)
                            hasNextFuture = Future()
                        }
                    }
                    transaction.await()
                    hasNextFuture.setResult(false)
                    Unit
                }

                override suspend fun hasNext(): Boolean {
                    if (!isLoading) {
                        isLoading = true
                        loadList()
                    }
                    if (list.isNotEmpty())
                        return true
                    return hasNextFuture.await()
                }

                override suspend fun next(): Pair<HashValue, ByteArray> {
                    return list.removeAt(0)
                }

            }
        }

    }

    class IndexDBChunkStorage(val db: IDBDatabase) : ChunkStorage {
        override fun startTransaction(): ChunkTransaction {
            return IndexDBChunkTransaction(db)
        }
    }

    class IndexDBLog(val db: IDBDatabase, val branch: String) : BranchLog {
        companion object {
            val TIME_KEY = "time"
            private val VALUE_KEY = "entry"
        }
        override fun getBranchName(): String {
            return branch
        }

        override suspend fun add(id: HashValue, message: ByteArray, changes: List<HashValue>) {
            val entry = BranchLogEntry(Date().getTime().toLong(), id, BranchLogEntry.Message(message))
            entry.changes.addAll(changes)
            add(entry)
        }

        private fun toJson(entry: BranchLogEntry): String {
            val context = SerialContext().apply {
                registerSerializer(HashValue::class, HashValueDataSerializer)
                registerSerializer(BranchLogEntry.Message::class, BranchLogMessageJsonSerializer)
            }
            return JSON(context = context, indented = true).stringify(entry)
        }

        private fun fromJson(string: String): BranchLogEntry {
            val context = SerialContext().apply {
                registerSerializer(HashValue::class, HashValueDataSerializer)
                registerSerializer(BranchLogEntry.Message::class, BranchLogMessageJsonSerializer)
            }
            return JSON(context = context, indented = true).parse(string)
        }

        override suspend fun add(entry: BranchLogEntry) {
            val transaction = db.transaction(AccountStorage.LOG_OBJECT_STORE, "readwrite")
            val store = transaction.objectStore(AccountStorage.LOG_OBJECT_STORE)
            store.put(json(TIME_KEY to entry.time.toString(), VALUE_KEY to toJson(entry))).await()
        }

        override suspend fun add(entry: BranchLogEntry, expectedEntryId: HashValue?): Boolean {
            val transaction = db.transaction(AccountStorage.LOG_OBJECT_STORE, "readwrite")
            val store = transaction.objectStore(AccountStorage.LOG_OBJECT_STORE)

            val eventRequest = store.openCursor(null, "prev")
            try {
                eventRequest.then {
                    it?.let {
                        val latestEntryJson = it.value[VALUE_KEY].unsafeCast<String>()
                        val latestEntry = fromJson(latestEntryJson)
                        if (latestEntry.entryId != expectedEntryId)
                            throw Exception("Entry missmatch")
                        store.put(json(TIME_KEY to entry.time.toString(), VALUE_KEY to toJson(entry)))
                    } ?: throw Exception("No entries")
                }
            } catch (e: Exception) {
                return false
            }
            transaction.await()
            return true
        }

        override suspend fun getEntries(): List<BranchLogEntry> {
            val transaction = db.transaction(AccountStorage.LOG_OBJECT_STORE, "readonly")
            val store = transaction.objectStore(AccountStorage.LOG_OBJECT_STORE)
            val eventRequest = store.openCursor(null, "prev")

            val output: MutableList<BranchLogEntry> = ArrayList()
            eventRequest.onsuccess = { event ->
                event.target.result.unsafeCast<IDBCursor?>()?.let {
                    output.add(fromJson(it.value[VALUE_KEY].unsafeCast<String>()))
                }
            }

            suspendCoroutine<Unit> { continuation ->
                transaction.oncomplete = {
                    continuation.resume(Unit)
                }
            }

            return output
        }

        override suspend fun getHead(): BranchLogEntry? {
            val transaction = db.transaction(AccountStorage.LOG_OBJECT_STORE, "readonly")
            val store = transaction.objectStore(AccountStorage.LOG_OBJECT_STORE)
            val eventRequest = store.openCursor(null, "prev")
            return eventRequest.await()?.let {
                fromJson(it.value[VALUE_KEY].unsafeCast<String>())
            }
        }

    }

    class BranchBackend(val db: IDBDatabase, val branch: String) : StorageBackend.BranchBackend {
        override fun getChunkStorage(): ChunkStorage {
            return IndexDBChunkStorage(db)
        }

        override fun getBranchLog(): BranchLog {
            return IndexDBLog(db, branch)
        }
    }

    companion object {
        private const val CHUNK_KEY = "key"
        private const val CHUNK_VALUE_KEY = "chunk"
    }

    override suspend fun create(namespace: String, branch: String): StorageBackend.BranchBackend {
        val db = AccountStorage(context, namespace).createBranch(branch)
        return BranchBackend(db, branch)
    }

    override suspend fun open(namespace: String, branch: String): StorageBackend.BranchBackend {
        val db = AccountStorage(context, namespace).openBranch(branch)
        return BranchBackend(db, branch)
    }

    override suspend fun exists(namespace: String, branch: String): Boolean {
        return AccountStorage(context, namespace).branchExists(branch)
    }

    override suspend fun delete(namespace: String, branch: String) {
        AccountStorage(context, namespace).deleteBranch(branch)
    }

    override suspend fun deleteNamespace(namespace: String) {
        AccountStorage(context, namespace).deleteNamespace()
    }

    override suspend fun listBranches(namespace: String): Collection<BranchLog> {
        return AccountStorage(context, namespace).listBranches()
                .map { branch ->
                    open(namespace, branch).getBranchLog()
                }
    }

    override suspend fun listNamespaces(): Collection<String> {
        val nameTracker = IndexDatabaseNameTracker.open()
        val branches = nameTracker.list()
        val matchString = if (context.isBlank())
            "([^/]+)[]*"
        else
            "$context/([^/]+)[]*"
        return branches.mapNotNull { it.match(matchString) }
                .map { it[1] }
                .distinct()
    }
}

/**
 * @context the storage context, e.g. the base directory path
 */
actual fun platformCreateStorage(context: String): StorageBackend {
    return IndexDBStorageBackend(context)
}