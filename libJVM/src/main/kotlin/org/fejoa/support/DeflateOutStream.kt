package org.fejoa.support

import java.io.ByteArrayOutputStream
import java.io.OutputStream
import java.util.zip.DeflaterOutputStream


class AsyncDeflateOutStream constructor(val outStream: AsyncOutStream) : AsyncOutStream {
    val buffer = ByteArrayOutputStream()
    val internal = DeflaterOutputStream(buffer)

    override suspend fun write(buffer: ByteArray, offset: Int, length: Int): Int {
        internal.write(buffer, offset, length)
        return length
    }

    override suspend fun flush() {
        internal.flush()
    }

    override suspend fun close() {
        internal.close()
        outStream.write(buffer.toByteArray())
    }
}

actual class DeflateOutStream actual constructor(val outStream: OutStream) : OutStream {
    private val deflater = DeflaterOutputStream(object : OutputStream() {
        override fun write(byte: Int) {
            outStream.writeByte(byte)
        }
    })

    override fun write(data: ByteArray, offset: Int, length: Int): Int {
        deflater.write(data, offset, length)
        return length
    }

    override fun write(byte: Byte): Int {
        deflater.write(byte.toInt())
        return 1
    }

    override fun flush() {
        deflater.flush()
    }

    override fun close() {
        deflater.close()
    }
}