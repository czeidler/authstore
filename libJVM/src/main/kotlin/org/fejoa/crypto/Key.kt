package org.fejoa.crypto


interface KeyJVM {
    val key: java.security.Key

    fun toByteArray(): ByteArray {
        return key.encoded
    }
}

open class SecreteKeyJVM(key: java.security.Key, algo: CryptoSettings.SYM_ALGO) : SecretKey, KeyJVM {
    override val key: java.security.Key = key
    override val algo: CryptoSettings.SYM_ALGO = algo
}

open class PrivateKeyJVM(key: java.security.Key, algo: CryptoSettings.ASYM_ALGO) : PrivateKey, KeyJVM {
    override val key: java.security.Key = key
    override val algo: CryptoSettings.ASYM_ALGO = algo
}

open class PublicKeyJVM(key: java.security.Key, algo: CryptoSettings.ASYM_ALGO) : PublicKey, KeyJVM {
    override val key: java.security.Key = key
    override val algo: CryptoSettings.ASYM_ALGO = algo
}
