package org.fejoa.test

actual inline fun measureTimeMillis(block: () -> Unit): Long {
    return kotlin.system.measureTimeMillis(block)
}