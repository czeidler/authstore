package org.fejoa.test

import kotlinx.coroutines.experimental.CoroutineScope
import kotlinx.coroutines.experimental.runBlocking

actual fun <T> testAsync(body: suspend CoroutineScope.() -> T) {
    runBlocking(block = body)
}

actual fun <T> testAsync(cleanUp: suspend () -> Unit, body: suspend () -> T) {
    return testAsync(null, cleanUp, body)
}

actual fun <T> testAsync(setUp: (suspend () -> Unit)?, cleanUp: (suspend () -> Unit)?, body: suspend () -> T) {
    return testAsync {
        try {
            setUp?.invoke()
            body.invoke()
        } finally {
            cleanUp?.invoke()
        }
    }
}