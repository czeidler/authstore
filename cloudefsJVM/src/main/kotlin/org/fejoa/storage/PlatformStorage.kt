package org.fejoa.storage

import org.fejoa.chunkstore.ChunkStore
import org.fejoa.repository.BranchLog
import org.fejoa.repository.ChunkStoreBranchLog
import org.fejoa.repository.toChunkTransaction
import org.fejoa.support.PathUtils
import java.io.File
import java.io.IOException


actual fun platformCreateStorage(context: String): StorageBackend {
    return ChunkStoreBackend(context)
}

class ChunkStoreBackend(val baseDir: String) : StorageBackend {
    companion object {
        private fun getNamespaceDir(baseDir: String, namespace: String): File {
            return File(PathUtils.appendDir(baseDir, namespace))
        }

        private fun getBranchDir(baseDir: String, namespace: String, branch: String): File {
            val namespaceDir = getNamespaceDir(baseDir, namespace)
            return File(namespaceDir, branch)
        }
    }

    class ChunkStoreBranchBackend(val chunkStore: ChunkStore, val baseDir: String, val namespace: String, val branch: String)
        : StorageBackend.BranchBackend {
        override fun getChunkStorage(): ChunkStorage {
            return object : ChunkStorage {
                override fun startTransaction(): ChunkTransaction {
                    return chunkStore.openTransaction().toChunkTransaction()
                }
            }
        }

        override fun getBranchLog(): BranchLog {
            val branchDir = getBranchDir(baseDir, namespace, branch)
            return ChunkStoreBranchLog(branchDir, branch)
        }
    }

    private fun getBranchDir(namespace: String, branch: String): File {
        return getBranchDir(baseDir, namespace, branch)
    }

    override suspend fun open(namespace: String, branch: String): StorageBackend.BranchBackend {
        val branchDir = getBranchDir(namespace, branch)
        return ChunkStoreBranchBackend(ChunkStore.open(branchDir, branch), baseDir, namespace, branch)
    }

    override suspend fun create(namespace: String, branch: String): StorageBackend.BranchBackend {
        val dir = getBranchDir(namespace, branch)
        dir.mkdirs()
        return ChunkStoreBranchBackend(ChunkStore.create(dir, branch), baseDir, namespace, branch)
    }

    override suspend fun exists(namespace: String, branch: String): Boolean {
        val branchDir = getBranchDir(namespace, branch)
        return ChunkStore.exists(branchDir, branch)
    }

    override suspend fun delete(namespace: String, branch: String) {
        getBranchDir(namespace, branch).deleteRecursively()
    }

    override suspend fun deleteNamespace(namespace: String) {
        getNamespaceDir(baseDir, namespace).deleteRecursively()
    }

    override suspend fun listBranches(namespace: String): Collection<BranchLog> {
        val namespaceDir = getNamespaceDir(baseDir, namespace)
        if (!namespaceDir.exists())
            return emptyList()

        val branches = ArrayList<BranchLog>()
        for (branch in namespaceDir.list()) {
            val branchFile = File(namespaceDir, branch)
            if (branchFile.isFile)
                continue
            if (!exists(namespace, branch))
                continue
            try {
                val branchLog = ChunkStoreBranchLog(namespaceDir, branch)
                branches.add(branchLog)
            } catch (e: IOException) {
                continue
            }
        }
        return branches
    }

    override suspend fun listNamespaces(): Collection<String> {
        return File(baseDir).listFiles()
                .filter { it.isDirectory }
                .map { it.name }
    }
}
